
import React, { useState } from "react";
import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
import { useNavigation } from '@react-navigation/native';
import OTPVerification from './OTPVerification';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


export const SignUp = () => {
  const navigation = useNavigation();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [createPassword, setCreatePassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const handleSignUp = async () => {
    // Validate name, email, and password
    if (!name.trim()) {
      setNameError("Name cannot be empty.");
      return;
    } else {
      setNameError("");
    }

    if (!email.trim()) {
      setEmailError("Email cannot be empty.");
      return;
    } else if (!validateEmail(email)) {
      setEmailError("Invalid email format.");
      return;
    } else {
      setEmailError("");
    }

    if (!validatePassword(createPassword)) {
      setPasswordError("Password must have at least 8 characters, including one uppercase letter, one lowercase letter, one number, and one special character");
      return;
    } else {
      setPasswordError("");
    }

    // Additional validation for password match
    if (createPassword !== confirmPassword) {
      setPasswordError("Passwords do not match");
      return;
    } else {
      setPasswordError("");
    }


    try {
      const response = await fetch('http://10.9.85.243:8080/api/users/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name, email, password: createPassword, confirmPassword: createPassword }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log('Signup successful:', data);


        navigation.navigate('OTPVerification', { email });

      } else {
        const errorData = await response.json();
        console.error('Signup failed:', errorData);
        // Handle the error and display a message to the user
      }
    } catch (error) {
      console.error('Error during signup:', error);
      // Handle any unexpected errors
    }
  };

  const navigateToLogin = () => {
    navigation.navigate('Login'); // Navigate to your login screen
  };

  const validateEmail = (email) => {
    // Email validation logic here
    return /\S+@\S+\.\S+/.test(email);
  };

  const validatePassword = (password) => {
    // Password validation logic here
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$/.test(password);
  };

  return (

    <SafeAreaView style={styles.container}>
      <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backButton}>
        <FontAwesome name="chevron-left" size={20} color="#ffa500" />
      </TouchableOpacity>
      <View style={styles.innerContainer}>

        <Text style={styles.text}>Sign Up</Text>
        <TextInput
          style={[styles.input, nameError ? styles.errorInput : null]}
          value={name}
          placeholder={"Name"}
          onChangeText={(text) => setName(text)}
          autoCapitalize={"none"}
        />
        {nameError ? <Text style={styles.errorText}>{nameError}</Text> : null}

        <TextInput
          style={[styles.input, emailError ? styles.errorInput : null]}
          value={email}
          placeholder={"Email Address"}
          onChangeText={(text) => setEmail(text)}
          autoCapitalize={"none"}
        />
        {emailError ? <Text style={styles.errorText}>{emailError}</Text> : null}

        <TextInput
          style={[styles.input, passwordError ? styles.errorInput : null]}
          value={createPassword}
          placeholder={"Create Password"}
          secureTextEntry
          onChangeText={(text) => setCreatePassword(text)}
        />
        {passwordError ? <Text style={styles.errorText}>{passwordError}</Text> : null}

        <TextInput
          style={[styles.input, passwordError ? styles.errorInput : null]}
          value={confirmPassword}
          placeholder={"Confirm Password"}
          secureTextEntry
          onChangeText={(text) => setConfirmPassword(text)}
        />
        {passwordError ? <Text style={styles.errorText}>{passwordError}</Text> : null}

        <TouchableOpacity onPress={handleSignUp} style={styles.button}>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>

        <Text style={styles.loginText}>
          Already have an account?{" "}
          <Text style={styles.loginLink} onPress={navigateToLogin}>Login</Text>
        </Text>

      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },


  // backButton: {
  //   padding: 10,
  // },
  backButton: {
    padding: 10,
    position: 'absolute',
    top: 0,
    left: 0,
  },
  innerContainer: {
    width: '80%',
    borderWidth: 1,
    borderColor: "#777",
    borderRadius: 20,
    padding: 20,
  },
  input: {
    height: 40,
    marginBottom: 10,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#777",
    borderRadius: 20,
    paddingHorizontal: 10,
  },
  text: {
    textAlign: 'center',
    color: '#ffa500',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 20,
  },
  button: {
    height: 40,
    marginBottom: 10,
    backgroundColor: "#ffa500",
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  loginText: {
    textAlign: 'center',
    color: '#444',
    fontSize: 12,
  },
  loginLink: {
    color: '#ffa500',
  },
  errorInput: {
    borderColor: 'red',
  },
  errorText: {
    color: 'red',
    marginLeft: 5,
    marginTop: -5,
    marginBottom: 5,
    fontSize: 11,
  },
});

export default SignUp;
