// import React, { useState } from 'react';
// import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
// import { useNavigation } from '@react-navigation/native'; // Import useNavigation hook
// import Header from '../components/Header';
// import SearchBarMenu from '../components/searchBarMenu';
// import UserBottomTabNavigator from '../components/userBottomTabNavigator'; // Adjust the path if necessary
// import { FontAwesome } from '@expo/vector-icons'; // Import FontAwesome icon library

// const Events = () => {
//   const navigation = useNavigation(); // Initialize useNavigation hook

//   const [filteredNewsData, setFilteredNewsData] = useState([]);
//   // Dummy data for announcement list
//   const newsData = [
//     { id: '1', title: 'Important Announcement: College Closure', image: require('../assets/home1.jpg'), details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', category: 'events' },
//     { id: '2', title: 'Upcoming Event: Annual College Day', image: require('../assets/home1.jpg'), details: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', category: 'events' },
//     // Add more announcement items as needed
//   ];

//   // Function to filter news data based on search term
//   const filterNewsData = (text) => {
//     const filteredData = newsData.filter(item => item.title.toLowerCase().includes(text.toLowerCase()));
//     setFilteredNewsData(filteredData);
//   };

//   // Render item for news list
//   const renderNewsItem = ({ item, index }) => {
//     // Function to truncate text after a certain number of words
//     const truncateText = (text, limit) => {
//       const words = text.split(' ');
//       if (words.length > limit) {
//         return words.slice(0, limit).join(' ') + '...';
//       } else {
//         return text;
//       }
//     };

//     const navigateToNewsDetails = () => {
//       // Navigate to news details screen with the news id or any unique identifier
//       navigation.navigate('UserNewsDetails', { newsId: item.id });
//     };

//     return (
//       <TouchableOpacity onPress={navigateToNewsDetails}>
//         <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
//           <View style={styles.newsImageContainer}>
//             <Image source={item.image} style={styles.newsImage} />
//           </View>
//           <View style={styles.newsDetails}>
//             <Text style={styles.newsTitle}>{item.title}</Text>
//             <Text style={styles.newsDescription}>{truncateText(item.details, 20)}</Text>
//           </View>
//         </View>
//       </TouchableOpacity>
//     );
//   };

//   return (
//     <View style={styles.container}>
//       <Header />

//       {/* Search Bar */}
//       <SearchBarMenu placeholder="Search by title..." onSearch={filterNewsData} />
//       <ScrollView contentContainerStyle={styles.scrollViewContent}>
//       <View style={styles.content}>
//         {/* Header with Text and Background Image */}
//         <View style={styles.headerWithText}>
//           <TouchableOpacity onPress={() => navigation.goBack()} style={styles.leftArrowContainer}>
//             <FontAwesome name="chevron-left" size={18} color="#ffa500" />
//           </TouchableOpacity>
//           <Text style={styles.headerText}>Events</Text>
//         </View>

//         <FlatList
//           data={filteredNewsData.length ? filteredNewsData : newsData}
//           renderItem={renderNewsItem}
//           keyExtractor={item => item.id}
//         />
//       </View>
//       </ScrollView>
//       {/* Bottom Tab Navigator */}
//       <UserBottomTabNavigator navigation={navigation} />
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   scrollViewContent: {
//     flexGrow: 1,
//   },
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//   },
//   content: {
//     flex: 1,
//   },
//   headerWithText: {
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'flex-start',
//     padding: 20,
//   },
//   headerText: {
//     fontSize: 20,
//     fontWeight: 'bold',
//     color: '#333',
//   },
//   leftArrowContainer: {
//     marginRight: 10,
//   },
//   newsItem: {
//     flexDirection: 'row',
//     alignItems: 'center',
//     paddingHorizontal: 20,
//     height: 150,
//     marginBottom: 20,
//   },
//   newsItemReverse: {
//     flexDirection: 'row-reverse', // Reverse the direction for alternate items
//   },
//   newsImageContainer: {
//     flex: 1,
//   },
//   newsImage: {
//     width: '100%',
//     height: '100%',    
//   },
//   newsDetails: {
//     flex: 1,
//   },
//   newsTitle: {
//     paddingHorizontal: 15,
//     paddingTop: 5,
//     fontSize: 13,
//     fontWeight: 'bold',
//   },
//   newsDescription: {
//     paddingHorizontal: 15,
//     paddingTop: 5,
//     fontSize: 11,
//     color: '#555',
//   },
// });

// export default Events;
import React, { useState, useEffect } from 'react';
import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Header from '../components/Header';
import SearchBarMenu from '../components/searchBarMenu';
import UserBottomTabNavigator from '../components/userBottomTabNavigator';
import FontAwesome from 'react-native-vector-icons/FontAwesome'; // Import FontAwesome for the left arrow icon
import NewsDetailsPage from '../screens/newsDetailsPage';
import UserNewsDetails from '../screens/userNewsDetails';

const Events = () => {
  const navigation = useNavigation();
  const [events, setEvents] = useState([]);
  const [filteredEvents, setFilteredEvents] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState('');

  useEffect(() => {
    const fetchEvents = async () => {
      try {
        const response = await fetch("http://10.9.85.243:8080/api/news/news/events"); // Assuming events are fetched from a different endpoint
        if (!response.ok) {
          throw new Error("Failed to fetch events");
        }
        const data = await response.json();
        const fetchedEvents = Array.isArray(data.events) ? data.events : [data.events];

        // Sort the events array by upload date in descending order
        fetchedEvents.sort((a, b) => new Date(b.uploadDate) - new Date(a.uploadDate));

        setEvents(fetchedEvents);
        setFilteredEvents(fetchedEvents);
      } catch (error) {
        console.error("Error fetching events:", error);
      }
    };

    fetchEvents();
  }, []);

  const filterEvents = (text) => {
    setSearchKeyword(text);
    const filteredData = events.filter(event =>
      event.title.toLowerCase().includes(text.toLowerCase())
    );
    setFilteredEvents(filteredData);
  };

  const navigateToEventDetails = (id) => {
    console.log("Navigating to event details with id:", id);
    navigation.navigate('UserNewsDetails', { newsId: id });
  };

  const renderEventItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => navigateToEventDetails(item._id)}>
      <View style={[styles.eventItem, index % 2 !== 0 && styles.eventItemReverse]}>
        <View style={styles.eventImageContainer}>
          {item.image ? (
            <Image
              source={{ uri: `http://10.9.85.243:8080/${item.image.replace(/\\/g, '/')}` }}
              style={styles.eventImage}
              onLoad={() => console.log('Image loaded successfully')}
              onError={(error) => console.error('Error loading image:', error)}
            />
          ) : (
            <View style={styles.placeholderImage}>
              <Text style={styles.placeholderText}>Image Not Available</Text>
            </View>
          )}
        </View>
        <View style={styles.eventDetails}>
          <Text style={styles.eventTitle}>{item.title}</Text>
          <Text style={styles.eventDescription}>{item.details}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <Header />
      <SearchBarMenu placeholder="Search by title..." onSearch={filterEvents} />
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.content}>
          <View style={styles.headerWithText}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.leftArrowContainer}>
              <FontAwesome name="chevron-left" size={20} color="#ffa500" />
            </TouchableOpacity>
            <Text style={styles.headerText}>Events</Text>
          </View>

          <FlatList
            data={filteredEvents}
            renderItem={renderEventItem}
            keyExtractor={item => item._id} // Assuming _id is unique for events
          />
        </View>
      </ScrollView>
      <UserBottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
  },
  headerWithText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 20,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333',
  },
  eventItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 150,
    marginBottom: 20,
  },
  eventItemReverse: {
    flexDirection: 'row-reverse', // Reverse the direction for alternate items
  },
  eventImageContainer: {
    flex: 1,
  },
  eventImage: {
    width: '100%',
    height: '100%',
  },
  eventDetails: {
    flex: 1,
  },
  eventTitle: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 13,
    fontWeight: 'bold',
  },
  eventDescription: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 11,
    color: '#555',
  },
  placeholderImage: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ccc',
  },
  placeholderText: {
    fontSize: 12,
    color: '#888',
  },
});

export default Events;
