

// // import React, { useState, useEffect } from 'react';
// // import { View, Text, StyleSheet, Image, FlatList, ScrollView, TouchableOpacity, Alert } from 'react-native';
// // import AsyncStorage from '@react-native-async-storage/async-storage';
// // import * as ImagePicker from 'expo-image-picker';
// // import Header from '../components/Header';
// // import UserBottomTabNavigator from '../components/userBottomTabNavigator';
// // import { useNavigation } from '@react-navigation/native';
// // import { FontAwesome } from '@expo/vector-icons';

// // const Profile = () => {
// //     const navigation = useNavigation();
// //     const [user, setUser] = useState({
// //         name: '',
// //         email: '',
// //         image: require('../assets/user.jpeg'), // Default local image
// //         favoriteNews: [],
// //     });

// //     const fetchUserData = async () => {
// //         try {
// //             const name = await AsyncStorage.getItem('name') || '';
// //             const email = await AsyncStorage.getItem('email') || '';
// //             const profileImage = await AsyncStorage.getItem('profile_image_url') || '/default-profile-image.png';
// //             console.log('User name:', name);
// //             console.log('User email:', email);
// //             console.log('Profile image URL:', profileImage);

// //             setUser(prevUser => ({
// //                 ...prevUser,
// //                 name,
// //                 email,
// //                 image: profileImage === '/default-profile-image.png'
// //                     ? require('../assets/user.jpeg')  // Use local default image if backend image is default
// //                     : { uri: `http://10.9.92.25:8080${profileImage}` }
// //             }));
// //         } catch (error) {
// //             console.error('Error fetching user data:', error);
// //         }
// //     };

// //     const fetchFavoriteNews = async () => {
// //         try {
// //             const email = await AsyncStorage.getItem('email');
// //             if (!email) {
// //                 throw new Error('User not authenticated');
// //             }

// //             const keys = await AsyncStorage.getAllKeys();
// //             console.log('All keys:', keys);

// //             const favoriteNewsKeys = keys.filter(key => key.startsWith(`news_${email}_`));
// //             console.log('Favorite news keys:', favoriteNewsKeys);

// //             if (favoriteNewsKeys.length === 0) {
// //                 console.log('No favorite news found');
// //                 return;
// //             }

// //             const favoriteNewsDetails = [];
// //             for (const key of favoriteNewsKeys) {
// //                 const newsDetailsString = await AsyncStorage.getItem(key);
// //                 if (newsDetailsString) {
// //                     const newsDetails = JSON.parse(newsDetailsString);
// //                     console.log('Favorite news details for key', key, ':', newsDetails);
// //                     favoriteNewsDetails.push(newsDetails);
// //                 }
// //             }

// //             setUser(prevUser => ({ ...prevUser, favoriteNews: favoriteNewsDetails }));
// //         } catch (error) {
// //             console.error('Error fetching favorite news:', error);
// //         }
// //     };

// //     useEffect(() => {
// //         fetchUserData();
// //         fetchFavoriteNews();
// //     }, []);

// //     const handleImageUpload = async () => {
// //         try {
// //             const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
// //             if (status !== 'granted') {
// //                 Alert.alert('Sorry, we need camera roll permissions to make this work!');
// //                 return;
// //             }

// //             const result = await ImagePicker.launchImageLibraryAsync({
// //                 mediaTypes: ImagePicker.MediaTypeOptions.Images,
// //                 allowsEditing: true,
// //                 aspect: [1, 1],
// //                 quality: 1,
// //             });

// //             if (!result.cancelled && result.assets && result.assets.length > 0) {
// //                 const imageUri = result.assets[0].uri;
// //                 console.log('Image URI:', imageUri);

// //                 if (!imageUri) {
// //                     console.error('Image URI is undefined');
// //                     Alert.alert('Error', 'Image URI is undefined.');
// //                     return;
// //                 }

// //                 if (!imageUri.startsWith('file://')) {
// //                     console.error('Invalid URI format:', imageUri);
// //                     Alert.alert('Error', 'Invalid image URI format.');
// //                     return;
// //                 }

// //                 const formData = new FormData();
// //                 formData.append('image', {
// //                     uri: imageUri,
// //                     type: 'image/jpeg',
// //                     name: 'profile_image.jpg',
// //                 });

// //                 console.log('Form Data:', formData);

// //                 const token = await AsyncStorage.getItem('token');
// //                 try {
// //                     const response = await fetch('http://10.9.92.25:8080/api/users/user/upload-profile-image', {
// //                         method: 'PUT',
// //                         body: formData,
// //                         headers: {
// //                             Accept: 'application/json',
// //                             'Authorization': `Bearer ${token}`,
// //                         },
// //                     });

// //                     if (!response.ok) {
// //                         throw new Error(`Image upload failed with status ${response.status}`);
// //                     }

// //                     const responseData = await response.json();
// //                     console.log('Image uploaded successfully', responseData);

// //                     const profileImageUrl = responseData.data.profileImage;
// //                     if (profileImageUrl) {
// //                         await AsyncStorage.setItem('profile_image_url', profileImageUrl);
// //                         setUser(prevUser => ({
// //                             ...prevUser,
// //                             image: { uri: `http://10.9.92.25:8080${profileImageUrl}` }
// //                         }));
// //                         console.log('Profile image URL saved to AsyncStorage:', profileImageUrl);
// //                     } else {
// //                         console.error('Profile image URL not found in response');
// //                         Alert.alert('Error', 'Profile image URL not found in response.');
// //                     }
// //                 } catch (error) {
// //                     console.error('Error uploading image:', error);
// //                     Alert.alert('Error uploading image:', error.message);
// //                 }
// //             } else {
// //                 console.log('Image picking cancelled.');
// //             }
// //         } catch (error) {
// //             console.error('Error with image picker:', error);
// //             Alert.alert('Error with image picker:', error.message);
// //         }
// //     };

// //     return (
// //         <View style={styles.container}>
// //             <Header />
// //             <ScrollView contentContainerStyle={styles.contentContainer}>
// //                 <View style={styles.userInfoContainer}>
// //                     <Text style={styles.profileText}>Profile</Text>
// //                     <TouchableOpacity onPress={handleImageUpload}>
// //                         <Image source={user.image} style={styles.userImage} />
// //                     </TouchableOpacity>
// //                     <View style={styles.userInfoText}>
// //                         <Text style={styles.userName}>{user.name}</Text>
// //                         <Text style={styles.userEmail}>{user.email}</Text>
// //                     </View>
// //                 </View>
// //                 <View style={styles.favoriteNewsContainer}>
// //                     <View style={styles.bookmarkedContainer}>
// //                         <FontAwesome name="bookmark" size={20} color="#ffa500" style={styles.bookmarkIcon} />
// //                         <Text style={styles.bookmarkedText}>Bookmarked</Text>
// //                     </View>
// //                     <FlatList
// //                         data={user.favoriteNews}
// //                         keyExtractor={(item) => item._id}
// //                         renderItem={({ item }) => (
// //                             <View style={styles.newsItem}>
// //                                 <Text style={styles.newsTitle}>{item.news.title}</Text>
// //                                 <Text style={styles.newsDate}>{item.news.reporter}</Text>
// //                                 <Text style={styles.newsDetails}>
// //                                     {item.news.content ? item.news.content.substring(0, 150) : 'No content available'}...
// //                                 </Text>
// //                             </View>
// //                         )}
// //                         ListEmptyComponent={<Text>No favorite news found</Text>}
// //                     />
// //                 </View>
// //             </ScrollView>
// //             <UserBottomTabNavigator navigation={navigation} />
// //         </View>
// //     );
// // };

// // const styles = StyleSheet.create({
// //     container: {
// //         flex: 1,
// //         backgroundColor: '#f7f7f7',
// //     },
// //     contentContainer: {
// //         flexGrow: 1,
// //         paddingHorizontal: 20,
// //         paddingBottom: 20,
// //     },
// //     userInfoContainer: {
// //         alignItems: 'center',
// //         marginBottom: 20,
// //         marginTop: 20,
// //         backgroundColor: '#fff',
// //         padding: 20,
// //         borderRadius: 10,
// //         shadowColor: '#000',
// //         shadowOffset: { width: 0, height: 2 },
// //         shadowOpacity: 0.2,
// //         shadowRadius: 2,
// //         elevation: 5,
// //     },
// //     profileText: {
// //         fontSize: 24,
// //         fontWeight: 'bold',
// //         marginBottom: 15,
// //     },
// //     userImage: {
// //         width: 150,
// //         height: 150,
// //         borderRadius: 75,
// //         marginBottom: 15,
// //     },
// //     userInfoText: {
// //         alignItems: 'center',
// //     },
// //     userName: {
// //         fontSize: 20,
// //         fontWeight: 'bold',
// //         textAlign: 'center',
// //         marginBottom: 5,
// //     },
// //     userEmail: {
// //         fontSize: 16,
// //         color: '#666',
// //     },
// //     favoriteNewsContainer: {
// //         flex: 1,
// //     },
// //     bookmarkedContainer: {
// //         flexDirection: 'row',
// //         alignItems: 'center',
// //         marginBottom: 10,
// //         marginTop: 30,
// //         paddingHorizontal: 10,
// //     },
// //     bookmarkIcon: {
// //         marginRight: 10,
// //     },
// //     bookmarkedText: {
// //         fontSize: 18,
// //         fontWeight: 'bold',
// //     },
// //     newsItem: {
// //         backgroundColor: '#fff',
// //         padding: 15,
// //         borderRadius: 10,
// //         shadowColor: '#000',
// //         shadowOffset: { width: 0, height: 2 },
// //         shadowOpacity: 0.1,
// //         shadowRadius: 2,
// //         elevation: 3,
// //         marginBottom: 10,
// //     },
// //     newsTitle: {
// //         fontSize: 18,
// //         fontWeight: 'bold',
// //         marginBottom: 5,
// //     },
// //     newsDate: {
// //         fontSize: 14,
// //         color: '#666',
// //         marginBottom: 10,
// //     },
// //     newsDetails: {
// //         fontSize: 14,
// //         color: '#666',
// //     },
// // });

// // export default Profile;
// import React, { useState, useEffect } from 'react';
// import { View, Text, StyleSheet, Image, FlatList, ScrollView, TouchableOpacity, Alert } from 'react-native';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import * as ImagePicker from 'expo-image-picker';
// import Header from '../components/Header';
// import UserBottomTabNavigator from '../components/userBottomTabNavigator';
// import { useNavigation } from '@react-navigation/native';
// import { FontAwesome } from '@expo/vector-icons';

// const Profile = () => {
//     const navigation = useNavigation();
//     const [user, setUser] = useState({
//         name: '',
//         email: '',
//         image: require('../assets/user.jpeg'), // Default local image
//         favoriteNews: [],
//     });

//     const fetchUserData = async () => {
//         try {
//             const email = await AsyncStorage.getItem('email') || '';
//             const name = await AsyncStorage.getItem(`name`) || '';
//             const profileImage = await AsyncStorage.getItem(`${email}_profile_image_url`) || '/default-profile-image.png';
//             console.log('User name:', name);
//             console.log('User email:', email);
//             console.log('Profile image URL:', profileImage);

//             setUser(prevUser => ({
//                 ...prevUser,
//                 name,
//                 email,
//                 image: profileImage === '/default-profile-image.png'
//                     ? require('../assets/user.jpeg')  // Use local default image if backend image is default
//                     : { uri: `http://10.9.85.243:8080${profileImage}` }
//             }));
//         } catch (error) {
//             console.error('Error fetching user data:', error);
//         }
//     };

//     const fetchFavoriteNews = async () => {
//         try {
//             const email = await AsyncStorage.getItem('email');
//             if (!email) {
//                 throw new Error('User not authenticated');
//             }

//             const keys = await AsyncStorage.getAllKeys();
//             console.log('All keys:', keys);

//             const favoriteNewsKeys = keys.filter(key => key.startsWith(`news_${email}_`));
//             console.log('Favorite news keys:', favoriteNewsKeys);

//             if (favoriteNewsKeys.length === 0) {
//                 console.log('No favorite news found');
//                 return;
//             }

//             const favoriteNewsDetails = [];
//             for (const key of favoriteNewsKeys) {
//                 const newsDetailsString = await AsyncStorage.getItem(key);
//                 if (newsDetailsString) {
//                     const newsDetails = JSON.parse(newsDetailsString);
//                     console.log('Favorite news details for key', key, ':', newsDetails);
//                     favoriteNewsDetails.push(newsDetails);
//                 }
//             }

//             setUser(prevUser => ({ ...prevUser, favoriteNews: favoriteNewsDetails }));
//         } catch (error) {
//             console.error('Error fetching favorite news:', error);
//         }
//     };

//     useEffect(() => {
//         fetchUserData();
//         fetchFavoriteNews();
//     }, []);

//     const handleImageUpload = async () => {
//         try {
//             const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
//             if (status !== 'granted') {
//                 Alert.alert('Sorry, we need camera roll permissions to make this work!');
//                 return;
//             }

//             const result = await ImagePicker.launchImageLibraryAsync({
//                 mediaTypes: ImagePicker.MediaTypeOptions.Images,
//                 allowsEditing: true,
//                 aspect: [1, 1],
//                 quality: 1,
//             });

//             if (!result.cancelled && result.assets && result.assets.length > 0) {
//                 const imageUri = result.assets[0].uri;
//                 console.log('Image URI:', imageUri);

//                 if (!imageUri) {
//                     console.error('Image URI is undefined');
//                     Alert.alert('Error', 'Image URI is undefined.');
//                     return;
//                 }

//                 if (!imageUri.startsWith('file://')) {
//                     console.error('Invalid URI format:', imageUri);
//                     Alert.alert('Error', 'Invalid image URI format.');
//                     return;
//                 }

//                 const formData = new FormData();
//                 formData.append('image', {
//                     uri: imageUri,
//                     type: 'image/jpeg',
//                     name: 'profile_image.jpg',
//                 });

//                 console.log('Form Data:', formData);

//                 const token = await AsyncStorage.getItem('token');
//                 const email = await AsyncStorage.getItem('email');

//                 try {
//                     const response = await fetch('http://10.9.85.243:8080/api/users/user/upload-profile-image', {
//                         method: 'PUT',
//                         body: formData,
//                         headers: {
//                             Accept: 'application/json',
//                             'Authorization': `Bearer ${token}`,
//                         },
//                     });

//                     if (!response.ok) {
//                         throw new Error(`Image upload failed with status ${response.status}`);
//                     }

//                     const responseData = await response.json();
//                     console.log('Image uploaded successfully', responseData);

//                     const profileImageUrl = responseData.data.profileImage;
//                     if (profileImageUrl) {
//                         await AsyncStorage.setItem(`${email}_profile_image_url`, profileImageUrl);
//                         setUser(prevUser => ({
//                             ...prevUser,
//                             image: { uri: `http://10.9.85.243:8080${profileImageUrl}` }
//                         }));
//                         console.log('Profile image URL saved to AsyncStorage:', profileImageUrl);
//                     } else {
//                         console.error('Profile image URL not found in response');
//                         Alert.alert('Error', 'Profile image URL not found in response.');
//                     }
//                 } catch (error) {
//                     console.error('Error uploading image:', error);
//                     Alert.alert('Error uploading image:', error.message);
//                 }
//             } else {
//                 console.log('Image picking cancelled.');
//             }
//         } catch (error) {
//             console.error('Error with image picker:', error);
//             Alert.alert('Error with image picker:', error.message);
//         }
//     };

//     return (
//         <View style={styles.container}>
//             <Header />
//             <ScrollView contentContainerStyle={styles.contentContainer}>
//                 <View style={styles.userInfoContainer}>
//                     <Text style={styles.profileText}>Profile</Text>
//                     <TouchableOpacity onPress={handleImageUpload}>
//                         <Image source={user.image} style={styles.userImage} />
//                     </TouchableOpacity>
//                     <View style={styles.userInfoText}>
//                         <Text style={styles.userName}>{user.name}</Text>
//                         <Text style={styles.userEmail}>{user.email}</Text>
//                     </View>
//                 </View>
//                 <View style={styles.favoriteNewsContainer}>
//                     <View style={styles.bookmarkedContainer}>
//                         <FontAwesome name="bookmark" size={20} color="#ffa500" style={styles.bookmarkIcon} />
//                         <Text style={styles.bookmarkedText}>Bookmarked</Text>
//                     </View>
//                     <FlatList
//                         data={user.favoriteNews}
//                         keyExtractor={(item) => item._id}
//                         renderItem={({ item }) => (
//                             <View style={styles.newsItem}>
//                                 <Text style={styles.newsTitle}>{item.news.title}</Text>
//                                 <Text style={styles.newsDate}>{item.news.reporter}</Text>
//                                 <Text style={styles.newsDetails}>
//                                     {item.news.content ? item.news.content.substring(0, 150) : 'No content available'}...
//                                 </Text>
//                             </View>
//                         )}
//                         ListEmptyComponent={<Text>No favorite news found</Text>}
//                     />
//                 </View>
//             </ScrollView>
//             <UserBottomTabNavigator navigation={navigation} />
//         </View>
//     );
// };

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#f7f7f7',
//     },
//     contentContainer: {
//         flexGrow: 1,
//         paddingHorizontal: 20,
//         paddingBottom: 20,
//     },
//     userInfoContainer: {
//         alignItems: 'center',
//         marginBottom: 20,
//         marginTop: 20,
//         backgroundColor: '#fff',
//         padding: 20,
//         borderRadius: 10,
//         shadowColor: '#000',
//         shadowOffset: { width: 0, height: 2 },
//         shadowOpacity: 0.2,
//         shadowRadius: 2,
//         elevation: 5,
//     },
//     profileText: {
//         fontSize: 24,
//         fontWeight: 'bold',
//         marginBottom: 15,
//     },
//     userImage: {
//         width: 150,
//         height: 150,
//         borderRadius: 75,
//         marginBottom: 15,
//     },
//     userInfoText: {
//         alignItems: 'center',
//     },
//     userName: {
//         fontSize: 20,
//         fontWeight: 'bold',
//         textAlign: 'center',
//         marginBottom: 5,
//     },
//     userEmail: {
//         fontSize: 16,
//         color: '#666',
//     },
//     favoriteNewsContainer: {
//         flex: 1,
//     },
//     bookmarkedContainer: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         marginBottom: 10,
//         marginTop: 30,
//         paddingHorizontal: 10,
//     },
//     bookmarkIcon: {
//         marginRight: 10,
//     },
//     bookmarkedText: {
//         fontSize: 18,
//         fontWeight: 'bold',
//     },
//     newsItem: {
//         backgroundColor: '#fff',
//         padding: 15,
//         borderRadius: 10,
//         shadowColor: '#000',
//         shadowOffset: { width: 0, height: 2 },
//         shadowOpacity: 0.1,
//         shadowRadius: 2,
//         elevation: 3,
//         marginBottom: 10,
//     },
//     newsTitle: {
//         fontSize: 18,
//         fontWeight: 'bold',
//         marginBottom: 5,
//     },
//     newsDate: {
//         fontSize: 14,
//         color: '#666',
//         marginBottom: 10,
//     },
//     newsDetails: {
//         fontSize: 14,
//         color: '#666',
//     },
// });

// export default Profile;
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, ScrollView, TouchableOpacity, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as ImagePicker from 'expo-image-picker';
import Header from '../components/Header';
import UserBottomTabNavigator from '../components/userBottomTabNavigator';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome } from '@expo/vector-icons';
import UserNewsDetails from '../screens/userNewsDetails'

const Profile = () => {
    const navigation = useNavigation();
    const [user, setUser] = useState({
        name: '',
        email: '',
        // image: require('../assets/profile.png'), // Default local image
        image: null,
        favoriteNews: [],
    });

    const fetchUserData = async () => {
        try {
            const email = await AsyncStorage.getItem('email') || '';
            const name = await AsyncStorage.getItem(`name`) || '';
            const profileImage = await AsyncStorage.getItem(`${email}_profile_image_url`) || '/default-profile-image.png';
            console.log('User name:', name);
            console.log('User email:', email);
            console.log('Profile image URL:', profileImage);

            setUser(prevUser => ({
                ...prevUser,
                name,
                email,
                image: profileImage === '/default-profile-image.png'
                    ? require('../assets/user.jpeg')  // Use local default image if backend image is default
                    : { uri: `http://10.9.85.243:8080${profileImage}` }
            }));
        } catch (error) {
            console.error('Error fetching user data:', error);
        }
    };

    const fetchFavoriteNews = async () => {
        try {
            const email = await AsyncStorage.getItem('email');
            if (!email) {
                throw new Error('User not authenticated');
            }

            const keys = await AsyncStorage.getAllKeys();
            console.log('All keys:', keys);

            const favoriteNewsKeys = keys.filter(key => key.startsWith(`news_${email}_`));
            console.log('Favorite news keys:', favoriteNewsKeys);

            if (favoriteNewsKeys.length === 0) {
                console.log('No favorite news found');
                return;
            }

            const favoriteNewsDetails = [];
            for (const key of favoriteNewsKeys) {
                const newsDetailsString = await AsyncStorage.getItem(key);
                if (newsDetailsString) {
                    const newsDetails = JSON.parse(newsDetailsString);
                    console.log('Favorite news details for key', key, ':', newsDetails);
                    favoriteNewsDetails.push(newsDetails);
                }
            }

            setUser(prevUser => ({ ...prevUser, favoriteNews: favoriteNewsDetails }));
        } catch (error) {
            console.error('Error fetching favorite news:', error);
        }
    };

    useEffect(() => {
        fetchUserData();
        fetchFavoriteNews();
    }, []);

    const handleImageUpload = async () => {
        try {
            const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== 'granted') {
                Alert.alert('Sorry, we need camera roll permissions to make this work!');
                return;
            }

            const result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                aspect: [1, 1],
                quality: 1,
            });

            if (!result.cancelled && result.assets && result.assets.length > 0) {
                const imageUri = result.assets[0].uri;
                console.log('Image URI:', imageUri);

                if (!imageUri) {
                    console.error('Image URI is undefined');
                    Alert.alert('Error', 'Image URI is undefined.');
                    return;
                }

                if (!imageUri.startsWith('file://')) {
                    console.error('Invalid URI format:', imageUri);
                    Alert.alert('Error', 'Invalid image URI format.');
                    return;
                }

                const formData = new FormData();
                formData.append('image', {
                    uri: imageUri,
                    type: 'image/jpeg',
                    name: 'profile_image.jpg',
                });

                console.log('Form Data:', formData);

                const token = await AsyncStorage.getItem('token');
                const email = await AsyncStorage.getItem('email');

                try {
                    const response = await fetch('http://10.9.85.243:8080/api/users/user/upload-profile-image', {
                        method: 'PUT',
                        body: formData,
                        headers: {
                            Accept: 'application/json',
                            'Authorization': `Bearer ${token}`,
                        },
                    });

                    if (!response.ok) {
                        throw new Error(`Image upload failed with status ${response.status}`);
                    }

                    const responseData = await response.json();
                    console.log('Image uploaded successfully', responseData);

                    const profileImageUrl = responseData.data.profileImage;
                    if (profileImageUrl) {
                        await AsyncStorage.setItem(`${email}_profile_image_url`, profileImageUrl);
                        setUser(prevUser => ({
                            ...prevUser,
                            image: { uri: `http://10.9.85.243:8080${profileImageUrl}` }
                        }));
                        console.log('Profile image URL saved to AsyncStorage:', profileImageUrl);
                    } else {
                        console.error('Profile image URL not found in response');
                        Alert.alert('Error', 'Profile image URL not found in response.');
                    }
                } catch (error) {
                    console.error('Error uploading image:', error);
                    Alert.alert('Error uploading image:', error.message);
                }
            } else {
                console.log('Image picking cancelled.');
            }
        } catch (error) {
            console.error('Error with image picker:', error);
            Alert.alert('Error with image picker:', error.message);
        }
    };

    // const navigateToNewsDetails = (id) => {
    //     console.log("Navigating to news details with id:", id);
    //     navigation.navigate('UserNewsDetails', { newsId: id });
    // };
    const navigateToNewsDetails = (id) => {
        console.log("Navigating to news details with id:", id);
        navigation.navigate('UserNewsDetails', { newsId: id });
    };

    return (
        <View style={styles.container}>
            <Header />

            <ScrollView contentContainerStyle={styles.contentContainer}>
                <View style={styles.headerWithText}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={styles.leftArrowContainer}>
                        <FontAwesome name="chevron-left" size={20} color="#ffa500" />
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Profile</Text>
                </View>
                <View style={styles.userInfoContainer}>

                    <Text style={styles.profileText}>Profile</Text>
                    <TouchableOpacity onPress={handleImageUpload}>
                        <Image source={user.image} style={styles.userImage} />
                    </TouchableOpacity>
                    <View style={styles.userInfoText}>
                        <Text style={styles.userName}>{user.name}</Text>
                        <Text style={styles.userEmail}>{user.email}</Text>
                    </View>
                </View>
                <View style={styles.favoriteNewsContainer}>
                    <View style={styles.bookmarkedContainer}>
                        <FontAwesome name="bookmark" size={20} color="#ffa500" style={styles.bookmarkIcon} />
                        <Text style={styles.bookmarkedText}>Bookmarked</Text>
                    </View>
                    <FlatList
                        data={user.favoriteNews}
                        keyExtractor={(item) => item._id}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => navigateToNewsDetails(item._id)}>
                                <View style={styles.newsItem}>
                                    <Text style={styles.newsTitle}>{item.news.title}</Text>
                                    <Text style={styles.newsDate}>{item.news.reporter}</Text>
                                    <Text style={styles.newsDetails}>
                                        {item.news.content ? item.news.content.substring(0, 150) : 'No content available'}...
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        )}
                        ListEmptyComponent={<Text>No favorite news found</Text>}
                    />
                </View>
            </ScrollView>
            <UserBottomTabNavigator navigation={navigation} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7f7f7',
    },
    contentContainer: {
        flexGrow: 1,
        paddingHorizontal: 20,
        paddingBottom: 20,
    },
    userInfoContainer: {
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 20,
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 5,
    },
    profileText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 15,
    },
    userImage: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 15,
    },
    userInfoText: {
        alignItems: 'center',
    },
    userName: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 5,
    },
    userEmail: {
        fontSize: 16,
        color: '#666',
    },
    favoriteNewsContainer: {
        flex: 1,
    },
    bookmarkedContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
        marginTop: 30,
        paddingHorizontal: 10,
    },
    bookmarkIcon: {
        marginRight: 10,
    },
    bookmarkedText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    newsItem: {
        backgroundColor: '#fff',
        padding: 15,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 3,
        marginBottom: 10,
    },
    newsTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    headerWithText: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
        paddingVertical: 15,
    },
    headerText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333',
        marginLeft: 5,
    },
    newsDate: {
        fontSize: 14,
        color: '#666',
        marginBottom: 10,
    },
    newsDetails: {
        fontSize: 14,
        color: '#666',
    },
});

export default Profile;
