
import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Header from '../components/Header';
import UserBottomTabNavigator from '../components/userBottomTabNavigator';
import SignUpAlert from '../components/SignUpAlert';
import { useNavigation } from '@react-navigation/native';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';


const UserNewsDetailsPage = ({ route }) => {
  const { newsId } = route.params;
  const navigation = useNavigation();
  const [newsDetails, setNewsDetails] = useState(null);
  const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState('');
  const [showAllComments, setShowAllComments] = useState(false);
  const [isBookmarked, setIsBookmarked] = useState(false);

  useEffect(() => {
    const fetchNewsDetails = async () => {
      try {
        const response = await fetch(`http://10.9.85.243:8080/api/news/${newsId}`);
        if (!response.ok) {
          throw new Error('Failed to fetch news');
        }
        const data = await response.json();
        setNewsDetails(Array.isArray(data) ? data : [data]);
        console.log('Fetched news details:', data);

        const commentsResponse = await fetch(`http://10.9.85.243:8080/api/news/${newsId}/comments`);
        if (!commentsResponse.ok) {
          throw new Error('Failed to fetch comments');
        }
        const commentsData = await commentsResponse.json();
        setComments(commentsData.comments.reverse());
        console.log('Fetched comments:', commentsData.comments);

        // Check if this news is bookmarked by the current user
        const email = await AsyncStorage.getItem('email');
        const bookmarkStatus = await AsyncStorage.getItem(`bookmark_${email}_${newsId}`);
        setIsBookmarked(JSON.parse(bookmarkStatus) || false);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchNewsDetails();
  }, [newsId]);
  const handleCommentSubmit = async () => {
    try {
      if (!newComment.trim()) {
        console.log('Comment cannot be empty');
        return;
      }

      const token = await AsyncStorage.getItem('token');
      const email = await AsyncStorage.getItem('email');
      const name = await AsyncStorage.getItem('name');
      const profileImage = await AsyncStorage.getItem(`${email}_profile_image_url`) || '/default_profile_image.png'; // Get profile image URL from AsyncStorage

      console.log(token, email);
      console.log('User Name:', name);

      const response = await fetch(`http://10.9.85.243:8080/api/news/${newsId}/comment`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify({ text: newComment, user: email }),
      });

      if (!response.ok) {
        const errorMessage = await response.text();
        throw new Error(`Failed to submit comment: ${errorMessage}`);
      }

      const newCommentData = await response.json();

      // Format the comment data with the correct profile image URL
      const formattedComment = {
        ...newCommentData,
        user: {
          name: name,
          profileImage: profileImage // Use profileImage obtained from AsyncStorage
        },
        createdAt: new Date().toISOString()
      };

      // Update the comments state with the new comment
      setComments([formattedComment, ...comments]);
      // Reset the new comment input
      setNewComment('');
      console.log('Comment successfully submitted:', formattedComment);
    } catch (error) {
      console.error('Error submitting comment:', error.message);
      navigation.navigate('SignUpAlert');
    }
  };

  // const handleCommentSubmit = async () => {
  //   try {
  //     if (!newComment.trim()) {
  //       console.log('Comment cannot be empty');
  //       return;
  //     }

  //     const token = await AsyncStorage.getItem('token');
  //     const email = await AsyncStorage.getItem('email');
  //     const name = await AsyncStorage.getItem('name');
  //     const profileImage = await AsyncStorage.getItem('profileImage') || '/default_profile_image.png';
  //     console.log(token, email);
  //     console.log('User Name:', name);

  //     const response = await fetch(`http://10.9.92.25:8080/api/news/${newsId}/comment`, {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Authorization': `Bearer ${token}`,
  //       },
  //       body: JSON.stringify({ text: newComment, user: email }),
  //     });

  //     if (!response.ok) {
  //       const errorMessage = await response.text();
  //       throw new Error(`Failed to submit comment: ${errorMessage}`);
  //     }

  //     const newCommentData = await response.json();
  //     const formattedComment = {
  //       ...newCommentData,
  //       user: {
  //         name: name,
  //         profileImage: profileImage ? `http://10.9.92.25:8080${newCommentData.user.profileImage}` : 'http://10.9.92.25:8080/default_profile_image.png'
  //       },
  //       createdAt: new Date().toISOString()
  //     };
  //     setComments([formattedComment, ...comments]);
  //     setNewComment('');
  //     console.log('Comment successfully submitted:', formattedComment);
  //     console.log(newCommentData)
  //   } catch (error) {
  //     console.error('Error submitting comment:', error.message);
  //     navigation.navigate('SignUpAlert');
  //   }
  // };

  const handleReportComment = async (commentId) => {
    try {
      const token = await AsyncStorage.getItem('token');

      const response = await fetch(`http://10.9.85.243:8080/api/news/${newsId}/comment/${commentId}/report`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });

      if (!response.ok) {
        const errorMessage = await response.text();
        throw new Error(`Failed to report comment: ${errorMessage}`);
      }

      Alert.alert('Comment reported successfully');
      console.log('Comment reported successfully:', commentId);
    } catch (error) {
      console.error('Error reporting comment:', error);
      Alert.alert('Error', 'Failed to report comment. Please try again later.');
    }
  };

  const handleBookmark = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      const email = await AsyncStorage.getItem('email');
      if (!token || !email) {
        throw new Error('User not authenticated');
      }

      const method = isBookmarked ? 'DELETE' : 'POST';

      const response = await fetch(`http://10.9.85.243:8080/api/news/${newsId}/${isBookmarked ? 'unbookmark' : 'bookmark'}`, {
        method,
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });

      if (!response.ok) {
        const errorMessage = await response.text();
        throw new Error(`Failed to ${isBookmarked ? 'remove bookmark' : 'bookmark'}: ${errorMessage}`);
      }

      const newBookmarkStatus = !isBookmarked;
      setIsBookmarked(newBookmarkStatus);

      // Save the bookmark status in AsyncStorage with user-specific keys
      const userBookmarkKey = `bookmark_${email}_${newsId}`;
      await AsyncStorage.setItem(userBookmarkKey, JSON.stringify(newBookmarkStatus));
      console.log(`Bookmark status saved in AsyncStorage: ${newBookmarkStatus}`, newsId);

      if (newBookmarkStatus) {
        const newsDetailsResponse = await fetch(`http://10.9.85.243:8080/api/news/${newsId}`, {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        });

        if (!newsDetailsResponse.ok) {
          const errorMessage = await newsDetailsResponse.text();
          throw new Error(`Failed to fetch news details: ${errorMessage}`);
        }

        const newsDetails = await newsDetailsResponse.json();
        const userNewsKey = `news_${email}_${newsId}`;
        await AsyncStorage.setItem(userNewsKey, JSON.stringify(newsDetails));
        console.log(`News details saved in AsyncStorage for newsId ${newsId}:`, newsDetails);
      } else {
        const userNewsKey = `news_${email}_${newsId}`;
        await AsyncStorage.removeItem(userNewsKey);
        console.log(`News details removed from AsyncStorage for newsId ${newsId}`);
      }

      const successMessage = newBookmarkStatus ? 'Added to Favorites successfully' : 'Removed from Favorites successfully';
      Alert.alert(successMessage);
      console.log(newBookmarkStatus ? 'Bookmarked successfully' : 'Bookmark removed');
    } catch (error) {
      console.error('Error bookmarking:', error);
      Alert.alert('Error', 'Failed to update bookmark status. Please try again later.');
    }
  };

  const handleShowMoreComments = () => {
    setShowAllComments(!showAllComments);
  };

  if (!newsDetails) {
    return (
      <View style={styles.container}>
        <Text>Loading...</Text>
      </View>
    );
  }

  const displayedComments = showAllComments ? comments : comments.slice(0, 4);

  return (
    <View style={styles.container}>
      <Header />
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backButton}>
          <FontAwesome name="chevron-left" size={20} color="#ffa500" />
        </TouchableOpacity>
        <View style={styles.newsImageContainer}>
          {newsDetails[0].news.image ? (
            <Image
              source={{ uri: `http://10.9.85.243:8080/${newsDetails[0].news.image.replace(/\\/g, '/')}` }}
              style={styles.newsImage}
              onLoad={() => console.log('Image loaded successfully')}
              onError={(error) => console.error('Error loading image:', error)}
            />
          ) : (
            <View style={styles.placeholderImage}>
              <Text style={styles.placeholderText}>Image Not Available</Text>
            </View>
          )}
        </View>
        <Text style={styles.newsTitle}>{newsDetails[0].news.title}</Text>

        <View style={styles.reporterContainer}>
          <Text style={styles.reporter}>Reported by {newsDetails[0].news.reporter}</Text>
          <TouchableOpacity onPress={handleBookmark}>
            <FontAwesome name="bookmark" size={20} color={isBookmarked ? "#ffa500" : "#ccc"} />
          </TouchableOpacity>
        </View>

        <Text style={styles.newsDetails}>{newsDetails[0].news.content}</Text>

        <View style={styles.commentInputContainer}>
          <TextInput
            style={styles.commentInput}
            placeholder="Leave a comment"
            value={newComment}
            onChangeText={setNewComment}
          />
          <TouchableOpacity onPress={handleCommentSubmit}>
            <FontAwesome name="send" size={18} color="grey" />
          </TouchableOpacity>
        </View>
        <Text style={styles.readMoreComments}>Comments</Text>
        {displayedComments.length > 0 ? (
          displayedComments.map((comment) => (
            <View key={comment._id} style={styles.commentContainer}>
              {/* <Image source={{ uri: `http://10.9.85.243:8080${comment.user.profileImage}` }} style={styles.commentProfileImage} /> */}
              <Image
                source={comment.user.profileImage ? { uri: `http://10.9.85.243:8080${comment.user.profileImage}` } : require('../assets/profile.png')}
                style={styles.commentProfileImage}
              />
              <View style={styles.commentContent}>
                <View style={styles.commentHeader}>
                  <Text style={styles.commentName}>{comment.user.name}</Text>
                  <Text style={styles.commentTimestamp}>
                    {new Date(comment.createdAt).toLocaleString('en-GB', {
                      day: '2-digit',
                      month: '2-digit',
                      year: 'numeric',
                      hour: '2-digit',
                      minute: '2-digit',
                    })}
                  </Text>
                  <TouchableOpacity onPress={() => handleReportComment(comment._id)}>
                    <Text style={styles.reportComment}>Report</Text>
                  </TouchableOpacity>
                </View>
                <Text style={styles.commentText}>{comment.text}</Text>
              </View>
            </View>
          ))
        ) : (
          <Text style={styles.noCommentsText}>No comments available.</Text>
        )}

        {comments.length > 4 && (
          <TouchableOpacity onPress={handleShowMoreComments}>
            <Text style={styles.showMoreComments}>
              {showAllComments ? 'Show Less' : 'Show More'}
            </Text>
          </TouchableOpacity>
        )}
      </ScrollView>
      <UserBottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingBottom: 20,
  },
  newsImageContainer: {
    alignItems: 'center',
    marginBottom: 16,
  },
  newsImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  placeholderImage: {
    width: '100%',
    height: 200,
    backgroundColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  placeholderText: {
    color: '#fff',
  },
  newsTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  reporterContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  reporter: {
    fontSize: 16,
    color: '#777',
  },
  newsDetails: {
    fontSize: 16,
    lineHeight: 24,
    marginBottom: 16,
  },
  commentInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 8,
    padding: 8,
  },
  commentInput: {
    flex: 1,
    marginRight: 8,
  },
  readMoreComments: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  commentContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
  },
  commentProfileImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 8,
  },
  commentContent: {
    flex: 1,
  },
  commentHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  commentName: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 1,
  },
  commentText: {
    marginBottom: 4,
    fontSize: 14,
    color: '#333',
  },
  commentTimestamp: {
    fontSize: 12,
    color: '#777',
    textAlign: 'center',
  },
  reportComment: {
    color: 'red',
    fontSize: 12,
    marginLeft: 8,
  },
  noCommentsText: {
    textAlign: 'center',
    marginVertical: 16,
    color: '#777',
  },
  showMoreComments: {
    textAlign: 'center',
    color: '#007bff',
    marginTop: 8,
  },
});

export default UserNewsDetailsPage;