// import React, { useState } from 'react';
// import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
// import { useNavigation } from '@react-navigation/native'; // Import useNavigation hook
// import Header from '../components/Header';
// import SearchBarMenu from '../components/searchBarMenu';
// import SignUp from '../screens/signup';
// import UserBottomTabNavigator from '../components/userBottomTabNavigator'; // Adjust the path if necessary

// const LandingPage = () => {
//   const navigation = useNavigation(); // Initialize useNavigation hook

//   const [filteredNewsData, setFilteredNewsData] = useState([]);
//   // Dummy data for news list
//   const newsData = [
//     { id: '1', title: 'Breaking: New Study Shows Benefits of Meditation', image: require('../assets/home1.jpg'), details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' },
//     { id: '2', title: 'Tech Giant Unveils Latest Smartphone Model', image: require('../assets/home1.jpg'), details: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.' },
//     { id: '3', title: 'Sports: Team Wins Championship Title After Decades', image: require('../assets/home1.jpg'), details: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
//     { id: '4', title: 'Entertainment: New Movie Release Breaks Box Office Records', image: require('../assets/home1.jpg'), details: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.' },
//   ];

//   // Function to filter news data based on search term
//   const filterNewsData = (text) => {
//     const filteredData = newsData.filter(item => item.title.toLowerCase().includes(text.toLowerCase()));
//     setFilteredNewsData(filteredData);
//   };

//   // Render item for news list
//   const renderNewsItem = ({ item, index }) => {
//     // Function to truncate text after a certain number of words
//     const truncateText = (text, limit) => {
//       const words = text.split(' ');
//       if (words.length > limit) {
//         return words.slice(0, limit).join(' ') + '...';
//       } else {
//         return text;
//       }
//     };

//     const navigateToNewsDetails = () => {
//       // Navigate to news details screen with the news id or any unique identifier
//       navigation.navigate('UserNewsDetails', { newsId: item.id });
//     };

//     return (
//       <TouchableOpacity onPress={navigateToNewsDetails}>
//         <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
//           <View style={styles.newsImageContainer}>
//             <Image source={item.image} style={styles.newsImage} />
//           </View>
//           <View style={styles.newsDetails}>
//             <Text style={styles.newsTitle}>{item.title}</Text>
//             <Text style={styles.newsDescription}>{truncateText(item.details, 20)}</Text>
//           </View>
//         </View>
//       </TouchableOpacity>
//     );
//   };

//   return (
//     <View style={styles.container}>
//       <Header />

//       {/* Search Bar */}
//       <SearchBarMenu placeholder="Search by news title..." onSearch={filterNewsData} />
//       <ScrollView contentContainerStyle={styles.scrollViewContent}>
//         <View style={styles.content}>
//           {/* Header with Text and Background Image */}
//           <View style={styles.headerWithText}>
//             <Image source={require('../assets/header.jpg')} style={styles.headerBackground} />
//             <Text style={styles.headerText}>Learn What's Happening Across Our College</Text>
//           </View>

//           <FlatList
//             data={filteredNewsData.length ? filteredNewsData : newsData}
//             renderItem={renderNewsItem}
//             keyExtractor={item => item.id}
//           />
//           {/* List of News */}
//         </View>
//       </ScrollView>
//       {/* Bottom Tab Navigator */}
//       <UserBottomTabNavigator navigation={navigation} />
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   scrollViewContent: {
//     flexGrow: 1,
//   },
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//   },
//   content: {
//     flex: 1,
//   },
//   headerWithText: {
//     textAlign: "center",
//     justifyContent: 'center',
//     paddingHorizontal: 20,
//     marginBottom: 20,
//   },
//   headerBackground: {
//     width: '100%',
//     height: 200,
//     resizeMode: 'cover',
//     borderRadius: 10,
//     marginBottom: 10,
//     opacity: 1,
//   },
//   headerText: {
//     fontSize: 24,
//     fontWeight: 'bold',
//     color: '#fff',
//     position: 'absolute',
//     bottom: 20,
//     top: 70,
//     left: 55,
//     textAlign: "center",
//     justifyContent: 'center',
//   },
//   signupRow: {
//     flexDirection: 'row',
//     paddingHorizontal: 20,
//     marginBottom: 20,
//   },
//   leftSide: {
//     flex: 2,
//   },
//   rightSide: {
//     flex: 1,
//     height: 40,
//     width: 20,
//     alignItems: 'center',
//     justifyContent: 'center',
//     borderRadius: 50, // Border radius of 50
//     backgroundColor: '#ffa500',
//   },
//   signupText: {
//     fontSize: 20,
//   },
//   signupButton: {
//     fontSize: 15,
//     color: '#fff',
//     fontWeight: 'bold',
//     paddingVertical: 10,
//     paddingHorizontal: 10,
//   },
//   newsItem: {
//     flexDirection: 'row',
//     alignItems: 'center',
//     paddingHorizontal: 20,
//     height: 150,
//   },
//   newsItemReverse: {
//     flexDirection: 'row-reverse', // Reverse the direction for alternate items
//   },
//   newsImageContainer: {
//     flex: 1,
//     // width: 50,
//     // height: 150,
//   },
//   newsImage: {
//     width: '100%',
//     height: '100%',
//   },
//   newsDetails: {
//     flex: 1,
//   },
//   newsTitle: {
//     paddingHorizontal: 15,
//     paddingTop: 5,
//     fontSize: 13,
//     fontWeight: 'bold',
//   },
//   newsDescription: {
//     paddingHorizontal: 15,
//     paddingTop: 5,
//     fontSize: 11,
//     color: '#555',
//   },
//   bottomTabNavigatorContainer: {
//     position: 'absolute',
//     bottom: 0,
//     left: 0,
//     right: 0,
//   },
// });

// export default LandingPage;
import React, { useState, useEffect } from 'react';
import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Header from '../components/Header';
import SearchBarMenu from '../components/searchBarMenu';
import UserBottomTabNavigator from '../components/userBottomTabNavigator'; // Adjust the path if necessary
import UserNewsDetails from '../screens/userNewsDetails';



const Home = () => {
  const navigation = useNavigation();
  const [news, setNews] = useState([]);
  const [filteredNewsData, setFilteredNewsData] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState('');

  useEffect(() => {
    const fetchApprovedNews = async () => {
      try {
        const response = await fetch("http://10.9.85.243:8080/api/news/approved-news");
        if (!response.ok) {
          throw new Error("Failed to fetch news");
        }
        const data = await response.json();
        const approvedNews = Array.isArray(data.news) ? data.news : [data.news];

        // Sort the news array by upload date in descending order
        approvedNews.sort((a, b) => new Date(b.uploadDate) - new Date(a.uploadDate));

        setNews(approvedNews);
        setFilteredNewsData(approvedNews);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
    };

    fetchApprovedNews();
  }, []);

  // const filterNewsData = async (text) => {
  //   try {
  //     setSearchKeyword(text);
  //     const response = await fetch(`http://10.9.85.243:8080/api/news/search?keyword=${text}`);
  //     if (!response.ok) {
  //       throw new Error("Failed to fetch filtered news");
  //     }
  //     const data = await response.json();
  //     const filteredNews = Array.isArray(data.news) ? data.news : [data.news];
  //     setFilteredNewsData(filteredNews);
  //   } catch (error) {
  //     console.error("Error fetching filtered news:", error);
  //   }
  // };
  const filterNewsData = async (text) => {
    try {
      setSearchKeyword(text);
      const response = await fetch(`http://10.9.85.243:8080/api/news/search?keyword=${text}`);

      if (!response.ok) {
        throw new Error("Failed to fetch filtered news");
      }

      const data = await response.json();
      const filteredNews = Array.isArray(data.news) ? data.news : [data.news];
      setFilteredNewsData(filteredNews);
    } catch (error) {
      console.error("Error fetching filtered news:", error);
      // Handle the error here, you might want to set an error state to display to the user
    }
  };

  // const navigateToNewsDetails = (id) => {
  //   navigation.navigate('NewsDetailsPage', { newsId: id });
  // };
  const navigateToNewsDetails = (id) => {
    console.log("Navigating to news details with id:", id);
    navigation.navigate('UserNewsDetails', { newsId: id });
  };

  const renderNewsItem = ({ item, index }) => {
    return (
      <TouchableOpacity key={item._id} onPress={() => navigateToNewsDetails(item._id)}>
        <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
          <View style={styles.newsImageContainer}>
            {item.image ? (
              <Image
                source={{ uri: `http://10.9.85.243:8080/${item.image.replace(/\\/g, '/')}` }}
                style={styles.newsImage}
                onLoad={() => console.log('Image loaded successfully')}
                onError={(error) => console.error('Error loading image:', error)}
              />
            ) : (
              <View style={styles.placeholderImage}>
                <Text style={styles.placeholderText}>Image Not Available</Text>
              </View>
            )}
          </View>
          <View style={styles.newsDetails}>
            <Text style={styles.newsTitle}>{item.title}</Text>
            {/* <Text style={styles.newsDescription}>{item.content}</Text> */}
            <Text style={styles.newsDescription}>
              {item.content ? item.content.substring(0, 150) : 'No content available'}...
              {/* {item.content ? item.news.content.substring(0, 150) : 'No content available'}... */}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };


  return (
    <View style={styles.container}>
      <Header />
      <SearchBarMenu placeholder="Search by news title..." onSearch={filterNewsData} />
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.content}>
          <View style={styles.headerWithText}>
            <Image source={require('../assets/header.jpg')} style={styles.headerBackground} />
            <Text style={styles.headerText}>Learn What's Happening Across Our College</Text>
          </View>

          <FlatList
            data={filteredNewsData}
            renderItem={renderNewsItem}
            keyExtractor={item => item._id}
          />
        </View>
      </ScrollView >
      <UserBottomTabNavigator navigation={navigation} />
    </View >
  );
};

const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
  },
  headerWithText: {
    textAlign: "center",
    justifyContent: 'center',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  headerBackground: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    borderRadius: 10,
    marginBottom: 10,
    opacity: 1,
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    bottom: 20,
    top: 70,
    left: 55,
    textAlign: "center",
    justifyContent: 'center',
  },
  signupRow: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  leftSide: {
    flex: 2,
  },
  rightSide: {
    flex: 1,
    height: 40,
    width: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: '#ffa500',
  },
  signupText: {
    fontSize: 20,
  },
  signupButton: {
    fontSize: 15,
    color: '#fff',
    fontWeight: 'bold',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  newsItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 150,
  },
  newsItemReverse: {
    flexDirection: 'row-reverse',
  },
  newsImageContainer: {
    flex: 1,
  },
  newsImage: {
    width: '100%',
    height: '100%',
  },
  newsDetails: {
    flex: 1,
  },
  newsTitle: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 13,
    fontWeight: 'bold',
  },
  newsDescription: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 11,
    color: '#555',
  },
  placeholderImage: {
    width: '100%',
    height: '100%',
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
  },
  placeholderText: {
    fontSize: 16,
    color: '#999',
  },
});

export default Home;
