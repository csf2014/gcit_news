import React, { useState, useEffect } from 'react';
import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Header from '../components/Header';
import SearchBarMenu from '../components/searchBarMenu';
import UserBottomTabNavigator from '../components/userBottomTabNavigator';
import NewsDetailsPage from '../screens/newsDetailsPage';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import UserNewsDetails from '../screens/userNewsDetails';
const Clubs = () => {
  const navigation = useNavigation();
  const [clubs, setClubs] = useState([]);
  const [filteredClubs, setFilteredClubs] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState('');

  useEffect(() => {
    const fetchClubs = async () => {
      try {
        const response = await fetch("http://10.9.85.243:8080/api/news/news/clubs");
        if (!response.ok) {
          throw new Error("Failed to fetch clubs");
        }
        const data = await response.json();
        const fetchedClubs = Array.isArray(data.clubs) ? data.clubs : [data.clubs];

        // Sort the clubs array by upload date in descending order
        fetchedClubs.sort((a, b) => new Date(b.uploadDate) - new Date(a.uploadDate));

        setClubs(fetchedClubs);
        setFilteredClubs(fetchedClubs);
      } catch (error) {
        console.error("Error fetching clubs:", error);
      }
    };

    fetchClubs();
  }, []);

  const filterClubs = (text) => {
    setSearchKeyword(text);
    const filteredData = clubs.filter(club =>
      club.title.toLowerCase().includes(text.toLowerCase())
    );
    setFilteredClubs(filteredData);
  };

  const navigateToClubDetails = (id) => {
    console.log("Navigating to club details with id:", id);
    navigation.navigate('UserNewsDetails', { newsId: id });
  };

  const renderClubItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => navigateToClubDetails(item._id)}>
      <View style={[styles.clubItem, index % 2 !== 0 && styles.clubItemReverse]}>
        <View style={styles.clubImageContainer}>
          {item.image ? (
            <Image
              source={{ uri: `http://10.9.85.243:8080/${item.image.replace(/\\/g, '/')}` }}
              style={styles.clubImage}
              onLoad={() => console.log('Image loaded successfully')}
              onError={(error) => console.error('Error loading image:', error)}
            />
          ) : (
            <View style={styles.placeholderImage}>
              <Text style={styles.placeholderText}>Image Not Available</Text>
            </View>
          )}
        </View>
        <View style={styles.clubDetails}>
          <Text style={styles.clubTitle}>{item.title}</Text>
          <Text style={styles.clubDescription}>{item.details}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <Header />
      <SearchBarMenu placeholder="Search by title..." onSearch={filterClubs} />
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.content}>
          <View style={styles.headerWithText}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.leftArrowContainer}>
              <FontAwesome name="chevron-left" size={20} color="#ffa500" />
            </TouchableOpacity>
            <Text style={styles.headerText}>Clubs</Text>
          </View>


          <FlatList
            data={filteredClubs}
            renderItem={renderClubItem}
            keyExtractor={item => item._idid}
          />
        </View>
      </ScrollView>
      <UserBottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
  },
  headerWithText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 20,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333',
  },
  clubItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 150,
    marginBottom: 20,
  },
  clubItemReverse: {
    flexDirection: 'row-reverse', // Reverse the direction for alternate items
  },
  clubImageContainer: {
    flex: 1,
  },
  clubImage: {
    width: '100%',
    height: '100%',
  },
  clubDetails: {
    flex: 1,
  },
  clubTitle: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 13,
    fontWeight: 'bold',
  },
  clubDescription: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 11,
    color: '#555',
  },
  placeholderImage: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ccc',
  },
  placeholderText: {
    fontSize: 12,
    color: '#888',
  },
});

export default Clubs;
