
// // import AsyncStorage from '@react-native-async-storage/async-storage';
// // import React, { useState } from "react";
// // import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
// // import { useNavigation } from '@react-navigation/native';

// // const Login = () => {
// //   const navigation = useNavigation();
// //   const [email, setEmail] = useState("");
// //   const [password, setPassword] = useState("");
// //   const [emailError, setEmailError] = useState("");
// //   const [passwordError, setPasswordError] = useState("");
// //   const navigateToHome = () => {
// //     navigation.navigate('Home');
// //   };

// //   const handleLogin = async () => {
// //     try {
// //       // Validate email and password
// //       if (!email.trim()) {
// //         setEmailError("Please enter your email.");
// //         return;
// //       }
// //       if (!password.trim()) {
// //         setPasswordError("Please enter your password.");
// //         return;
// //       }

// //       // Call backend API to login
// //       const response = await fetch('http://10.9.92.25:8080/api/users/login', {
// //         method: 'POST',
// //         headers: {
// //           'Content-Type': 'application/json',
// //         },
// //         body: JSON.stringify({ email, password }),
// //       });

// //       if (response.ok) {
// //         const data = await response.json();
// //         console.log('Login successful:', data);

// //         // Store user ID and token in AsyncStorage
// //         await AsyncStorage.setItem('email', data.data.user.email); // Assuming the user's email is nested inside data.user.email

// //         // Only store the token if the login is successful
// //         await AsyncStorage.setItem('token', data.token);

// //         console.log('Token stored:', data.token);

// //         // Alert for successful login
// //         alert("Login successful!");

// //         // Redirect to the home page
// //         navigateToHome();
// //       } else {
// //         const errorData = await response.json();
// //         console.error('Login failed:', errorData);
// //         // Handle the error and display a message to the user
// //       }
// //     } catch (error) {
// //       console.error('Error during login:', error);
// //       // Handle any unexpected errors
// //     }
// //   };


// //   return (
// //     <SafeAreaView style={styles.container}>
// //       <View style={styles.innerContainer}>
// //         <Text style={styles.text}>Login</Text>
// //         <TextInput
// //           style={styles.input}
// //           placeholder="Email"
// //           onChangeText={setEmail}
// //           value={email}
// //         />
// //         {emailError ? <Text style={styles.errorText}>{emailError}</Text> : null}
// //         <TextInput
// //           style={styles.input}
// //           placeholder="Password"
// //           onChangeText={setPassword}
// //           value={password}
// //           secureTextEntry={true}
// //         />
// //         {passwordError ? <Text style={styles.errorText}>{passwordError}</Text> : null}
// //         <TouchableOpacity onPress={handleLogin} style={styles.button}>
// //           <Text style={styles.buttonText}>Login</Text>
// //         </TouchableOpacity>
// //       </View>
// //     </SafeAreaView>
// //   );
// // };

// // const styles = StyleSheet.create({
// //   container: {
// //     flex: 1,
// //     backgroundColor: '#fff',
// //     alignItems: 'center',
// //     justifyContent: 'center',
// //   },
// //   innerContainer: {
// //     width: '80%', // Adjust the width to cover most of the screen
// //     borderWidth: 1,
// //     borderColor: "#777",
// //     borderRadius: 20,
// //     padding: 20,
// //   },
// //   input: {
// //     height: 40,
// //     marginBottom: 10,
// //     backgroundColor: "#fff",
// //     borderWidth: 1,
// //     borderColor: "#777",
// //     borderRadius: 20,
// //     paddingHorizontal: 10,
// //   },
// //   text: {
// //     textAlign: 'center',
// //     color: '#ffa500',
// //     fontWeight: 'bold',
// //     fontSize: 18,
// //     marginBottom: 20,
// //   },
// //   button: {
// //     height: 40,
// //     marginBottom: 10,
// //     backgroundColor: "#ffa500",
// //     borderRadius: 20,
// //     justifyContent: 'center',
// //     alignItems: 'center',
// //   },
// //   buttonText: {
// //     color: 'white',
// //     fontSize: 16,
// //   },
// //   signupText: {
// //     textAlign: 'center',
// //     color: '#444',
// //     fontSize: 12,
// //   },
// //   signupLink: {
// //     color: '#ffa500',
// //   },
// //   errorInput: {
// //     borderColor: 'red',
// //   },
// //   errorText: {
// //     color: 'red',
// //     marginLeft: 5,
// //     marginTop: -5,
// //     marginBottom: 5,
// //     fontSize: 11,
// //   },
// // });

// // export default Login;
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import React, { useState } from "react";
// import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
// import { useNavigation } from '@react-navigation/native';

// const Login = () => {
//   const navigation = useNavigation();
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   const [emailError, setEmailError] = useState("");
//   const [passwordError, setPasswordError] = useState("");

//   const navigateToHome = () => {
//     navigation.navigate('Home');
//   };

//   const handleLogin = async () => {
//     try {
//       // Validate email and password
//       if (!email.trim()) {
//         setEmailError("Please enter your email.");
//         return;
//       }
//       if (!password.trim()) {
//         setPasswordError("Please enter your password.");
//         return;
//       }

//       // Call backend API to login
//       const response = await fetch('http://10.9.92.25:8080/api/users/login', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify({ email, password }),
//       });

//       if (response.ok) {
//         const data = await response.json();
//         console.log('Login successful:', data);

//         // Store user email and token in AsyncStorage
//         await AsyncStorage.setItem('email', data.data.user.email); // Assuming the user's email is nested inside data.data.user.email
//         await AsyncStorage.setItem('token', data.token);

//         // Print the token and email
//         console.log('Token stored:', data.token);
//         console.log('User email:', data.data.user.email);

//         // Alert for successful login
//         alert("Login successful!");

//         // Redirect to the home page
//         navigateToHome();
//       } else {
//         const errorData = await response.json();
//         console.error('Login failed:', errorData);
//         // Handle the error and display a message to the user
//       }
//     } catch (error) {
//       console.error('Error during login:', error);
//       // Handle any unexpected errors
//     }
//   };

//   return (
//     <SafeAreaView style={styles.container}>
//       <View style={styles.innerContainer}>
//         <Text style={styles.text}>Login</Text>
//         <TextInput
//           style={styles.input}
//           placeholder="Email"
//           onChangeText={(text) => {
//             setEmail(text);
//             setEmailError(''); // Clear error when user starts typing
//           }}
//           value={email}
//         />
//         {emailError ? <Text style={styles.errorText}>{emailError}</Text> : null}
//         <TextInput
//           style={styles.input}
//           placeholder="Password"
//           onChangeText={(text) => {
//             setPassword(text);
//             setPasswordError(''); // Clear error when user starts typing
//           }}
//           value={password}
//           secureTextEntry={true}
//         />
//         {passwordError ? <Text style={styles.errorText}>{passwordError}</Text> : null}
//         <TouchableOpacity onPress={handleLogin} style={styles.button}>
//           <Text style={styles.buttonText}>Login</Text>
//         </TouchableOpacity>
//       </View>
//     </SafeAreaView>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   innerContainer: {
//     width: '80%',
//     borderWidth: 1,
//     borderColor: "#777",
//     borderRadius: 20,
//     padding: 20,
//   },
//   input: {
//     height: 40,
//     marginBottom: 10,
//     backgroundColor: "#fff",
//     borderWidth: 1,
//     borderColor: "#777",
//     borderRadius: 20,
//     paddingHorizontal: 10,
//   },
//   text: {
//     textAlign: 'center',
//     color: '#ffa500',
//     fontWeight: 'bold',
//     fontSize: 18,
//     marginBottom: 20,
//   },
//   button: {
//     height: 40,
//     marginBottom: 10,
//     backgroundColor: "#ffa500",
//     borderRadius: 20,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   buttonText: {
//     color: 'white',
//     fontSize: 16,
//   },
//   errorText: {
//     color: 'red',
//     marginLeft: 5,
//     marginTop: -5,
//     marginBottom: 5,
//     fontSize: 11,
//   },
// });

// export default Login;
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState } from "react";
import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
import { useNavigation } from '@react-navigation/native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
const Login = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const navigateToHome = () => {
    navigation.navigate('Home');
  };

  const handleLogin = async () => {
    try {
      // Validate email and password
      if (!email.trim()) {
        setEmailError("Please enter your email.");
        return;
      }
      if (!password.trim()) {
        setPasswordError("Please enter your password.");
        return;
      }

      // Call backend API to login
      const response = await fetch('http://10.9.85.243:8080/api/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log('Login successful:', data);

        // Store user email, name, and token in AsyncStorage
        await AsyncStorage.setItem('email', data.data.user.email); // Assuming the user's email is nested inside data.data.user.email
        await AsyncStorage.setItem('name', data.data.user.name); // Assuming the user's name is nested inside data.data.user.name
        await AsyncStorage.setItem('token', data.token);

        // Print the token, email, and name
        console.log('Token stored:', data.token);
        console.log('User email:', data.data.user.email);
        console.log('User name:', data.data.user.name);

        // Alert for successful login
        alert("Login successful!");

        // Redirect to the home page
        navigateToHome();
      } else {
        const errorData = await response.json();
        console.error('Login failed:', errorData);
        // Handle the error and display a message to the user
      }
    } catch (error) {
      console.error('Error during login:', error);
      // Handle any unexpected errors
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backButton}>
        <FontAwesome name="chevron-left" size={20} color="#ffa500" />
      </TouchableOpacity>
      <View style={styles.innerContainer}>
        <Text style={styles.text}>Login</Text>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={(text) => {
            setEmail(text);
            setEmailError(''); // Clear error when user starts typing
          }}
          value={email}
        />
        {emailError ? <Text style={styles.errorText}>{emailError}</Text> : null}
        <TextInput
          style={styles.input}
          placeholder="Password"
          onChangeText={(text) => {
            setPassword(text);
            setPasswordError(''); // Clear error when user starts typing
          }}
          value={password}
          secureTextEntry={true}
        />
        {passwordError ? <Text style={styles.errorText}>{passwordError}</Text> : null}
        <TouchableOpacity onPress={handleLogin} style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainer: {
    width: '80%',
    borderWidth: 1,
    borderColor: "#777",
    borderRadius: 20,
    padding: 20,
  },
  input: {
    height: 40,
    marginBottom: 10,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#777",
    borderRadius: 20,
    paddingHorizontal: 10,
  },
  text: {
    textAlign: 'center',
    color: '#ffa500',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 20,
  },
  backButton: {
    padding: 10,
    position: 'absolute',
    top: 0,
    left: 0,
  },
  button: {
    height: 40,
    marginBottom: 10,
    backgroundColor: "#ffa500",
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  errorText: {
    color: 'red',
    marginLeft: 5,
    marginTop: -5,
    marginBottom: 5,
    fontSize: 11,
  },
});

export default Login;
