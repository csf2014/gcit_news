
import React, { useState, useEffect } from 'react';
import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Header from '../components/Header';
import SearchBarMenu from '../components/searchBarMenu';
import UserBottomTabNavigator from '../components/userBottomTabNavigator';
import NewsDetailsPage from '../screens/newsDetailsPage';
import UserNewsDetails from '../screens/userNewsDetails';

const Announcement = () => {
  const navigation = useNavigation();
  const [announcements, setAnnouncements] = useState([]);
  const [filteredAnnouncements, setFilteredAnnouncements] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState('');

  useEffect(() => {
    const fetchAnnouncements = async () => {
      try {
        const response = await fetch("http://10.9.85.243:8080/api/news/news/announcements");
        if (!response.ok) {
          throw new Error("Failed to fetch announcements");
        }
        const data = await response.json();
        const fetchedAnnouncements = Array.isArray(data.announcements) ? data.announcements : [data.announcements];

        // Sort the announcements array by upload date in descending order
        fetchedAnnouncements.sort((a, b) => new Date(b.uploadDate) - new Date(a.uploadDate));

        setAnnouncements(fetchedAnnouncements);
        setFilteredAnnouncements(fetchedAnnouncements);
      } catch (error) {
        console.error("Error fetching announcements:", error);
      }
    };

    fetchAnnouncements();
  }, []);

  const filterAnnouncements = (text) => {
    setSearchKeyword(text);
    const filteredData = announcements.filter(announcement =>
      announcement.title.toLowerCase().includes(text.toLowerCase())
    );
    setFilteredAnnouncements(filteredData);
  };

  const navigateToAnnouncementDetails = (id) => {
    console.log("Navigating to announcement details with id:", id);
    navigation.navigate('UserNewsDetails', { newsId: id });
  };

  const renderAnnouncementItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => navigateToAnnouncementDetails(item._id)}>
      <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
        <View style={styles.newsImageContainer}>
          {/* <Image source={{ uri: item.image }} style={styles.newsImage} /> */}
          {item.image ? (
            <Image
              source={{ uri: `http://10.9.85.243:8080/${item.image.replace(/\\/g, '/')}` }}
              style={styles.newsImage}
              onLoad={() => console.log('Image loaded successfully')}
              onError={(error) => console.error('Error loading image:', error)}
            />
          ) : (
            <View style={styles.placeholderImage}>
              <Text style={styles.placeholderText}>Image Not Available</Text>
            </View>
          )}
        </View>
        <View style={styles.newsDetails}>
          <Text style={styles.newsTitle}>{item.title}</Text>
          <Text style={styles.newsDescription}>{item.details}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <Header />
      <SearchBarMenu placeholder="Search by title..." onSearch={filterAnnouncements} />
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.content}>
          <View style={styles.headerWithText}>
            <Text style={styles.headerText}>Announcements</Text>
          </View>
          <FlatList
            data={filteredAnnouncements}
            renderItem={renderAnnouncementItem}
            keyExtractor={item => item.id}
          />
        </View>
      </ScrollView>
      <UserBottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
  },
  headerWithText: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25,
    marginTop: 5,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333',
  },
  newsItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 150,
    marginBottom: 20,
  },
  newsItemReverse: {
    flexDirection: 'row-reverse', // Reverse the direction for alternate items
  },
  newsImageContainer: {
    flex: 1,
  },
  newsImage: {
    width: '100%',
    height: '100%',
  },
  newsDetails: {
    flex: 1,
  },
  newsTitle: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 13,
    fontWeight: 'bold',
  },
  newsDescription: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 11,
    color: '#555',
  },
});

export default Announcement;
