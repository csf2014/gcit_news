import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Header = () => {
    const currentDate = new Date();
    const options = {
      weekday: "long",
      month: "long",
      day: "numeric",
      year: "numeric"
    };
    const formattedDate = currentDate.toLocaleDateString(undefined, options);  
    return (
        <View style={styles.container}>
            <Text style={styles.title}>GCIT NEWS</Text>
            <Text style={styles.date}>{formattedDate}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  title: {
    fontSize: 28,
    color: "#008000",
    margin: 5,
    fontWeight: 'bold',
  },
  date: {
    backgroundColor: "#ffae1a",
    textAlign: "center",
    padding: 5,
    width: "100%",
    fontSize: 13,
    color: "#FFFFFF",
  },
});

export default Header;
