import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const BottomTabNavigator = ({ navigation }) => {
  const navigateToSignupAlert = () => {
    navigation.navigate('SignUpAlert');
  };
  const navigateToLandingPage = () => {
    navigation.navigate('LandingPage');
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToLandingPage}>
        <FontAwesome name="home" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToSignupAlert}>
        <FontAwesome name="bullhorn" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToSignupAlert}>
        <FontAwesome name="edit" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToSignupAlert}>
        <FontAwesome name="th" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToSignupAlert}>
        <FontAwesome name="cog" size={24} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#ffa500',
    paddingBottom: 15,
    paddingTop: 15,
  },
  tabItem: {
    alignItems: 'center',
  },
});

export default BottomTabNavigator;
