import React, { useState } from 'react';
import { TextInput, StyleSheet, View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; // Import Ionicons from Expo icons

const SearchBarMenu = ({ placeholder, onSearch }) => {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearchChange = (text) => {
    setSearchTerm(text);
    onSearch(text);
  };

  return (
    <View style={styles.searchBarContainer}>
      <TextInput
        placeholder={placeholder}
        style={styles.searchBar}
        value={searchTerm}
        onChangeText={handleSearchChange}
      />
      <TouchableOpacity style={styles.searchIcon} onPress={() => onSearch(searchTerm)}>
        <Ionicons name="search" size={20} color="#ffa500" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  searchBarContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: '#ccc',
    marginHorizontal: 20,
    marginBottom: 15,
    marginTop: 15,
    borderRadius: 50,
  },
  searchBar: {
    flex: 1,
    marginRight: 0,
  },
  searchIcon: {
    padding: 0,
  },
});

export default SearchBarMenu;
