import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const UserBottomTabNavigator = ({ navigation }) => {
  const navigateToHome = () => {
    navigation.navigate('Home');
  };
  const navigateToAnnouncement = () => {
    navigation.navigate('Announcement');
  };
  const navigateToCreateNews = () => {
    navigation.navigate('CreateNews');
  };
  const navigateToCategories = () => {
    navigation.navigate('Categories');
  };
  const navigateToSetting = () => {
    navigation.navigate('Setting');
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToHome}>
        <FontAwesome name="home" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToAnnouncement}>
        <FontAwesome name="bullhorn" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToCreateNews}>
        <FontAwesome name="edit" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToCategories}>
        <FontAwesome name="th" size={24} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={navigateToSetting}>
        <FontAwesome name="cog" size={24} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#ffa500',
    paddingBottom: 15,
    paddingTop: 15,
  },
  tabItem: {
    alignItems: 'center',
  },
});

export default UserBottomTabNavigator;
