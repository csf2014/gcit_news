import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AdminHome from "./pages/adminHome";
import AdminUser from "./pages/adminUser"; // Make sure the import statement is correct
import AdminNews from "./pages/adminNews";
import AdminReport from "./pages/adminReport";
import AdminLogin from "./pages/adminLogin";
import AdminAddNewsPage from "./pages/adminAddNewsPage";
import AdminNewsDetails from "./pages/adminNewsDetails";

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<AdminLogin />} />
        <Route path="/home" element={<AdminHome />} />
        <Route path="/users" element={<AdminUser />} /> {/* Check the import statement here */}
        <Route path="/news-request" element={<AdminNews />} />
        <Route path="/add-news" element={<AdminAddNewsPage />} />
        <Route path="/admin/news/:id" element={<AdminNewsDetails />} />
        <Route path="/reported-comments" element={<AdminReport />} />
      </Routes>
    </Router>
  );
};

export default App;
