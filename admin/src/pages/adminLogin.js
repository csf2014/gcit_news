// import React, { useState } from "react";
// import { useNavigate } from "react-router-dom";

// const AdminLogin = () => {
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   const [emailError, setEmailError] = useState("");
//   const [passwordError, setPasswordError] = useState("");
//   const navigate = useNavigate(); // Initialize useNavigate

//   const doLogin = async () => {
//     // Validate email and password
//     if (!email.trim()) {
//       setEmailError("Email cannot be empty.");
//       return;
//     } else {
//       setEmailError("");
//     }

//     if (!password.trim()) {
//       setPasswordError("Password cannot be empty.");
//       return;
//     } else {
//       setPasswordError("");
//     }
//     try {
//       const response = await fetch('http://localhost:8080/api/users/admin/login', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify({ email, password }),
//       });

//       if (response.ok) {
//         // Redirect to home page after successful login
//         navigate("/home");
//       } else {
//         // Handle error response
//         const errorMessage = await response.text();
//         console.error('Login failed:', errorMessage);
//         // Display error message to the user, you can set it to state
//       }
//     } catch (error) {
//       console.error('Error during login:', error);
//       // Handle network errors or other exceptions
//       // Display error message to the user
//     }


//     // Perform login logic here
//     // For now, let's just log the email and password
//     console.log("Logging in with", email, "and", password);

//     // Redirect to home page after login
//     navigate("/home");
//   };

//   // Rest of the component remains the same
//   return (
//     <div style={{
//       display: "flex",
//       alignItems: "center",
//       justifyContent: "center",
//       height: "100vh",
//       backgroundColor: "#fff",
//     }}>
//       <div style={{
//         width: '20%',
//         border: "1px solid #42506B",
//         borderRadius: "20px",
//         padding: "20px",
//       }}>
//         <h2 style={{
//           textAlign: "center",
//           color: "#42506B",
//           fontWeight: "bold",
//           fontSize: "18px",
//           marginTop: "0px",
//         }}>Login</h2>

//         <input
//           style={{
//             width: '93%',
//             height: "35px",
//             marginBottom: "15px",
//             backgroundColor: "#fff",
//             border: "1px solid #42506B",
//             borderRadius: "20px",
//             padding: "0 10px",
//             ...(emailError && { borderColor: "red" })
//           }}
//           value={email}
//           placeholder="Email Address"
//           onChange={(e) => setEmail(e.target.value)}
//         />
//         {emailError && <p style={{ color: "red", marginLeft: "5px", marginTop: "-10px", marginBottom: "10px", fontSize: "11px" }}>{emailError}</p>}

//         <input
//           style={{
//             width: '93%',
//             height: "35px",
//             marginBottom: "15px",
//             backgroundColor: "#fff",
//             border: "1px solid #42506B",
//             borderRadius: "20px",
//             padding: "0 10px",
//             ...(passwordError && { borderColor: "red" })
//           }}
//           value={password}
//           placeholder="Password"
//           type="password"
//           onChange={(e) => setPassword(e.target.value)}
//         />
//         {passwordError && <p style={{ color: "red", marginLeft: "5px", marginTop: "-10px", marginBottom: "10px", fontSize: "11px" }}>{passwordError}</p>}

//         <button onClick={doLogin} style={{
//           width: '100%',
//           height: "37px",
//           marginBottom: "10px",
//           backgroundColor: "#42506B",
//           borderRadius: "20px",
//           color: "white",
//           border: "none",
//           cursor: "pointer",
//         }}>Login</button>

//       </div>
//     </div>
//   );
// };

// export default AdminLogin;
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const AdminLogin = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const navigate = useNavigate(); // Initialize useNavigate

  const doLogin = async () => {
    // Validate email and password
    if (!email.trim()) {
      setEmailError("Email cannot be empty.");
      return;
    } else {
      setEmailError("");
    }

    if (!password.trim()) {
      setPasswordError("Password cannot be empty.");
      return;
    } else {
      setPasswordError("");
    }

    try {
      // const response = await fetch('http://10.9.92.25:8080/api/users/admin/login', {
      const response = await fetch('http://10.9.85.243:8080/api/users/admin/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (response.ok) {
        // Get the token from the response
        const { token } = await response.json();

        // Store the token in local storage
        localStorage.setItem('token', token);

        // Log the token
        console.log('Token:', token);

        // Redirect to home page after successful login
        navigate("/home");
      } else {
        // Handle error response
        const errorMessage = await response.text();
        console.error('Login failed:', errorMessage);
        // Display error message to the user, you can set it to state
      }
    } catch (error) {
      console.error('Error during login:', error);
      // Handle network errors or other exceptions
      // Display error message to the user
    }
  };

  // Rest of the component remains the same
  return (
    <div style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      height: "100vh",
      backgroundColor: "#fff",
    }}>
      <div style={{
        width: '20%',
        border: "1px solid #42506B",
        borderRadius: "20px",
        padding: "20px",
      }}>
        <h2 style={{
          textAlign: "center",
          color: "#42506B",
          fontWeight: "bold",
          fontSize: "18px",
          marginTop: "0px",
        }}>Login</h2>

        <input
          style={{
            width: '93%',
            height: "35px",
            marginBottom: "15px",
            backgroundColor: "#fff",
            border: "1px solid #42506B",
            borderRadius: "20px",
            padding: "0 10px",
            ...(emailError && { borderColor: "red" })
          }}
          value={email}
          placeholder="Email Address"
          onChange={(e) => setEmail(e.target.value)}
        />
        {emailError && <p style={{ color: "red", marginLeft: "5px", marginTop: "-10px", marginBottom: "10px", fontSize: "11px" }}>{emailError}</p>}

        <input
          style={{
            width: '93%',
            height: "35px",
            marginBottom: "15px",
            backgroundColor: "#fff",
            border: "1px solid #42506B",
            borderRadius: "20px",
            padding: "0 10px",
            ...(passwordError && { borderColor: "red" })
          }}
          value={password}
          placeholder="Password"
          type="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        {passwordError && <p style={{ color: "red", marginLeft: "5px", marginTop: "-10px", marginBottom: "10px", fontSize: "11px" }}>{passwordError}</p>}

        <button onClick={doLogin} style={{
          width: '100%',
          height: "37px",
          marginBottom: "10px",
          backgroundColor: "#ffa500",
          borderRadius: "20px",
          color: "white",
          border: "none",
          cursor: "pointer",
        }}>Login</button>

      </div>
    </div>
  );
};

export default AdminLogin;
