
import React from "react";
import { Link } from "react-router-dom";
import AdminHeader from "../components/adminHeader";
import backgroundImage1 from "../assets/home1.jpg";
import backgroundImage2 from "../assets/home2.jpg";
import backgroundImage3 from "../assets/home3.jpg";

const AdminHome = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <AdminHeader />
      <div style={{ display: "flex", justifyContent: "space-between", marginLeft: "250px", marginRight: "250px", marginTop: "50px" }}>

        <Link to="/users" style={{ textDecoration: "none" }}>
          <ClickableBox backgroundImage={backgroundImage1} text="Users" />
        </Link>
        <Link to="/news-request" style={{ textDecoration: "none" }}>
          <ClickableBox backgroundImage={backgroundImage2} text="News Requests" />
        </Link>
        <Link to="/reported-comments" style={{ textDecoration: "none" }}>
          <ClickableBox backgroundImage={backgroundImage3} text="Reported Comments" />
        </Link>
      </div>
    </div>
  );
};

const ClickableBox = ({ backgroundImage, text }) => {
  return (
    <div
      style={{
        flex: 1,
        margin: "10px",
        width: 345,
        height: 450,
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: "cover",
        cursor: "pointer",
        overflow: "hidden",
      }}
    >
      <div
        style={{
          marginTop: "320px",
          textAlign: "right",
          fontSize: "25px",
          fontWeight: "bold",
          color: "#333",
          top: 0,
          width: "100%",
          backgroundColor: "rgba(255,255,255, 0.6)",
          padding: "10px",
        }}
      >
        <p style={{ margin: 20 }}>{text}</p>
      </div>
    </div>
  );
};

export default AdminHome;
