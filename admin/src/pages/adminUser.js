
import React, { useState, useEffect } from "react";
import { FaSearch } from "react-icons/fa"; // Import the search icon
import AdminHeader from "../components/adminHeader";
import AdminNavigation from "../components/adminNavigation";
import AdminDeleteUserConfirmation from "../components/adminDeleteUserConfirmation";

const AdminUser = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [users, setUsers] = useState([]); // Initialize users state as an empty array
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [userToDelete, setUserToDelete] = useState(null);

  // useEffect(() => {
  //   const fetchUsers = async () => {
  //     try {
  //       const token = localStorage.getItem('token');
  //       const response = await fetch("http://localhost:8080/api/users/getusers", {
  //         headers: {
  //           Authorization: `Bearer ${token}`,
  //         },
  //       });
  //       if (!response.ok) {
  //         throw new Error("Failed to fetch users");
  //       }
  //       const userData = await response.json();
  //       setUsers(userData.data); // Assuming userData is structured with a 'data' property containing the user array
  //     } catch (error) {
  //       console.error("Error fetching users:", error);
  //     }
  //   };

  //   fetchUsers();
  // }, []);
  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const token = localStorage.getItem('token');
        let url = "http://localhost:8080/api/users/getusers";

        // If searchTerm exists, append it to the URL as a query parameter
        if (searchTerm) {
          url = `http://localhost:8080/api/users/search?name=${searchTerm}`;
        }

        const response = await fetch(url, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (!response.ok) {
          throw new Error("Failed to fetch users");
        }
        const userData = await response.json();

        // If searchTerm exists, sort the users array to bring the matching user(s) to the top
        if (searchTerm) {
          const matchingUser = userData.data.find(user => user.name.toLowerCase() === searchTerm.toLowerCase());
          const remainingUsers = userData.data.filter(user => user.name.toLowerCase() !== searchTerm.toLowerCase());
          setUsers([matchingUser, ...remainingUsers]);
        } else {
          setUsers(userData.data);
        }
      } catch (error) {
        console.error("Error fetching users:", error);
      }
    };

    fetchUsers();
  }, [searchTerm]);


  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleDelete = (userId) => {
    setUserToDelete(userId);
    setShowDeleteModal(true);
  };


  const handleConfirmDelete = async () => {
    try {
      const token = localStorage.getItem('token');
      const response = await fetch(`http://10.9.85.243:8080/api/users/user/${userToDelete}`, {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (!response.ok) {
        throw new Error('Failed to delete user');
      }

      // If the deletion is successful, remove the user from the state
      const updatedUsers = users.filter((user) => user._id !== userToDelete);
      setUsers(updatedUsers);
      alert('User deleted successfully');
      console.log('User deleted successfully');
    } catch (error) {
      console.error('Error deleting user:', error);
      // Handle error, show message to user, etc.
    } finally {
      // Reset userToDelete and hide the confirmation modal
      setUserToDelete(null);
      setShowDeleteModal(false);
    }
  };

  const handleCancelDelete = () => {
    // Reset userToDelete and hide the confirmation modal
    setUserToDelete(null);
    setShowDeleteModal(false);
  };



  // Styles for the search bar container
  const searchBarContainerStyle = {
    width: "78%",
    marginLeft: "166px",
    marginTop: "20px",
    marginBottom: "20px",
    display: "flex",
    alignItems: "center",
    position: "relative", // Position the container relatively for the absolute positioning of the search icon
  };

  // Styles for the search bar input
  const searchBarInputStyle = {
    padding: "12px 25px",
    borderRadius: "100px",
    border: "1px solid #ccc",
    marginRight: "0px",
    width: "calc(100% - 40px)", // Adjust width to leave space for the search icon
  };

  // Styles for the search icon
  const searchIconStyle = {
    position: "absolute", // Position the search icon absolutely
    right: "30px", // Adjust the right position to align with the end of the input
    cursor: "pointer",
    fontSize: "17px",
    color: "#ffa500"
  };

  // Styles for the table
  const tableStyle = {
    width: "78%",
    marginLeft: "170px",
    borderCollapse: "collapse"
  };

  // Styles for table headers
  const tableHeaderStyle = {
    backgroundColor: "#fff",
    color: "#333",
    fontSize: "18px",
    borderBottom: "2px solid #ccc",
    width: "25%" // Set a fixed width for each header cell
  };

  // Styles for table cells
  const tableCellStyle = {
    padding: "10px",
    textAlign: "left",
    borderBottom: "1px solid #ccc"
  };

  // Styles for delete button
  const deleteButtonStyle = {
    width: '130px',
    height: '40px',
    padding: "6px 12px",
    borderRadius: "5px",
    backgroundColor: "red",
    color: "#fff",
    border: 'none',
  };
  return (
    <div>
      <AdminHeader />
      <AdminNavigation />
      <div style={searchBarContainerStyle}>
        <input
          type="text"
          placeholder="Search users..."
          value={searchTerm}
          onChange={handleSearchChange}
          style={searchBarInputStyle}
        />
        <FaSearch style={searchIconStyle} onClick={() => console.log("Searching...")} />
      </div>
      <table style={tableStyle}>
        <thead>
          <tr style={tableHeaderStyle}>
            <th style={tableCellStyle}>Name</th>
            <th style={tableCellStyle}>Email Address</th>
            <th style={tableCellStyle}>Registration Date</th>
            <th style={tableCellStyle}></th>
          </tr>
        </thead>
        <tbody>


          {users.map((user) => (
            user && ( // Add a null check before accessing properties of user
              <tr key={user._id} style={{ borderBottom: "1px solid #ccc" }}>
                <td style={tableCellStyle}>{user.name}</td>
                <td style={tableCellStyle}>{user.email}</td>
                {/* <td style={tableCellStyle}>{user.registrationDate}</td> */}
                <td style={{ padding: "10px", textAlign: "left", borderBottom: "1px solid #ccc" }}>{new Date(user.registrationDate).toLocaleDateString('en-GB')}</td>
                <td style={{ ...tableCellStyle, textAlign: 'right' }}>
                  <button onClick={() => handleDelete(user._id)} style={deleteButtonStyle}>Delete User</button>
                </td>
              </tr>
            )
          ))}



        </tbody>
      </table>
      {showDeleteModal && (
        <AdminDeleteUserConfirmation
          onConfirm={handleConfirmDelete}
          onCancel={handleCancelDelete}
        />
      )}
    </div>
  );
};

export default AdminUser;

