

// import React, { useState, useEffect } from "react";
// import AdminHeader from "../components/adminHeader";
// import AdminNavigation from "../components/adminNavigation";
// import AdminApproveNewsConfirmation from "../components/adminApproveNewsConfirmation";
// import { Link } from "react-router-dom"; // Import Link component
// import AdminDeleteNewsConfirmation from "../components/adminDeleteNewsConfirmation";
// const AdminNews = () => {
//   const [unapprovedNews, setUnapprovedNews] = useState([]);
//   const [showApproveModal, setShowApproveModal] = useState(false);
//   const [newsIdToApprove, setNewsIdToApprove] = useState(null);
//   const [news, setNews] = useState([]);
//   const [showDeleteModal, setShowDeleteModal] = useState(false);
//   const [newsToDelete, setNewsToDelete] = useState(null); // Added state for news to delete


//   useEffect(() => {
//     const fetchUnapprovedNews = async () => {
//       try {
//         const token = localStorage.getItem("token"); // Assuming you store the token in localStorage after login
//         const response = await fetch("http://10.9.92.25:8080/api/news/unapproved", {
//           method: "GET",
//           headers: {
//             "Authorization": `Bearer ${token}`,
//             "Content-Type": "application/json"
//           }
//         });
//         if (!response.ok) {
//           throw new Error("Failed to fetch unapproved news");
//         }
//         const data = await response.json();
//         setUnapprovedNews(data);
//       } catch (error) {
//         console.error("Error fetching unapproved news:", error);
//       }
//     };

//     fetchUnapprovedNews();
//   }, []);

//   useEffect(() => {
//     const fetchAllNews = async () => {
//       try {
//         const response = await fetch("http://10.9.92.25:8080/api/news");
//         if (!response.ok) {
//           throw new Error("Failed to fetch news");
//         }
//         const data = await response.json();
//         console.log("Fetched data:", data); // Log the fetched data
//         const newsArray = Array.isArray(data.news) ? data.news : [data.news]; // Ensure data.news is always an array
//         setNews(newsArray);
//       } catch (error) {
//         console.error("Error fetching news:", error);
//       }
//     };

//     fetchAllNews();
//   }, []);

//   const handleApprove = (newsId) => {
//     console.log("News ID to approve:", newsId);
//     setNewsIdToApprove(newsId);
//     setShowApproveModal(true);
//   };

//   const handleConfirmApprove = async () => {
//     try {
//       const token = localStorage.getItem("token");
//       const response = await fetch(`http://10.9.92.25:8080/api/news/${newsIdToApprove}/approve`, {
//         method: "PATCH",
//         headers: {
//           "Authorization": `Bearer ${token}`,
//           "Content-Type": "application/json"
//         },
//         body: JSON.stringify({ approved: true }) // Include the news ID in the request body
//       });
//       if (!response.ok) {
//         throw new Error("Failed to approve news");
//       }

//       // Remove the approved news from the unapprovedNews state
//       const updatedNews = unapprovedNews.filter(news => news._id !== newsIdToApprove);
//       setUnapprovedNews(updatedNews);

//       console.log("News approved successfully");
//       setShowApproveModal(false);
//     } catch (error) {
//       console.error("Error approving news:", error);
//     }
//   };

//   const handleCancelApprove = () => {
//     setShowApproveModal(false);
//   };
//   const handleDelete = (newsId) => {
//     setNewsToDelete(newsId);
//     setShowDeleteModal(true);
//   };



//   const handleConfirmDelete = async () => {
//     try {
//       const token = localStorage.getItem("token");
//       const response = await fetch(`http://10.9.92.25:8080/api/news/${newsToDelete}`, {
//         method: "DELETE",
//         headers: {
//           "Authorization": `Bearer ${token}`,
//           "Content-Type": "application/json"
//         }
//       });

//       // Remove the deleted news item from the news array immediately
//       setNews(news.filter((newsItem) => newsItem._id !== newsToDelete));

//       // Show a success message
//       alert("News deleted successfully");

//       console.log("News deleted successfully");

//     } catch (error) {
//       console.error("Error deleting news:", error);
//     } finally {
//       // Reset the state variables
//       setNewsToDelete(null);
//       setShowDeleteModal(false);
//     }
//   };


//   const handleCancelDelete = () => {
//     setNewsToDelete(null);
//     setShowDeleteModal(false);
//   };

//   return (
//     <div>
//       <AdminHeader />
//       <AdminNavigation />
//       <div style={{ marginBottom: "20px", textAlign: "right" }}>
//         <Link to="/add-news">
//           <button style={buttonStyle}>Add News</button>
//         </Link>
//       </div>
//       <table style={tableStyle}>
//         <thead>
//           <tr style={tableHeaderStyle}>
//             <th style={tableCellStyle}>User</th>
//             <th style={tableCellStyle}>News</th>
//             <th style={tableCellStyle}></th>
//           </tr>
//         </thead>
//         <tbody>
//           {unapprovedNews.map((news) => (
//             <tr key={news._id} style={{ borderBottom: "1px solid #ccc" }}>
//               <td style={tableCellStyle}>
//                 {news.image && (
//                   <img
//                     src={`http://localhost:8080/${news.image}`} // Construct the image URL dynamically based on the image path
//                     alt="News Image"
//                     style={{ width: "50px", height: "50px", borderRadius: "50%" }}
//                     onLoad={() => console.log('Image loaded successfully')}
//                     onError={(error) => {
//                       console.error('Error loading image:', error);
//                       // Handle the error gracefully (e.g., display a placeholder image)
//                     }}

//                   />
//                 )}
//               </td>
//               <td style={tableCellStyle}>
//                 <p><strong>{news.newsTitle}. {news.date}</strong></p>
//                 <p>{news.content}</p>
//               </td>
//               <td style={{ ...tableCellStyle, textAlign: 'right' }}>
//                 <button onClick={() => handleApprove(news._id)} style={approveButtonStyle}>Approve News</button>
//               </td>
//             </tr>
//           ))}
//         </tbody>
//       </table>

//       {showApproveModal && (
//         <AdminApproveNewsConfirmation
//           onConfirm={handleConfirmApprove}
//           onCancel={handleCancelApprove}
//         />
//       )}

//       <h2 style={{ textAlign: "center" }}>News</h2>
//       <div style={{ maxHeight: "500px", overflowY: "auto" }}>
//         <table style={tableStyle}>
//           <thead>
//             <tr style={tableHeaderStyle}>
//               <th style={tableCellStyle}>Title</th>
//               <th style={tableCellStyle}>Reporter</th>
//               <th style={tableCellStyle}>Content</th>
//               <th style={tableCellStyle}>Image</th>
//               <th style={tableCellStyle}>Category</th>
//               <th style={tableCellStyle}>Approved</th>
//               <th style={tableCellStyle}>Delete</th>
//             </tr>
//           </thead>
//           <tbody>

//             {news.map((newsItem) => (
//               <tr key={newsItem._id} style={{ borderBottom: "1px solid #ccc" }}>
//                 <td style={tableCellStyle}>{newsItem.title}</td>
//                 <td style={tableCellStyle}>{newsItem.reporter}</td>
//                 <td style={tableCellStyle}>{newsItem.content}</td>
//                 <td style={tableCellStyle}>
//                   {/* Display the image using the image URL */}
//                   {newsItem.image && (
//                     <img
//                       src={`http://localhost:8080/${newsItem.image}`} // Construct the image URL dynamically based on the image path
//                       alt="News Image"
//                       style={{ width: "50px", height: "50px", borderRadius: "50%" }}
//                       onLoad={() => console.log('Image loaded successfully')}
//                       onError={(error) => {
//                         console.error('Error loading image:', error);
//                         // Handle the error gracefully (e.g., display a placeholder image)
//                       }}

//                     />
//                   )}
//                 </td>
//                 <td style={tableCellStyle}>{newsItem.category}</td>
//                 <td style={tableCellStyle}>{newsItem.approved ? 'Yes' : 'No'}</td>
//                 <td style={{ ...tableCellStyle, textAlign: 'right' }}>
//                   <button onClick={() => handleDelete(newsItem._id)} style={deleteButtonStyle}>Delete News</button>
//                 </td>
//               </tr>
//             ))}


//           </tbody>
//         </table>

//       </div>
//       {showDeleteModal && (
//         <AdminDeleteNewsConfirmation
//           onConfirm={handleConfirmDelete}
//           onCancel={handleCancelDelete}
//         />
//       )}


//     </div>
//   );
// };

// const buttonStyle = {
//   marginRight: "170px",
//   marginTop: "20px",
//   width: '130px',
//   height: '40px',
//   padding: "6px 12px",
//   color: "#111",
//   border: "1px solid #111",
//   borderRadius: "5px",
//   cursor: "pointer",
// };

// const tableStyle = {
//   width: "79%",
//   marginLeft: "165px",
//   borderCollapse: "collapse"
// };

// const tableHeaderStyle = {
//   backgroundColor: "#fff",
//   color: "#333",
//   fontSize: "18px",
//   borderBottom: "2px solid #ccc",
//   width: "25%" // Set a fixed width for each header cell
// };

// const tableCellStyle = {
//   padding: "10px",
//   textAlign: "left",
//   borderBottom: "1px solid #ccc"
// };

// const approveButtonStyle = {
//   width: '130px',
//   height: '40px',
//   padding: "6px 12px",
//   borderRadius: "5px",
//   backgroundColor: "#4CAF50",
//   color: "#fff",
//   cursor: "pointer",
//   border: "1px solid #333"
// };

// const deleteButtonStyle = {
//   width: '130px',
//   height: '40px',
//   padding: "6px 12px",
//   borderRadius: "5px",
//   backgroundColor: "red",
//   color: "#fff",
//   cursor: "pointer",
//   border: "1px solid #333",
// };

// export default AdminNews;
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import AdminHeader from "../components/adminHeader";
import AdminNavigation from "../components/adminNavigation";
import AdminApproveNewsConfirmation from "../components/adminApproveNewsConfirmation";
import AdminDeleteNewsConfirmation from "../components/adminDeleteNewsConfirmation";

const AdminNews = () => {
  const [unapprovedNews, setUnapprovedNews] = useState([]);
  const [showApproveModal, setShowApproveModal] = useState(false);
  const [newsIdToApprove, setNewsIdToApprove] = useState(null);
  const [news, setNews] = useState([]);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [newsToDelete, setNewsToDelete] = useState(null);

  useEffect(() => {
    const fetchUnapprovedNews = async () => {
      try {
        const token = localStorage.getItem("token");
        // const response = await fetch("http://10.9.92.25:8080/api/news/unapproved", {
        const response = await fetch("http://10.9.85.243:8080/api/news/unapproved", {
          method: "GET",
          headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
          }
        });
        if (!response.ok) {
          throw new Error("Failed to fetch unapproved news");
        }
        const data = await response.json();
        setUnapprovedNews(data);
      } catch (error) {
        console.error("Error fetching unapproved news:", error);
      }
    };

    fetchUnapprovedNews();
  }, []);

  useEffect(() => {
    const fetchAllNews = async () => {
      try {
        const response = await fetch("http://10.9.85.243:8080/api/news");
        if (!response.ok) {
          throw new Error("Failed to fetch news");
        }
        const data = await response.json();
        console.log("Fetched data:", data);
        const newsArray = Array.isArray(data.news) ? data.news : [data.news];
        setNews(newsArray);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
    };

    fetchAllNews();
  }, []);

  const handleApprove = (newsId) => {
    console.log("News ID to approve:", newsId);
    setNewsIdToApprove(newsId);
    setShowApproveModal(true);
  };

  const handleConfirmApprove = async () => {
    try {
      const token = localStorage.getItem("token");
      const response = await fetch(`http://10.9.85.243:8080/api/news/${newsIdToApprove}/approve`, {
        method: "PATCH",
        headers: {
          "Authorization": `Bearer ${token}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ approved: true })
      });
      if (!response.ok) {
        throw new Error("Failed to approve news");
      }

      const updatedNews = unapprovedNews.filter(news => news._id !== newsIdToApprove);
      setUnapprovedNews(updatedNews);

      console.log("News approved successfully");
      setShowApproveModal(false);
    } catch (error) {
      console.error("Error approving news:", error);
    }
  };

  const handleCancelApprove = () => {
    setShowApproveModal(false);
  };

  const handleDelete = (newsId) => {
    setNewsToDelete(newsId);
    setShowDeleteModal(true);
  };

  const handleConfirmDelete = async () => {
    try {
      const token = localStorage.getItem("token");
      const response = await fetch(`http://10.9.85.243:8080/api/news/${newsToDelete}`, {
        method: "DELETE",
        headers: {
          "Authorization": `Bearer ${token}`,
          "Content-Type": "application/json"
        }
      });

      setNews(news.filter((newsItem) => newsItem._id !== newsToDelete));

      alert("News deleted successfully");

      console.log("News deleted successfully");
    } catch (error) {
      console.error("Error deleting news:", error);
    } finally {
      setNewsToDelete(null);
      setShowDeleteModal(false);
    }
  };

  const handleCancelDelete = () => {
    setNewsToDelete(null);
    setShowDeleteModal(false);
  };

  return (
    <div>
      <AdminHeader />
      <AdminNavigation />
      <div style={{ marginBottom: "20px", textAlign: "right" }}>
        <Link to="/add-news">
          <button style={buttonStyle}>Add News</button>
        </Link>
      </div>
      <table style={tableStyle}>
        <thead>
          <tr style={tableHeaderStyle}>
            <th style={tableCellStyle}>User</th>
            <th style={tableCellStyle}>News</th>
            <th style={tableCellStyle}></th>
          </tr>
        </thead>
        <tbody>
          {unapprovedNews.map((news) => (
            <tr key={news._id} style={{ borderBottom: "1px solid #ccc" }}>
              <td style={tableCellStyle}>
                {news.image && (
                  <img
                    src={`http://localhost:8080/${news.image}`}
                    alt="News Image"
                    style={{ width: "50px", height: "50px", borderRadius: "50%" }}
                    onLoad={() => console.log('Image loaded successfully')}
                    onError={(error) => {
                      console.error('Error loading image:', error);
                    }}
                  />
                )}
              </td>
              <td style={tableCellStyle}>
                <Link to={`/admin/news/${news._id}`}>
                  <p><strong>{news.newsTitle}. {news.date}</strong></p>
                  <p>{news.content}</p>
                </Link>
              </td>
              <td style={{ ...tableCellStyle, textAlign: 'right' }}>
                <button onClick={() => handleApprove(news._id)} style={approveButtonStyle}>Approve News</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {showApproveModal && (
        <AdminApproveNewsConfirmation
          onConfirm={handleConfirmApprove}
          onCancel={handleCancelApprove}
        />
      )}

      <h2 style={{ textAlign: "center" }}>News</h2>
      <div style={{ maxHeight: "500px", overflowY: "auto" }}>
        <table style={tableStyle}>
          <thead>
            <tr style={tableHeaderStyle}>
              <th style={tableCellStyle}>Title</th>
              <th style={tableCellStyle}>Reporter</th>
              <th style={tableCellStyle}>Content</th>
              <th style={tableCellStyle}>Image</th>
              <th style={tableCellStyle}>Category</th>
              <th style={tableCellStyle}>Approved</th>
              <th style={tableCellStyle}>Delete</th>
            </tr>
          </thead>
          <tbody>
            {news.map((newsItem) => (
              <tr key={newsItem._id} style={{ borderBottom: "1px solid #ccc" }}>
                <td style={tableCellStyle}>
                  <Link to={`/admin/news/${newsItem._id}`}>
                    {newsItem.title}
                  </Link>
                </td>
                <td style={tableCellStyle}>{newsItem.reporter}</td>
                <td style={tableCellStyle}>{newsItem.content}</td>
                <td style={tableCellStyle}>
                  {newsItem.image && (
                    <img
                      src={`http://localhost:8080/${newsItem.image}`}
                      alt="News Image"
                      style={{ width: "50px", height: "50px", borderRadius: "50%" }}
                      onLoad={() => console.log('Image loaded successfully')}
                      onError={(error) => {
                        console.error('Error loading image:', error);
                      }}
                    />
                  )}
                </td>
                <td style={tableCellStyle}>{newsItem.category}</td>
                <td style={tableCellStyle}>{newsItem.approved ? 'Yes' : 'No'}</td>
                <td style={{ ...tableCellStyle, textAlign: 'right' }}>
                  <button onClick={() => handleDelete(newsItem._id)} style={deleteButtonStyle}>Delete News</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {showDeleteModal && (
        <AdminDeleteNewsConfirmation
          onConfirm={handleConfirmDelete}
          onCancel={handleCancelDelete}
        />
      )}
    </div>
  );
};

const buttonStyle = {
  marginRight: "170px",
  marginTop: "20px",
  width: '130px',
  height: '40px',
  padding: "6px 12px",
  color: "#ffa500",
  backgroundColor: '#fff',
  border: "1px solid #ffa500",
  borderRadius: "5px",
  cursor: "pointer",
};
const tableStyle = {
  width: "79%",
  marginLeft: "165px",
  borderCollapse: "collapse"
};

const tableHeaderStyle = {
  backgroundColor: "#fff",
  color: "#333",
  fontSize: "18px",
  borderBottom: "2px solid #ccc",
  width: "25%"
};

const tableCellStyle = {
  padding: "10px",
  textAlign: "left",
  borderBottom: "1px solid #ccc"
};

// const approveButtonStyle = {
//   width: '130px',
//   height: '40px',
//   padding: "6px 12px",
//   borderRadius: "5px",
//   backgroundColor: "#4CAF50",
//   color: "#fff",
//   cursor: "pointer",
//   border: "1px solid #333"
// };
const approveButtonStyle = {
  // width: '130px',
  // height: '40px',
  // padding: "6px 12px",
  // borderRadius: "5px",
  // backgroundColor: "#4CAF50",
  // color: "#fff",
  // cursor: "pointer"
  width: '130px',
  height: '40px',
  padding: "6px 12px",
  borderRadius: "5px",
  backgroundColor: "#4CAF50",
  color: "#fff",
  border: 'none',
};

// Styles for delete button
const deleteButtonStyle = {
  width: '130px',
  height: '40px',
  padding: "6px 12px",
  borderRadius: "5px",
  backgroundColor: "red",
  color: "#fff",
  border: 'none',
};

export default AdminNews;
