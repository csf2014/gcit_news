// // // import React from "react";
// // // import { useParams, useNavigate } from "react-router-dom";
// // // import AdminHeader from "../components/adminHeader";
// // // import home3Image from "../assets/news.png"; // Import the image

// // // const AdminNewsDetail = () => {
// // //   const { id } = useParams(); // Get the news ID from URL params
// // //   const navigate = useNavigate(); // Initialize the useNavigate hook

// // //   // Fetch news details based on the ID and display them
// // //   // For example:
// // //   const news = {
// // //     id: id,
// // //     newsTitle: "New Sarpang Dzong gets serthog",
// // //     date: "20/02/2024",
// // //     content: "Her Majesty Gyalyum Tshering Yangdon Wangchuck graced the serthog installation ceremony at Sarpang Dzong yesterday. Yonten Lopen, Trulku Namgyel of the Zhung Dratshang, presided over the ceremony. The construction of the new dzong, set to finish by December 2024, is 75 percent complete. Structural and architectural aspects have been completed, except for the Kuenrey, where foundation work has just been finished. Project manager, Sangay Kinga, said that mural painting and sculpture creation for the relics have commenced. “We expect the results in the coming months. The project must be finalised and handed over within a year.",
// // //     reporterName: "Anushia Subba", // Add reporter's name to the news object
// // //     category: "Announcements", // Add category to the news object
// // //     imageUrl: home3Image // Use the imported image
// // //   };

// // //   const handleBack = () => {
// // //     navigate(-1); // Navigate back to the previous page
// // //   };

// // //   return (
// // //     <div>
// // //       <AdminHeader />
// // //       <div style={styles.container}>
// // //         <button onClick={handleBack} style={styles.closeButton}>x</button>
// // //         <img src={news.imageUrl} alt="" style={styles.image} /> {/* Render the image */}
// // //         <h2 style={styles.title}>{news.newsTitle}</h2>
// // //         <p style={styles.date}>Date: {news.date}</p>
// // //         <p style={styles.reporter}>Reporter: {news.reporterName}</p> {/* Render reporter's name */}
// // //         <p style={styles.category}>Category: {news.category}</p> {/* Render category */}
// // //         <p style={styles.content}>{news.content}</p>
// // //       </div>
// // //     </div>
// // //   );
// // // };


// // import React, { useState, useEffect } from 'react';
// // import { useParams } from 'react-router-dom';
// // import AdminHeader from '../components/adminHeader';
// // import AdminNavigation from '../components/adminNavigation';

// // const AdminNewsDetails = () => {
// //   const { id } = useParams();
// //   const [newsDetails, setNewsDetails] = useState(null);

// //   useEffect(() => {
// //     const fetchNewsDetails = async () => {
// //       try {
// //         const response = await fetch(`http://10.9.92.25:8080/api/news/${id}`);
// //         if (!response.ok) {
// //           throw new Error('Failed to fetch news details');
// //         }
// //         const data = await response.json();
// //         setNewsDetails(data);
// //       } catch (error) {
// //         console.error('Error fetching news details:', error);
// //       }
// //     };

// //     fetchNewsDetails();
// //   }, [id]);

// //   if (!newsDetails) {
// //     return <div>Loading...</div>;
// //   }

// //   return (
// //     <div>
// //       <AdminHeader />
// //       <AdminNavigation />
// //       <div style={{ padding: '20px' }}>
// //         <h1>{newsDetails.title}</h1>
// //         <p>Reporter: {newsDetails.reporter}</p>
// //         <p>Date: {newsDetails.date}</p>
// //         <p>Category: {newsDetails.category}</p>
// //         <div>
// //           <img
// //             src={`http://localhost:8080/${newsDetails.image}`}
// //             alt="News Image"
// //             style={{ maxWidth: '100%' }}
// //             onError={(error) => console.error('Error loading image:', error)}
// //           />
// //         </div>
// //         <p>{newsDetails.content}</p>
// //       </div>
// //     </div>
// //   );
// // };

// // const styles = {
// //   container: {
// //     padding: "25px 40px",
// //     backgroundColor: "#fff",
// //     boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
// //     borderRadius: "5px",
// //     margin: "20px auto",
// //     maxWidth: "800px",
// //     position: "relative" // Ensure the container is positioned relatively to place the button absolutely inside it
// //   },
// //   title: {
// //     fontSize: "24px",
// //     marginBottom: "10px",
// //   },
// //   date: {
// //     color: "#666",
// //     marginBottom: "10px",
// //   },
// //   reporter: {
// //     color: "#666",
// //     marginBottom: "10px",
// //   },
// //   category: {
// //     color: "#666",
// //     marginBottom: "10px",
// //   },
// //   content: {
// //     lineHeight: "1.5",
// //     marginBottom: "20px",
// //   },
// //   image: {
// //     width: "100%",
// //     height: "300px",
// //     borderRadius: "5px",
// //     marginTop: '30px',
// //   },
// //   closeButton: {
// //     position: "absolute",
// //     top: "10px",
// //     right: "10px",
// //     backgroundColor: '#fff',
// //     color: "#ffa500",
// //     fontWeight: "bold",
// //     border: "none",
// //     width: "30px",
// //     height: "30px",
// //     cursor: "pointer",
// //     fontSize: "25px",
// //     display: "flex",
// //     justifyContent: "center",
// //     alignItems: "center"
// //   }
// // };

// // export default AdminNewsDetails;
// import React, { useState, useEffect } from 'react';
// import { useParams, Link } from 'react-router-dom'; // Importing Link component
// import AdminHeader from '../components/adminHeader';
// import AdminNavigation from '../components/adminNavigation';

// const AdminNewsDetails = () => {
//   const { id } = useParams();
//   const [newsDetails, setNewsDetails] = useState(null);

//   useEffect(() => {
//     const fetchNewsDetails = async () => {
//       try {
//         const response = await fetch(`http://10.9.92.25:8080/api/news/${id}`);
//         if (!response.ok) {
//           throw new Error('Failed to fetch news details');
//         }
//         const data = await response.json();
//         console.log('Fetched news details:', data); // Add this console log
//         setNewsDetails(data);
//       } catch (error) {
//         console.error('Error fetching news details:', error);
//       }
//     };

//     fetchNewsDetails();
//   }, [id]);


//   if (!newsDetails) {
//     return <div>Loading...</div>;
//   }

//   return (
//     <div>
//       <AdminHeader />
//       <AdminNavigation />
//       <div style={styles.container}>
//         {/* Link to navigate back to the admin news page */}
//         <Link to="/news-request" style={styles.closeButton}>&times;</Link>
//         <div>
//           <h1 style={styles.title}>{newsDetails.title}</h1>
//           <p style={styles.reporter}>Reporter: {newsDetails.reporter}</p>
//           <p style={styles.date}>Date: {newsDetails.date}</p>
//           <p style={styles.category}>Category: {newsDetails.category}</p>
//           {newsDetails.image && (
//             <div>
//               <img
//                 src={`http://localhost:8080/${newsDetails.image}`}
//                 alt="News Image"
//                 style={styles.image}
//                 onError={(error) => console.error('Error loading image:', error)}
//               />
//             </div>
//           )}
//           <p style={styles.content}>{newsDetails.content}</p>
//         </div>
//       </div>
//     </div>
//   );
// }

// const styles = {
//   container: {
//     padding: "25px 40px",
//     backgroundColor: "#fff",
//     boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
//     borderRadius: "5px",
//     margin: "20px auto",
//     maxWidth: "800px",
//     position: "relative"
//   },
//   title: {
//     fontSize: "24px",
//     marginBottom: "10px",
//   },
//   date: {
//     color: "#666",
//     marginBottom: "10px",
//   },
//   reporter: {
//     color: "#666",
//     marginBottom: "10px",
//   },
//   category: {
//     color: "#666",
//     marginBottom: "10px",
//   },
//   content: {
//     lineHeight: "1.5",
//     marginBottom: "20px",
//   },
//   image: {
//     width: "100%",
//     height: "300px",
//     borderRadius: "5px",
//     marginTop: '30px',
//   },
//   closeButton: {
//     position: "absolute",
//     top: "10px",
//     right: "10px",
//     backgroundColor: '#fff',
//     color: "#ffa500",
//     fontWeight: "bold",
//     border: "none",
//     width: "30px",
//     height: "30px",
//     cursor: "pointer",
//     fontSize: "25px",
//     display: "flex",
//     justifyContent: "center",
//     alignItems: "center",
//     textDecoration: "none" // Ensure the link text is not underlined
//   }
// };

// export default AdminNewsDetails;
import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import AdminHeader from '../components/adminHeader';
import AdminNavigation from '../components/adminNavigation';

const AdminNewsDetails = () => {
  const { id } = useParams();
  const [newsDetails, setNewsDetails] = useState(null);

  useEffect(() => {
    const fetchNewsDetails = async () => {
      try {
        const response = await fetch(`http://10.9.85.243:8080/api/news/${id}`);
        if (!response.ok) {
          throw new Error('Failed to fetch news details');
        }
        const data = await response.json();
        console.log('Fetched news details:', data);
        setNewsDetails(data);
      } catch (error) {
        console.error('Error fetching news details:', error);
      }
    };

    fetchNewsDetails();
  }, [id]);

  if (!newsDetails) {
    return (
      <div style={styles.loadingContainer}>
        <div>Loading...</div>
      </div>
    );
  }

  const { title, reporter, date, category, content, image } = newsDetails.news;

  console.log('Rendering news details:', newsDetails.news);

  return (
    <div>
      <AdminHeader />
      <AdminNavigation />
      <div style={styles.container}>
        <Link to="/news-request" style={styles.closeButton}>&times;</Link>
        <div>
          {image && (
            <div style={styles.imageContainer}>
              <img
                src={`http://localhost:8080/${image}`}
                alt="News"
                style={styles.image}
                onError={(error) => console.error('Error loading image:', error)}
              />
            </div>
          )}
          <h1 style={styles.title}>{title}</h1>
          <p style={styles.reporter}>Reporter: {reporter}</p>
          <p style={styles.date}>Date: {new Date(date).toLocaleDateString()}</p>
          <p style={styles.category}>Category: {category}</p>

          <p style={styles.content}>{content}</p>
        </div>
      </div>
    </div>
  );
}

const styles = {
  container: {
    padding: "25px 40px",
    backgroundColor: "#fff",
    boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
    borderRadius: "5px",
    margin: "20px auto",
    maxWidth: "800px",
    position: "relative" // Ensure the container is positioned relatively to place the button absolutely inside it
  },
  title: {
    fontSize: "24px",
    marginBottom: "10px",
  },
  date: {
    color: "#666",
    marginBottom: "10px",
  },
  reporter: {
    color: "#666",
    marginBottom: "10px",
  },
  category: {
    color: "#666",
    marginBottom: "10px",
  },
  content: {
    lineHeight: "1.5",
    marginBottom: "20px",
  },
  image: {
    width: "100%",
    height: "400px",
    borderRadius: "5px",
    marginTop: '30px',
  },
  closeButton: {
    position: "absolute",
    top: "10px",
    right: "10px",
    backgroundColor: '#fff',
    color: "#ffa500",
    fontWeight: "bold",
    border: "none",
    width: "30px",
    height: "30px",
    cursor: "pointer",
    fontSize: "25px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
};

export default AdminNewsDetails;
