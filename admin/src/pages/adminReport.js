// // // // import React, { useState } from "react";
// // // // import AdminHeader from "../components/adminHeader";
// // // // import AdminNavigation from "../components/adminNavigation";
// // // // import AdminDeleteCommentConfirmation from "../components/adminDeleteCommentConfirmation"; // You'll need to create this component

// // // // import user1Img from "../assets/user1.jpeg";
// // // // import user2Img from "../assets/user.jpeg";

// // // // const AdminReport = () => {
// // // //   const [reportedComments, setReportedComments] = useState([
// // // //     { id: 1, newsTitle: "Breaking News", date: "20/02/2024", comment: "Lorem Ipsum is simply dummy text...", imageUrl: user1Img },
// // // //     { id: 2, newsTitle: "Latest Updates", date: "21/02/2024", comment: "Lorem Ipsum has been the industry's standard...", imageUrl: user2Img },
// // // //     { id: 3, newsTitle: "Important Announcement", date: "22/02/2024", comment: "Contrary to popular belief, Lorem Ipsum is not simply random text...", imageUrl: user1Img }
// // // //   ]);
// // // //   const [showDeleteModal, setShowDeleteModal] = useState(false);
// // // //   const [commentToDelete, setCommentToDelete] = useState(null);

// // // //   const handleDelete = (commentId) => {
// // // //     setCommentToDelete(commentId);
// // // //     setShowDeleteModal(true);
// // // //   };

// // // //   const handleConfirmDelete = () => {
// // // //     const updatedComments = reportedComments.filter((comment) => comment.id !== commentToDelete);
// // // //     setReportedComments(updatedComments);
// // // //     console.log("Comment deleted");
// // // //     setShowDeleteModal(false);
// // // //   };

// // // //   const handleCancelDelete = () => {
// // // //     setShowDeleteModal(false);
// // // //   };

// // // //   // Styles for the table
// // // //   const tableStyle = {
// // // //     width: "78%",
// // // //     marginLeft: "170px",
// // // //     borderCollapse: "collapse"
// // // //   };

// // // //   // Styles for table headers
// // // //   const tableHeaderStyle = {
// // // //     backgroundColor: "#fff",
// // // //     color: "#333",
// // // //     fontSize: "18px",
// // // //     borderBottom: "2px solid #ccc",
// // // //     width: "25%" // Set a fixed width for each header cell
// // // //   };

// // // //   // Styles for table cells
// // // //   const tableCellStyle = {
// // // //     padding: "10px",
// // // //     textAlign: "left",
// // // //     borderBottom: "1px solid #ccc"
// // // //   };

// // // //   // Styles for delete button
// // // //   const deleteButtonStyle = {
// // // //     width: '130px',
// // // //     height: '40px',
// // // //     padding: "6px 12px",
// // // //     borderRadius: "5px",
// // // //     backgroundColor: "red",
// // // //     color: "#111",
// // // //     border: "1px solid #333",
// // // //   };

// // // //   return (
// // // //     <div>
// // // //       <AdminHeader />
// // // //       <AdminNavigation />
// // // //       <table style={tableStyle}>
// // // //         <thead>
// // // //           <tr style={tableHeaderStyle}>
// // // //             <th style={tableCellStyle}>User</th>
// // // //             <th style={tableCellStyle}>Comment</th>
// // // //             <th style={tableCellStyle}></th>
// // // //           </tr>
// // // //         </thead>
// // // //         <tbody>
// // // //           {reportedComments.map((comment) => (
// // // //             <tr key={comment.id} style={{ borderBottom: "1px solid #ccc" }}>
// // // //               <td style={tableCellStyle}>
// // // //                 <img src={comment.imageUrl} alt="User" style={{ width: "50px", height: "50px" }} />
// // // //               </td>
// // // //               <td style={tableCellStyle}>
// // // //                 <p><strong>{comment.newsTitle}. {comment.date}</strong></p>
// // // //                 <p>{comment.comment}</p>
// // // //               </td>
// // // //               <td style={{ ...tableCellStyle, textAlign: 'right' }}>
// // // //                 <button onClick={() => handleDelete(comment.id)} style={deleteButtonStyle}>Delete Comment</button>
// // // //               </td>
// // // //             </tr>
// // // //           ))}
// // // //         </tbody>
// // // //       </table>
// // // //       {showDeleteModal && (
// // // //         <AdminDeleteCommentConfirmation
// // // //           onConfirm={handleConfirmDelete}
// // // //           onCancel={handleCancelDelete}
// // // //         />
// // // //       )}
// // // //     </div>
// // // //   );
// // // // };

// // // // export default AdminReport;
// // // import React, { useState, useEffect } from "react";
// // // import AdminHeader from "../components/adminHeader";
// // // import AdminNavigation from "../components/adminNavigation";
// // // import AdminDeleteCommentConfirmation from "../components/adminDeleteCommentConfirmation"; // You'll need to create this component


// // // const AdminReport = () => {
// // //   const [reportedComments, setReportedComments] = useState([]);
// // //   const [showDeleteModal, setShowDeleteModal] = useState(false);
// // //   const [commentToDelete, setCommentToDelete] = useState(null);

// // //   useEffect(() => {
// // //     async function fetchReportedComments() {
// // //       try {
// // //         const response = await fetch("http://10.9.92.25:8080/api/news/comments/reported-comments");
// // //         if (!response.ok) {
// // //           throw new Error("Failed to fetch reported comments");
// // //         }
// // //         const data = await response.json();
// // //         setReportedComments(data);
// // //       } catch (error) {
// // //         console.error("Error fetching reported comments:", error.message);
// // //       }
// // //     }

// // //     fetchReportedComments();
// // //   }, []);

// // //   const handleDelete = (commentId) => {
// // //     setCommentToDelete(commentId);
// // //     setShowDeleteModal(true);
// // //   };

// // //   const handleConfirmDelete = () => {
// // //     const updatedComments = reportedComments.filter((comment) => comment.id !== commentToDelete);
// // //     setReportedComments(updatedComments);
// // //     console.log("Comment deleted");
// // //     setShowDeleteModal(false);
// // //   };

// // //   const handleCancelDelete = () => {
// // //     setShowDeleteModal(false);
// // //   };

// // //   // Styles and JSX for table...
// // //   const tableStyle = {
// // //     width: "78%",
// // //     marginLeft: "170px",
// // //     borderCollapse: "collapse"
// // //   };

// // //   const tableHeaderStyle = {
// // //     backgroundColor: "#fff",
// // //     color: "#333",
// // //     fontSize: "18px",
// // //     borderBottom: "2px solid #ccc",
// // //     width: "25%"
// // //   };

// // //   const tableCellStyle = {
// // //     padding: "10px",
// // //     textAlign: "left",
// // //     borderBottom: "1px solid #ccc"
// // //   };

// // //   const deleteButtonStyle = {
// // //     width: '130px',
// // //     height: '40px',
// // //     padding: "6px 12px",
// // //     borderRadius: "5px",
// // //     backgroundColor: "red",
// // //     color: "#111",
// // //     border: "1px solid #333",
// // //   };

// // //   return (
// // //     <div>
// // //       <AdminHeader />
// // //       <AdminNavigation />
// // //       <table style={tableStyle}>
// // //         <thead>
// // //           <tr style={tableHeaderStyle}>
// // //             <th style={tableCellStyle}>User</th>
// // //             <th style={tableCellStyle}>Comment</th>
// // //             <th style={tableCellStyle}></th>
// // //           </tr>
// // //         </thead>
// // //         <tbody>
// // //           {reportedComments.map((comment) => (
// // //             <tr key={comment.id} style={{ borderBottom: "1px solid #ccc" }}>
// // //               <td style={tableCellStyle}>
// // //                 <img src={comment.imageUrl} alt="User" style={{ width: "50px", height: "50px" }} />
// // //               </td>
// // //               <td style={tableCellStyle}>
// // //                 <p><strong>{comment.newsTitle}. {comment.date}</strong></p>
// // //                 <p>{comment.comment}</p>
// // //               </td>
// // //               <td style={{ ...tableCellStyle, textAlign: 'right' }}>
// // //                 <button onClick={() => handleDelete(comment.id)} style={deleteButtonStyle}>Delete Comment</button>
// // //               </td>
// // //             </tr>
// // //           ))}
// // //         </tbody>
// // //       </table>
// // //       {showDeleteModal && (
// // //         <AdminDeleteCommentConfirmation
// // //           onConfirm={handleConfirmDelete}
// // //           onCancel={handleCancelDelete}
// // //         />
// // //       )}
// // //     </div>
// // //   );
// // // };

// // // export default AdminReport;

// // import React, { useState, useEffect } from "react";
// // import AdminHeader from "../components/adminHeader";
// // import AdminNavigation from "../components/adminNavigation";
// // import AdminDeleteCommentConfirmation from "../components/adminDeleteCommentConfirmation"; // You'll need to create this component


// // const AdminReport = () => {
// //   const [reportedComments, setReportedComments] = useState([]);
// //   const [showDeleteModal, setShowDeleteModal] = useState(false);
// //   const [commentToDelete, setCommentToDelete] = useState(null);

// //   useEffect(() => {
// //     async function fetchReportedComments() {
// //       try {
// //         const response = await fetch("http://10.9.92.25:8080/api/news/comments/reported-comments");
// //         if (!response.ok) {
// //           throw new Error("Failed to fetch reported comments");
// //         }
// //         const data = await response.json();
// //         setReportedComments(data);
// //       } catch (error) {
// //         console.error("Error fetching reported comments:", error.message);
// //       }
// //     }

// //     fetchReportedComments();
// //   }, []);

// //   const handleDelete = (commentId) => {
// //     setCommentToDelete(commentId);
// //     setShowDeleteModal(true);
// //   };

// //   const handleConfirmDelete = () => {
// //     const updatedComments = reportedComments.filter((comment) => comment.id !== commentToDelete);
// //     setReportedComments(updatedComments);
// //     console.log("Comment deleted");
// //     setShowDeleteModal(false);
// //   };

// //   const handleCancelDelete = () => {
// //     setShowDeleteModal(false);
// //   };

// //   // Styles and JSX for table...
// //   const tableStyle = {
// //     width: "78%",
// //     marginLeft: "170px",
// //     borderCollapse: "collapse"
// //   };

// //   const tableHeaderStyle = {
// //     backgroundColor: "#fff",
// //     color: "#333",
// //     fontSize: "18px",
// //     borderBottom: "2px solid #ccc",
// //     width: "25%"
// //   };

// //   const tableCellStyle = {
// //     padding: "10px",
// //     textAlign: "left",
// //     borderBottom: "1px solid #ccc"
// //   };

// //   const deleteButtonStyle = {
// //     width: '130px',
// //     height: '40px',
// //     padding: "6px 12px",
// //     borderRadius: "5px",
// //     backgroundColor: "red",
// //     color: "#111",
// //     border: "1px solid #333",
// //   };

// //   return (
// //     <div>
// //       <AdminHeader />
// //       <AdminNavigation />
// //       <table style={tableStyle}>
// //         <thead>
// //           <tr style={tableHeaderStyle}>
// //             <th style={tableCellStyle}>User</th>
// //             <th style={tableCellStyle}>Comment</th>
// //             <th style={tableCellStyle}>Date</th>
// //             <th style={tableCellStyle}></th>
// //           </tr>
// //         </thead>
// //         <tbody>
// //           {reportedComments.map((comment) => (
// //             <tr key={comment.id} style={{ borderBottom: "1px solid #ccc" }}>
// //               <td style={tableCellStyle}>
// //                 <p>{comment.commentUsername}</p>
// //               </td>
// //               <td style={tableCellStyle}>
// //                 <p><strong>{comment.newsTitle}. {comment.date}</strong></p>
// //                 <p>{comment.commentText}</p>
// //               </td>
// //               <td style={tableCellStyle}>
// //                 <p><strong> {new Date(comment.commentCreatedAt).toLocaleDateString('en-GB')}</strong></p>

// //               </td>

// //               <td style={{ ...tableCellStyle, textAlign: 'right' }}>
// //                 <button onClick={() => handleDelete(comment.id)} style={deleteButtonStyle}>Delete Comment</button>
// //               </td>
// //             </tr>
// //           ))}
// //         </tbody>
// //       </table>
// //       {showDeleteModal && (
// //         <AdminDeleteCommentConfirmation
// //           onConfirm={handleConfirmDelete}
// //           onCancel={handleCancelDelete}
// //         />
// //       )}
// //     </div>
// //   );
// // };

// // export default AdminReport;


import React, { useState, useEffect } from "react";
import AdminHeader from "../components/adminHeader";
import AdminNavigation from "../components/adminNavigation";
import AdminDeleteCommentConfirmation from "../components/adminDeleteCommentConfirmation"; // You'll need to create this component


const AdminReport = () => {
  const [reportedComments, setReportedComments] = useState([]);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [commentToDelete, setCommentToDelete] = useState(null);

  useEffect(() => {
    async function fetchReportedComments() {
      try {
        const response = await fetch("http://10.9.85.243:8080/api/news/comments/reported-comments");
        if (!response.ok) {
          throw new Error("Failed to fetch reported comments");
        }
        const data = await response.json();
        setReportedComments(data);
      } catch (error) {
        console.error("Error fetching reported comments:", error.message);
      }
    }

    fetchReportedComments();
  }, []);

  const handleDelete = (commentId) => {
    setCommentToDelete(commentId);
    console.log(commentId)

    setShowDeleteModal(true);
  };

  const handleConfirmDelete = async () => {
    try {
      const token = localStorage.getItem("token");
      const response = await fetch(`http://10.9.85.243:8080/api/news/comments/reported-comments/${commentToDelete}`, {
        method: 'DELETE',
        headers: {
          "Authorization": `Bearer ${token}`,
          "Content-Type": "application/json"
        }
      });
      if (!response.ok) {
        throw new Error('Failed to delete reported comment');
      }
      const updatedComments = reportedComments.filter((comment) => comment._id !== commentToDelete);
      setReportedComments(updatedComments);
      console.log('Comment deleted');
      setShowDeleteModal(false);
    } catch (error) {
      console.error('Error deleting reported comment:', error.message);
    }
  };

  const handleCancelDelete = () => {
    setShowDeleteModal(false);
  };

  // Styles and JSX for table...
  const tableStyle = {
    width: "78%",
    marginLeft: "170px",
    borderCollapse: "collapse"
  };

  const tableHeaderStyle = {
    backgroundColor: "#fff",
    color: "#333",
    fontSize: "18px",
    borderBottom: "2px solid #ccc",
    width: "25%"
  };

  const tableCellStyle = {
    padding: "10px",
    textAlign: "left",
    borderBottom: "1px solid #ccc"
  };

  // Styles for delete button
  const deleteButtonStyle = {
    width: '130px',
    height: '40px',
    padding: "6px 12px",
    borderRadius: "5px",
    backgroundColor: "red",
    color: "#fff",
    border: 'none',
  };

  return (
    <div>
      <AdminHeader />
      <AdminNavigation />
      <table style={tableStyle}>
        <thead>
          <tr style={tableHeaderStyle}>
            <th style={tableCellStyle}>User</th>
            <th style={tableCellStyle}>Comment</th>
            <th style={tableCellStyle}>Date</th>
            <th style={tableCellStyle}>ReportCount</th>
            <th style={tableCellStyle}></th>
          </tr>
        </thead>
        <tbody>
          {reportedComments.map((comment) => (

            <tr key={comment._id} style={{ borderBottom: "1px solid #ccc" }}>
              <td style={tableCellStyle}>
                <p>{comment.commentUsername}</p>
              </td>
              <td style={tableCellStyle}>
                <p><strong>{comment.newsTitle}. {comment.date}</strong></p>
                <p>{comment.commentText}</p>
              </td>
              <td style={tableCellStyle}>
                <p><strong> {new Date(comment.commentCreatedAt).toLocaleDateString('en-GB')}</strong></p>

              </td>
              <td style={tableCellStyle}>
                <p><strong>{comment.reportCount}</strong></p>

              </td>

              <td style={{ ...tableCellStyle, textAlign: 'right' }}>
                <button onClick={() => handleDelete(comment._id)} style={deleteButtonStyle}>Delete Comment</button>
              </td>


            </tr>
          ))}
        </tbody>

      </table>
      {showDeleteModal && (
        <AdminDeleteCommentConfirmation
          onConfirm={handleConfirmDelete}
          onCancel={handleCancelDelete}
        />
      )}
    </div>
  );
};

export default AdminReport;
