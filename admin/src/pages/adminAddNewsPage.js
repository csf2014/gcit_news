
import React, { useState, useRef } from 'react';
import AdminNavigation from '../components/adminNavigation';
import AdminHeader from '../components/adminHeader';
import cloudIcon from '../assets/cloud-icon.svg';

const AdminAddNewsPage = () => {
  const [title, setTitle] = useState('');
  const [reporter, setReporter] = useState('');
  const [category, setCategory] = useState('');
  const [content, setContent] = useState('');
  const [image, setImage] = useState(null);
  const [userId, setUserId] = useState(localStorage.getItem('userId'));
  const fileInputRef = useRef(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log('Form submitted');

    try {
      const formData = new FormData();
      formData.append('title', title);
      formData.append('reporter', reporter); // Correct key name
      formData.append('category', category);
      formData.append('content', content);
      formData.append('image', image);

      // Retrieve the token from local storage
      const token = localStorage.getItem('token');

      // const response = await fetch('http://10.9.92.25:8080/api/news/create', {
      const response = await fetch('http://10.9.85.243:8080/api/news/create', {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`, // Include the token in the Authorization header
        },
        body: formData,
      });

      if (response.ok) {
        alert('News created successfully');
        console.log('News created successfully');
        setTitle('');
        setReporter('');
        setCategory('');
        setContent('');
        setImage(null);
      } else {
        throw new Error('Failed to create news');
      }
    } catch (error) {
      console.error('Error creating news:', error.message);
      alert('Failed to create news');
    }
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    console.log('File selected:', file);
    setImage(file);
  };

  const handleClear = () => {
    setTitle('');
    setReporter('');
    setCategory('');
    setContent('');
    setImage(null);
  };

  const handleUploadClick = () => {
    fileInputRef.current.click();
  };

  return (
    <div>
      <AdminHeader />
      <AdminNavigation />
      <div style={{ margin: '20px 182px 20px 166px' }}>
        <form onSubmit={handleSubmit}>
          <div style={{ marginBottom: '15px' }}>
            <label>Title:</label>
            <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} style={inputStyle} />
          </div>
          <div style={{ display: 'flex', marginBottom: '15px' }}>
            <div style={{ flex: '3' }}>
              <label>Reporter Name:</label>
              <input type="text" value={reporter} onChange={(e) => setReporter(e.target.value)} style={inputStyle} />
            </div>
            <div style={{ flex: '1', marginLeft: '35px', marginRight: '5px' }}>
              <label>Category:</label>
              <select value={category} onChange={(e) => setCategory(e.target.value)} style={inputStyle}>
                <option value="">Select category...</option>
                <option value="announcements">Announcements</option>
                <option value="events">Events</option>
                <option value="clubs">Clubs</option>
              </select>
            </div>
          </div>
          <div style={{ marginBottom: '15px' }}>
            <label>Content:</label>
            <textarea value={content} onChange={(e) => setContent(e.target.value)} style={inputStyle2} />
          </div>
          <div style={{ marginBottom: '15px' }}>
            <label>Image:</label>
            <button type="button" onClick={handleUploadClick} style={buttonStyle}>Upload Image</button>
            <input ref={fileInputRef} type="file" style={{ display: 'none' }} onChange={handleFileChange} />
          </div>
          <div>
            <button type="button" onClick={handleClear} style={buttonStyle}>Clear</button>
            <button type="submit" style={buttonStyle}>Post</button>
          </div>
        </form>
      </div>
    </div>
  );
};

const inputStyle = {
  width: '100%',
  marginTop: '5px',
  padding: '8px',
  border: '1px solid #ccc',
  borderRadius: '5px',
};

const inputStyle2 = {
  width: '100%',
  height: "140px",
  marginTop: '5px',
  padding: '8px',
  border: '1px solid #ccc',
  borderRadius: '5px',
};

const buttonStyle = {
  padding: '10px 50px',
  backgroundColor: "#ffa500",
  color: '#fff',
  border: 'none',
  borderRadius: '5px',
  cursor: 'pointer',
  marginRight: '10px',
  marginTop: "20px"
};

export default AdminAddNewsPage;
