// components/header.js
import React from "react";

const AdminHeader = () => {
    const currentDate = new Date();
    const options = {
        weekday: "long",
        month: "long",
        day: "numeric",
        year: "numeric"
    };
    const formattedDate = currentDate.toLocaleDateString(undefined, options);
    return (
        <div style={{ textAlign: "center" }}>
            <h1 style={{ fontSize: "40px", color: "#008000" }}>GCIT NEWS</h1>
            <p style={{ fontSize: "16px", color: "#FFFFFF", backgroundColor: "#ffa500", padding: "5px" }}>{formattedDate}</p>
        </div>
    );
};

export default AdminHeader;
