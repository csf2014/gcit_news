// import React from "react";
// import { Link, useLocation } from "react-router-dom";

// const AdminNavigation = () => {
//   const location = useLocation();

//   // Define active link styles
//   const activeLinkStyle = {
//     backgroundColor: "#ffa500",
//     color: "#FFFFFF",
//   };

//   // Check if the current path matches the link
//   const isActive = (path) => {
//     return location.pathname === path;
//   };

//   return (
//     <header style={headerStyle}>
//       <nav style={navStyle}>
//         <Link
//           to="/users"
//           style={isActive("/users") ? { ...linkStyle, ...activeLinkStyle } : linkStyle}
//         >
//           Users
//         </Link>
//         <Link
//           to="/news-request"
//           style={isActive("/news-request") ? { ...linkStyle, ...activeLinkStyle } : linkStyle}
//         >
//           News
//         </Link>
//         <Link
//           to="/reported-comments"
//           style={isActive("/reported-comments") ? { ...linkStyle, ...activeLinkStyle } : linkStyle}
//         >
//           Reported Comments
//         </Link>
//       </nav>
//     </header>
//   );
// };

// const headerStyle = {
//   color: "#333",
//   padding: "20px",
// };

// const navStyle = {
//   display: "flex",
//   justifyContent: "center",
// };

// const linkStyle = {
//   width: 400, // Fixed width for each link
//   height: 120, // Fixed height for each link
//   // backgroundColor: "#ffa500",
//   border: "1px solid #ffa500",
//   color: "#ffa500",
//   fontSize: "20px",
//   fontWeight: "bold",
//   textDecoration: "none",
//   textAlign: "center",
//   display: "flex",
//   justifyContent: "center",
//   alignItems: "center",
// };

// export default AdminNavigation;
import React from "react";
import { Link, useLocation } from "react-router-dom";

const AdminNavigation = () => {
  const location = useLocation();

  // Define active link styles
  const activeLinkStyle = {
    backgroundColor: "#ffa500",
    color: "#FFFFFF",
  };

  // Check if the current path matches the link
  const isActive = (path) => {
    return location.pathname === path;
  };

  const handleLogout = () => {
    // Implement your logout logic here
    // Redirect to home page after logout
    window.location.href = "http://localhost:3000/";
  };

  return (
    <header style={headerStyle}>
      <div style={logoutContainerStyle}>
        <button onClick={handleLogout} style={logoutButtonStyle}>Logout</button>
      </div>
      <nav style={navStyle}>
        <Link
          to="/users"
          style={isActive("/users") ? { ...linkStyle, ...activeLinkStyle } : linkStyle}
        >
          Users
        </Link>
        <Link
          to="/news-request"
          style={isActive("/news-request") ? { ...linkStyle, ...activeLinkStyle } : linkStyle}
        >
          News
        </Link>
        <Link
          to="/reported-comments"
          style={isActive("/reported-comments") ? { ...linkStyle, ...activeLinkStyle } : linkStyle}
        >
          Reported Comments
        </Link>
      </nav>
    </header>
  );
};

const headerStyle = {
  color: "#333",
  padding: "20px",
};

const logoutContainerStyle = {
  textAlign: "right",
  marginBottom: "10px",
  marginTop: "-10px" // Add margin below the logout button

};

const navStyle = {
  display: "flex",
  justifyContent: "center",
};

const linkStyle = {
  width: 400, // Fixed width for each link
  height: 120, // Fixed height for each link
  border: "1px solid #ffa500",
  color: "#ffa500",
  fontSize: "20px",
  fontWeight: "bold",
  textDecoration: "none",
  textAlign: "center",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  marginRight: "10px", // Add margin between links and logout button
};

const logoutButtonStyle = {
  color: "#008000",
  // color: "#ffa500",
  fontSize: "20px",
  fontWeight: "bold",
  backgroundColor: "transparent",
  border: "none",
  cursor: "pointer",
};

export default AdminNavigation;
