// components/DeleteConfirmationModal.js
import React from "react";

const AdminDeleteCommentConfirmation = ({ onConfirm, onCancel }) => {
  return (
    <div style={modalStyle}>
      <h2 style={{textAlign: 'center'}}>Delete Comment</h2>
      <p style={{textAlign: 'center', marginBottom: "25px"}}>Are you sure you want to delete this comment?</p>
      <div style={{ textAlign: "center" }}>
        <button onClick={onConfirm} style={confirmButtonStyle}>Yes</button>
        <button onClick={onCancel} style={cancelButtonStyle}>No</button>
      </div>
    </div>
  );
};

const modalStyle = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  backgroundColor: "#fff",
  padding: "10px 40px 40px 40px",
  borderRadius: "5px",
  boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.3)",
  zIndex: "999",
};

const confirmButtonStyle = {
  padding: "8px 36px",
  borderRadius: "5px",
  backgroundColor: "red",
  color: "#111",
  border: "1px solid #777",
  marginRight: "10px",
};

const cancelButtonStyle = {
  padding: "8px 36px",
  borderRadius: "5px",
  backgroundColor: "#ccc",
  color: "#333",
  border: "1px solid #777",
};

export default AdminDeleteCommentConfirmation;
