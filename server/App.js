
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const userRoutes = require('./Routes/userRoute');
const newsRoutes = require('./Routes/newsRoutes');
const cors = require('cors');
const app = express();
const cookieParser = require('cookie-parser');
const path = require('path');

// Middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

// Parse cookies
app.use(cookieParser());

// MongoDB connection
mongoose.connect('mongodb+srv://12220059gcit:P2GHQjkN50VasV7X@cluster0.xrt5d17.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0')
    .then(() => {
        console.log('Connected to MongoDB');
    })
    .catch((error) => {
        console.log('Error connecting to MongoDB:', error);
    });

// Routes
app.use('/api/users', userRoutes);
app.use('/api/news', newsRoutes);

// Error handling middleware
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use('/uploads/profile', express.static(path.join(__dirname, 'uploads/profile')));
// Define a route to get a list of all image filenames
app.get("/uploads/list", (req, res) => {
    // Read the directory and list all image filenames
    const fs = require("fs");
    const directoryPath = __dirname + "/uploads"; // Set the path to your uploads directory

    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            return res.status(500).json({ error: "Error listing images" });
        }

        // Only return filenames, not the full path
        res.json(files);
    });
});

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
