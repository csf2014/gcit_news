
const News = require('../Model/newsModel');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const fs = require('fs'); // Import the 'fs' module to work with the filesystem

// Multer configuration for image upload
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/'); // Save uploaded files to the 'uploads' directory
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname); // Keep the original filename
    },
});

const upload = multer({ storage: storage });

exports.createNews = async (req, res) => {
    try {
        const { title, content, category, reporter, image } = req.body;

        // Check if the user is an admin (you may have a function to verify this)
        const isAdmin = req.user && req.user.role === 'admin';

        const newsData = {
            title,
            content,
            category,
            reporter,
            approved: isAdmin ? true : false, // Automatically approve if admin, else mark as pending
        };

        if (image) {
            // If image URL is provided, store it in the database
            newsData.image = image;
        } else if (req.file) {
            // If image file is uploaded, store its path in the database
            newsData.image = req.file.path;
        } else {
            // If neither image URL nor image file is provided, return an error
            return res.status(400).json({ message: 'Image is required' });
        }

        const news = await News.create(newsData);
        res.status(201).json({ news });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
};

// Get all news articles
exports.getAllNews = async (req, res) => {
    try {
        const news = await News.find();
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Get one news article by ID
exports.getNewsById = async (req, res) => {
    try {
        const news = await News.findById(req.params.id);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Update a news article by ID
exports.updateNewsById = async (req, res) => {
    try {
        const news = await News.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Delete a news article by ID
exports.deleteNewsById = async (req, res) => {
    try {
        const news = await News.findByIdAndDelete(req.params.id);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }

        // Remove the image file associated with the deleted news article
        if (news.image) {
            fs.unlinkSync(news.image); // Delete the image file from the filesystem
        }

        res.status(200).json({ message: 'News article deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Search for news articles by keyword
exports.searchNewsByKeyword = async (req, res) => {
    try {
        const keyword = req.query.keyword;
        const news = await News.find({ $text: { $search: keyword } });
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};


// exports.createComment = async (req, res) => {
//     try {
//         const { text } = req.body;

//         // Check if the user is authenticated
//         const token = req.cookies.jwt;
//         if (!token) {
//             return res.status(401).json({ error: 'Unauthorized - Please log in' });
//         }

//         const decoded = jwt.verify(token, 'your-secret-key'); // Replace 'your-secret-key' with your actual secret key
//         const userId = decoded.id;

//         const news = await News.findById(req.params.id);
//         if (!news) {
//             return res.status(404).json({ message: 'News article not found' });
//         }

//         // Add the comment
//         news.comments.push({ text, user: userId });
//         await news.save();

//         // Populate the newly added comment with user details
//         const populatedNews = await News.findById(req.params.id).populate({
//             path: 'comments.user',
//             select: 'name profileImage'// Assuming the user's name is stored in the 'name' field
//         });

//         // Get the newly added comment
//         const addedComment = populatedNews.comments.find(comment => comment.text === text && comment.user.id === userId);

//         res.status(201).json({
//             _id: addedComment._id,
//             text: addedComment.text,
//             // user: addedComment.user.name, // Include the user's name in the response
//             user: {
//                 name: addedComment.user.name,
//                 profileImage: addedComment.user.profileImage // Include the user's profile image in the response
//             },
//             createdAt: addedComment.createdAt
//         });
//     } catch (err) {
//         res.status(400).json({ message: err.message });
//     }
// };
exports.createComment = async (req, res) => {
    try {
        const { text } = req.body;

        // Check if the user is authenticated
        const token = req.cookies.jwt;
        if (!token) {
            return res.status(401).json({ error: 'Unauthorized - Please log in' });
        }

        const decoded = jwt.verify(token, 'your-secret-key'); // Replace 'your-secret-key' with your actual secret key
        const userId = decoded.id;

        const news = await News.findById(req.params.id);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }

        // Add the comment
        news.comments.push({ text, user: userId });
        await news.save();

        // Populate the newly added comment with user details
        const populatedNews = await News.findById(req.params.id).populate({
            path: 'comments.user',
            select: 'name profileImage' // Assuming the user's name is stored in the 'name' field
        });

        // Get the newly added comment
        const addedComment = populatedNews.comments.find(comment => comment.text === text && comment.user && comment.user.id === userId);

        if (!addedComment || !addedComment.user) {
            return res.status(500).json({ message: 'Failed to add comment or user details are missing' });
        }

        res.status(201).json({
            _id: addedComment._id,
            text: addedComment.text,
            user: {
                name: addedComment.user.name,
                profileImage: addedComment.user.profileImage // Include the user's profile image in the response
            },
            createdAt: addedComment.createdAt
        });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
};
// // Get all comments for a particular news article
// exports.getAllCommentsForNews = async (req, res) => {
//     try {
//         const newsId = req.params.id;

//         // Find the news article by ID
//         const news = await News.findById(newsId);
//         if (!news) {
//             return res.status(404).json({ message: 'News article not found' });
//         }

//         // Extract comments from the news article
//         const comments = news.comments;

//         res.status(200).json({ comments });
//     } catch (err) {
//         console.error('Error:', err);
//         res.status(500).json({ message: 'Internal server error' });
//     }
// };
// exports.getAllCommentsForNews = async (req, res) => {
//     try {
//         const newsId = req.params.id;

//         // Find the news article by ID and populate the user details in the comments
//         const news = await News.findById(newsId).populate({
//             path: 'comments',
//             populate: {
//                 path: 'user',
//                 select: 'name'
//             }
//         });

//         if (!news) {
//             return res.status(404).json({ message: 'News article not found' });
//         }

//         // Extract comments from the news article with user names
//         const comments = news.comments.map(comment => ({
//             _id: comment._id,
//             text: comment.text,
//             user: comment.user ? comment.user.name : 'Unknown', // Check if user exists
//             createdAt: comment.createdAt
//         }));

//         res.status(200).json({ comments });
//     } catch (err) {
//         console.error('Error:', err);
//         res.status(500).json({ message: 'Internal server error' });
//     }
// };

exports.getAllCommentsForNews = async (req, res) => {
    try {
        const newsId = req.params.id;

        // Find the news article by ID and populate the user details in the comments
        const news = await News.findById(newsId).populate({
            path: 'comments',
            populate: {
                path: 'user',
                select: 'name profileImage' // Include both name and profileImage
            }
        });

        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }

        // Extract comments from the news article with user names and profile images
        const comments = news.comments.map(comment => ({
            _id: comment._id,
            text: comment.text,
            user: comment.user ? {
                name: comment.user.name,
                profileImage: comment.user.profileImage
            } : { name: 'Unknown', profileImage: null }, // Check if user exists
            createdAt: comment.createdAt
        }));

        res.status(200).json({ comments });
    } catch (err) {
        console.error('Error:', err);
        res.status(500).json({ message: 'Internal server error' });
    }
};

// Update a news article by ID
exports.approveNewsById = async (req, res) => {
    try {
        const isAdmin = req.user && req.user.role === 'admin';
        if (!isAdmin) {
            return res.status(403).json({ message: 'Unauthorized - Only admins can approve news' });
        }

        const newsId = req.params.id;
        const updatedData = { ...req.body, approved: true }; // Set approved to true

        const news = await News.findByIdAndUpdate(newsId, updatedData, { new: true });
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }

        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};


exports.getUnapprovedNews = async (req, res) => {
    try {
        // Check if the user is an admin
        const isAdmin = req.user && req.user.role === 'admin';
        if (!isAdmin) {
            return res.status(403).json({ message: 'Unauthorized - Only admins can access unapproved news' });
        }

        // Retrieve all unapproved news articles
        const unapprovedNews = await News.find({ approved: false });
        res.status(200).json(unapprovedNews); // Return just the array of unapproved news
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};
// Get all approved news articles
exports.getApprovedNews = async (req, res) => {
    try {
        const approvedNews = await News.find({ approved: true });
        res.status(200).json({ news: approvedNews });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Add Bookmark: Create a bookmark on a news article
exports.createBookmark = async (req, res) => {
    try {
        const newsId = req.params.id;

        // Check if the user is authenticated
        const token = req.cookies.jwt;
        if (!token) {
            return res.status(401).json({ error: 'Unauthorized - Please log in' });
        }

        const decoded = jwt.verify(token, 'your-secret-key'); // Replace 'your-secret-key' with your actual secret key
        const userId = decoded.id;

        const news = await News.findById(newsId);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }

        // Check if the news article is already bookmarked by the user
        const isBookmarked = news.bookmarks.some(bookmark => bookmark.user.toString() === userId);
        if (isBookmarked) {
            return res.status(400).json({ message: 'News article already bookmarked by the user' });
        }

        // Add the bookmark
        news.bookmarks.push({ user: userId });
        await news.save();

        res.status(201).json({ message: 'News article bookmarked successfully' });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
};
// Remove Bookmark: Remove a bookmark from a news article
exports.removeBookmark = async (req, res) => {
    try {
        const newsId = req.params.id;

        // Check if the user is authenticated
        const token = req.cookies.jwt;
        if (!token) {
            return res.status(401).json({ error: 'Unauthorized - Please log in' });
        }

        const decoded = jwt.verify(token, 'your-secret-key'); // Replace 'your-secret-key' with your actual secret key
        const userId = decoded.id;

        const news = await News.findById(newsId);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }

        // Check if the news article is bookmarked by the user
        const bookmarkIndex = news.bookmarks.findIndex(bookmark => bookmark.user.toString() === userId);
        if (bookmarkIndex === -1) {
            return res.status(400).json({ message: 'News article is not bookmarked by the user' });
        }

        // Remove the bookmark
        news.bookmarks.splice(bookmarkIndex, 1);
        await news.save();

        res.status(200).json({ message: 'News article unbookmarked successfully' });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
};

exports.reportComment = async (req, res) => {
    try {
        const newsId = req.params.id; // Extract the newsId from req.params
        const commentId = req.params.commentId; // Extract the commentId from req.params

        // Check if the user is authenticated
        const token = req.cookies.jwt;
        if (!token) {
            return res.status(401).json({ error: 'Unauthorized - Please log in' });
        }

        const decoded = jwt.verify(token, 'your-secret-key'); // Replace 'your-secret-key' with your actual secret key
        const userId = decoded.id;

        // Find the news article
        const news = await News.findById(newsId);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }

        // Find the comment within the news article
        const comment = news.comments.id(commentId);
        if (!comment) {
            return res.status(404).json({ message: 'Comment not found' });
        }

        // Check if the user has already reported the comment
        const existingReport = comment.reports.find(report => report.user.toString() === userId);
        if (existingReport) {
            return res.status(400).json({ message: 'You have already reported this comment' });
        }

        // Set the reported field to true
        comment.reported = true;

        // Add the user's report to the comment
        comment.reports.push({ user: userId });
        comment.reportCount += 1; // Increment the report count
        await news.save();

        // Get the updated report count for the comment
        const reportCount = comment.reportCount;

        res.status(200).json({ message: 'Comment reported successfully', reportCount });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
};

exports.getAllReportedComments = async (req, res) => {
    try {
        // Find all news articles with reported comments
        const newsWithReportedComments = await News.find({ 'comments.reported': true })
            .populate({
                path: 'comments.user',
                select: 'username name' // Select both username and name
            })
            .select('title createdAt comments');

        if (!newsWithReportedComments || newsWithReportedComments.length === 0) {
            return res.status(404).json({ message: 'No reported comments found' });
        }

        // Extract reported comments and related information
        const reportedComments = newsWithReportedComments.reduce((acc, news) => {
            news.comments.forEach(comment => {
                if (comment.reported) {
                    const reporterUsername = comment.reports.length > 0 ? comment.reports[0].user.username : 'Unknown';
                    const commentUsername = comment.user ? comment.user.name : 'Unknown'; // Add null check here
                    const commentUserId = comment.user ? comment.user._id : null; // Add null check for user ID
                    acc.push({
                        _id: comment._id,
                        newsTitle: news.title,
                        newsCreatedAt: news.createdAt,
                        commentText: comment.text,
                        commentUsername: commentUsername, // Use the updated commentUsername variable
                        reportCount: comment.reportCount,
                        reporterUsername: reporterUsername,
                        commentCreatedAt: comment.createdAt, // Include the creation date of the comment
                        commentUserId: commentUserId // Include the user ID of the comment maker
                    });
                }
            });
            return acc;
        }, []);

        if (reportedComments.length === 0) {
            return res.status(404).json({ message: 'No reported comments found' });
        }

        res.status(200).json(reportedComments);
    } catch (err) {
        console.error('Error:', err);
        res.status(500).json({ message: 'Internal server error' });
    }
};

exports.deleteReportedComment = async (req, res) => {
    try {
        // Check if the user is an admin
        const isAdmin = req.user && req.user.role === 'admin';
        console.log("isAdmin: ", isAdmin); // Log isAdmin value for debugging
        console.log("Decoded User: ", req.user); // Log decoded user object for debugging
        if (!isAdmin) {
            return res.status(403).json({ message: 'Unauthorized - Only admins can delete comment' });
        }

        const commentId = req.params.commentId;

        // Find the news article containing the reported comment
        const news = await News.findOne({ 'comments._id': commentId });
        if (!news) {
            return res.status(404).json({ message: 'News article containing the reported comment not found' });
        }

        // Find the reported comment within the news article
        const commentIndex = news.comments.findIndex(comment => comment._id.toString() === commentId);
        if (commentIndex === -1) {
            return res.status(404).json({ message: 'Reported comment not found' });
        }

        // Remove the reported comment from the parent document
        news.comments.splice(commentIndex, 1);

        // Save the parent document
        await news.save();

        res.status(200).json({ message: 'Reported comment deleted successfully' });
    } catch (err) {
        console.error('Error:', err);
        res.status(500).json({ message: 'Internal server error' });
    }
};

// Get all news articles with category "announcements"
exports.getAnnouncements = async (req, res) => {
    try {
        const announcements = await News.find({ category: 'announcements', approved: true });
        res.status(200).json({ announcements });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};
// Get all news articles with category "events"
exports.getevents = async (req, res) => {
    try {
        const events = await News.find({ category: 'events', approved: true });
        res.status(200).json({ events });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};
// Get all news articles with category "clubs"
exports.getclubs = async (req, res) => {
    try {
        const clubs = await News.find({ category: 'clubs', approved: true });
        res.status(200).json({ clubs });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

