

// const User = require('../Model/userModel');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const revokedTokens = [];

// const secretKey = 'your-secret-key'; // Define your secret key here

// const signToken = (id) => {
//     return jwt.sign({ id }, secretKey, {
//         expiresIn: '1h', // Token expires in 1 hour
//     });
// };

// const createSendToken = (user, statusCode, res) => {
//     const token = signToken(user._id);

//     // Set cookie options
//     const cookieOptions = {
//         expires: new Date(Date.now() + 1 * 60 * 60 * 1000), // Expires in 1 hour
//         httpOnly: true,
//     };

//     // Set cookie with the token
//     res.cookie('jwt', token, cookieOptions);

//     // Remove password from output
//     user.password = undefined;

//     res.status(statusCode).json({
//         status: 'success',
//         token,
//         data: {
//             user,
//         },
//     });
// };


// module.exports = { signToken, createSendToken };


// const userController = {

//     async signup(req, res) {
//         try {
//             const { name, email, password, confirmPassword } = req.body;

//             // Check if the email is already registered
//             const existingUser = await User.findOne({ email });
//             if (existingUser) {
//                 return res.status(400).json({ error: 'Email is already registered' });
//             }

//             // Check if passwords match
//             if (password !== confirmPassword) {
//                 return res.status(400).json({ error: 'Passwords do not match' });
//             }

//             // Hash the password
//             const hashedPassword = await bcrypt.hash(password, 10); // 10 is the salt rounds

//             // Create a new user with hashed password, verified set to false, and registration date
//             const user = new User({
//                 name,
//                 email,
//                 password: hashedPassword,
//                 confirmPassword: hashedPassword,
//                 verified: false,
//                 registrationDate: new Date() // Automatically set the registration date
//             });
//             await user.save();

//             // Generate and send OTP through email
//             const otp = Math.floor(100000 + Math.random() * 900000).toString();
//             user.otp = otp;
//             user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
//             await user.save();

//             const transporter = nodemailer.createTransport({
//                 service: 'Gmail',
//                 auth: {
//                     user: 'sangaychoden2800@gmail.com',
//                     pass: 'brme fzad bcil mxsj'
//                 }
//             });

//             const mailOptions = {
//                 from: 'sangaychoden2800@gmail.com',
//                 to: email,
//                 subject: 'Verification OTP',
//                 text: `Your OTP for signup is ${otp}`
//             };

//             transporter.sendMail(mailOptions, (error, info) => {
//                 if (error) {
//                     console.log('Error sending email:', error);
//                     return res.status(500).json({ error: 'Error sending OTP email' });
//                 } else {
//                     console.log('Email sent:', info.response);
//                     // Send response indicating account creation is pending
//                     const formattedRegistrationDate = user.registrationDate.toLocaleDateString('en-GB');
//                     res.status(201).json({
//                         message: 'User registration pending. Check your email for OTP.',
//                         registrationDate: formattedRegistrationDate // Format date as day/month/year
//                     });
//                 }
//             });
//         } catch (error) {
//             console.log('Error in signup:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },


//     async login(req, res) {
//         try {
//             const { email, password } = req.body;

//             // Find the user by email
//             const user = await User.findOne({ email });

//             // Check if user exists
//             if (!user) {
//                 return res.status(400).json({ error: 'User not found' });
//             }

//             // Check if the user's account is verified
//             if (!user.verified) {
//                 return res.status(400).json({ error: 'Account not verified. Please verify your email.' });
//             }

//             // Compare passwords
//             const passwordMatch = await bcrypt.compare(password, user.password);
//             if (!passwordMatch) {
//                 return res.status(400).json({ error: 'Incorrect password' });
//             }

//             // Send token
//             createSendToken(user, 200, res);
//         } catch (error) {
//             console.log('Error in login:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },

//     async verifyOTP(req, res) {
//         try {
//             const { email, otp } = req.body;

//             // Find the user by email and OTP
//             const user = await User.findOne({ email, otp });

//             // Check if user exists and if the OTP is valid
//             if (!user) {
//                 return res.status(400).json({ error: 'Invalid OTP or email' });
//             }

//             // Check if OTP is expired (assuming otpExpiration is stored in the database)
//             if (user.otpExpiration && Date.now() > user.otpExpiration) {
//                 return res.status(400).json({ error: 'OTP has expired' });
//             }

//             // Update user as verified
//             user.verified = true;
//             await user.save();

//             res.status(200).json({ message: 'OTP verified successfully' });
//         } catch (error) {
//             console.error('Error in verifying OTP:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },

//     async resendOTP(req, res) {
//         try {
//             const { email } = req.body;

//             // Find the user by email
//             const user = await User.findOne({ email });

//             // Check if user exists
//             if (!user) {
//                 return res.status(400).json({ error: 'User not found' });
//             }

//             // Generate and send new OTP through email
//             const otp = Math.floor(100000 + Math.random() * 900000).toString();
//             user.otp = otp;
//             user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
//             await user.save();

//             const transporter = nodemailer.createTransport({
//                 service: 'Gmail',
//                 auth: {
//                     user: 'sangaychoden2800@gmail.com',
//                     pass: 'brme fzad bcil mxsj'
//                 }
//             });

//             const mailOptions = {
//                 from: 'sangaychoden2800@gmail.com',
//                 to: email,
//                 subject: 'New OTP',
//                 text: `Your new OTP is ${otp}`
//             };

//             transporter.sendMail(mailOptions, (error, info) => {
//                 if (error) {
//                     console.log('Error sending email:', error);
//                     return res.status(500).json({ error: 'Error sending OTP email' });
//                 } else {
//                     console.log('Email sent:', info.response);
//                     res.status(200).json({ message: 'New OTP sent successfully. Check your email.' });
//                 }
//             });
//         } catch (error) {
//             console.error('Error in resending OTP:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },

//     async verifyResentOTP(req, res) {
//         try {
//             const { email, otp } = req.body;

//             // Find the user by email and OTP
//             const user = await User.findOne({ email, otp });

//             // Check if user exists and if the OTP is valid
//             if (!user) {
//                 return res.status(400).json({ error: 'Invalid OTP or email' });
//             }

//             // Check if OTP is expired (assuming otpExpiration is stored in the database)
//             if (user.otpExpiration && Date.now() > user.otpExpiration) {
//                 return res.status(400).json({ error: 'OTP has expired' });
//             }

//             // Update user as verified
//             user.verified = true;
//             await user.save();

//             res.status(200).json({ message: 'OTP verified successfully' });
//         } catch (error) {
//             console.error('Error in verifying resent OTP:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },

//     async getAllUsers(req, res) {
//         try {
//             // Find all users
//             const users = await User.find();

//             res.status(200).json({ status: 'success', data: users });
//         } catch (error) {
//             console.error('Error getting all users:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },
//     async deleteUser(req, res) {
//         try {
//             const isAdmin = req.user && req.user.role === 'admin';
//             if (!isAdmin) {
//                 return res.status(403).json({ error: 'Unauthorized - Only admins can delete users' });
//             }

//             const userId = req.params.id;

//             // Find user by ID and delete
//             const deletedUser = await User.findByIdAndDelete(userId);

//             if (!deletedUser) {
//                 return res.status(404).json({ error: 'User not found' });
//             }

//             res.status(200).json({ message: 'User deleted successfully', deletedUser });
//         } catch (error) {
//             console.error('Error deleting user:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },
//     // 
//     async searchUsers(req, res) {
//         try {
//             // Check if the user is an admin
//             if (!(req.user && req.user.role === 'admin')) {
//                 return res.status(403).json({ error: 'Unauthorized - Only admins can perform this action' });
//             }

//             const { name, email } = req.query;

//             // Construct the query based on the provided parameters
//             const query = {};
//             if (name) {
//                 query.name = { $regex: new RegExp(name, 'i') }; // Case-insensitive search for name
//             }
//             if (email) {
//                 query.email = { $regex: new RegExp(email, 'i') }; // Case-insensitive search for email
//             }

//             // Execute the query
//             const users = await User.find(query);

//             // Check if there are no matching users
//             if (users.length === 0) {
//                 return res.status(404).json({ message: 'No users found matching the search criteria' });
//             }

//             res.status(200).json({ status: 'success', data: users });
//         } catch (error) {
//             console.error('Error searching users:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },
//     logout(req, res) {
//         try {
//             const token = req.cookies.jwt; // Assuming token is stored in cookies

//             // Check if token exists
//             if (!token) {
//                 return res.status(401).json({ error: 'You are not logged in' });
//             }

//             // Add token to revoked tokens list
//             revokedTokens.push(token);

//             // Clear cookie
//             res.clearCookie('jwt');

//             res.status(200).json({ message: 'Logout successful' });
//         } catch (error) {
//             console.error('Error logging out:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },


// };

// module.exports = userController;
// const AppError = require('../utils/appError');
// const User = require('../Model/userModel');
// const multer = require('multer');
// const path = require('path');
// const fs = require('fs');
// const { promisify } = require('util');
// const unlinkAsync = promisify(fs.unlink);
// const secretKey = 'your-secret-key'; // Define your secret key here

// const signToken = (id) => {
//     return jwt.sign({ id }, secretKey, {
//         expiresIn: '1h', // Token expires in 1 hour
//     });
// };

// const createSendToken = (user, statusCode, res) => {
//     const token = signToken(user._id);

//     // Set cookie options
//     const cookieOptions = {
//         expires: new Date(Date.now() + 1 * 60 * 60 * 1000), // Expires in 1 hour
//         httpOnly: true,
//     };

//     // Set cookie with the token
//     res.cookie('jwt', token, cookieOptions);

//     // Remove password from output
//     user.password = undefined;

//     res.status(statusCode).json({
//         status: 'success',
//         token,
//         data: {
//             user,
//         },
//     });
// };


// module.exports = { signToken, createSendToken };

// // Multer configuration for image upload
// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, 'uploads/'); // Save uploaded files to the 'uploads' directory
//     },
//     filename: function (req, file, cb) {
//         cb(null, file.originalname); // Keep the original filename
//     },
// });

// const upload = multer({ storage: storage });
// // const upload = multer({
// //     storage: multerStorage,
// //     fileFilter: multerFilter,
// // });

// exports.uploadUserPhoto = upload.single('image');

// // Upload image function
// const uploadImage = async (file) => {
//     const uploadDir = path.join(__dirname, '..', 'uploads');

//     if (!fs.existsSync(uploadDir)) {
//         fs.mkdirSync(uploadDir, { recursive: true });
//     }

//     const filename = `${Date.now()}-${file.originalname}`;
//     const filePath = path.join(uploadDir, filename);

//     // Move the uploaded file to the upload directory
//     await fs.promises.rename(file.path, filePath);

//     return `/uploads/${filename}`;
// };

// // User controller methods
// const userController = {
//     async signup(req, res) {
//         try {
//             const { name, email, password, confirmPassword } = req.body;

//             const existingUser = await User.findOne({ email });
//             if (existingUser) {
//                 return res.status(400).json({ error: 'Email is already registered' });
//             }

//             if (password !== confirmPassword) {
//                 return res.status(400).json({ error: 'Passwords do not match' });
//             }

//             const hashedPassword = await bcrypt.hash(password, 10);

//             const user = new User({
//                 name,
//                 email,
//                 password: hashedPassword,
//                 confirmPassword: hashedPassword,
//                 verified: false,
//                 registrationDate: new Date(),
//                 profileImage: '/default-profile-image.png',
//             });
//             await user.save();

//             const otp = Math.floor(100000 + Math.random() * 900000).toString();
//             user.otp = otp;
//             user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
//             await user.save();

//             const transporter = nodemailer.createTransport({
//                 service: 'Gmail',
//                 auth: {
//                     user: 'your-email@gmail.com',
//                     pass: 'your-email-password'
//                 }
//             });

//             const mailOptions = {
//                 from: 'your-email@gmail.com',
//                 to: email,
//                 subject: 'Verification OTP',
//                 text: `Your OTP for signup is ${otp}`
//             };

//             transporter.sendMail(mailOptions, (error, info) => {
//                 if (error) {
//                     console.log('Error sending email:', error);
//                     return res.status(500).json({ error: 'Error sending OTP email' });
//                 } else {
//                     console.log('Email sent:', info.response);
//                     const formattedRegistrationDate = user.registrationDate.toLocaleDateString('en-GB');
//                     res.status(201).json({
//                         message: 'User registration pending. Check your email for OTP.',
//                         registrationDate: formattedRegistrationDate
//                     });
//                 }
//             });
//         } catch (error) {
//             console.log('Error in signup:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },

//     // async uploadProfileImage(req, res) {
//     //     try {
//     //         if (!req.file) {
//     //             return res.status(400).json({ error: 'No image uploadedtt' });
//     //         }

//     //         const file = req.file;
//     //         const imagePath = await uploadImage(file);

//     //         res.status(200).json({ status: 'success', imagePath });
//     //     } catch (error) {
//     //         console.error('Error uploading profile image:', error);
//     //         res.status(500).json({ error: 'Internal server error' });
//     //     }
//     // },
//     async uploadProfileImage(req, res) {
//         try {
//             if (!req.file) {
//                 return res.status(400).json({ error: 'No image uploaded' });
//             }

//             const file = req.file;
//             const imagePath = await uploadImage(file);

//             // Return the image path in the response
//             res.status(200).json({ status: 'success', imagePath });
//         } catch (error) {
//             console.error('Error uploading profile image:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },
const User = require('../Model/userModel');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const { promisify } = require('util');
const unlinkAsync = promisify(fs.unlink);
const secretKey = 'your-secret-key'; // Define your secret key here

const signToken = (id) => {
    return jwt.sign({ id }, secretKey, {
        expiresIn: '1h', // Token expires in 1 hour
    });
};

const createSendToken = (user, statusCode, res) => {
    const token = signToken(user._id);

    // Set cookie options
    const cookieOptions = {
        expires: new Date(Date.now() + 1 * 60 * 60 * 1000), // Expires in 1 hour
        httpOnly: true,
    };

    // Set cookie with the token
    res.cookie('jwt', token, cookieOptions);

    // Remove password from output
    user.password = undefined;

    res.status(statusCode).json({
        status: 'success',
        token,
        data: {
            user,
        },
    });
};


module.exports = { signToken, createSendToken };

// Multer configuration for image upload
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/profile/'); // Save uploaded files to the 'uploads' directory
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname); // Keep the original filename
    },
});

const upload = multer({ storage: storage });
// const upload = multer({
//     storage: multerStorage,
//     fileFilter: multerFilter,
// });

exports.uploadUserPhoto = upload.single('image');

const uploadImage = async (file) => {
    const uploadDir = path.join(__dirname, '..', 'uploads/profile');

    if (!fs.existsSync(uploadDir)) {
        fs.mkdirSync(uploadDir, { recursive: true });
    }

    const filename = `${Date.now()}-${file.originalname}`;
    const filePath = path.join(uploadDir, filename);

    // Move the uploaded file to the upload directory
    await fs.promises.rename(file.path, filePath);

    return `/uploads/profile/${filename}`; // Ensure the path format is correct
};


// User controller methods
const userController = {
    async signup(req, res) {
        try {
            const { name, email, password, confirmPassword } = req.body;

            const existingUser = await User.findOne({ email });
            if (existingUser) {
                return res.status(400).json({ error: 'Email is already registered' });
            }

            if (password !== confirmPassword) {
                return res.status(400).json({ error: 'Passwords do not match' });
            }

            const hashedPassword = await bcrypt.hash(password, 10);

            const user = new User({
                name,
                email,
                password: hashedPassword,
                confirmPassword: hashedPassword,
                verified: false,
                registrationDate: new Date(),
                profileImage: '/default-profile-image.png',
            });
            await user.save();

            const otp = Math.floor(100000 + Math.random() * 900000).toString();
            user.otp = otp;
            user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
            await user.save();

            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'sangaychoden2800@gmail.com',
                    pass: 'brme fzad bcil mxsj'
                }
            });

            const mailOptions = {
                from: 'your-email@gmail.com',
                to: email,
                subject: 'Verification OTP',
                text: `Your OTP for signup is ${otp}`
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log('Error sending email:', error);
                    return res.status(500).json({ error: 'Error sending OTP email' });
                } else {
                    console.log('Email sent:', info.response);
                    const formattedRegistrationDate = user.registrationDate.toLocaleDateString('en-GB');
                    res.status(201).json({
                        message: 'User registration pending. Check your email for OTP.',
                        registrationDate: formattedRegistrationDate
                    });
                }
            });
        } catch (error) {
            console.log('Error in signup:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },



    async uploadProfileImage(req, res) {
        try {
            if (!req.file) {
                return res.status(400).json({ error: 'No image uploaded' });
            }

            const file = req.file;
            const imagePath = await uploadImage(file);

            // Update the user's profile image path in the database
            const user = await User.findByIdAndUpdate(req.user.id, { profileImage: imagePath }, { new: true });

            // Return the updated user document in the response
            res.status(200).json({ status: 'success', data: user });
        } catch (error) {
            console.error('Error uploading profile image:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },


    async login(req, res) {
        try {
            const { email, password } = req.body;

            // Find the user by email
            const user = await User.findOne({ email });

            // Check if user exists
            if (!user) {
                return res.status(400).json({ error: 'User not found' });
            }

            // Check if the user's account is verified
            if (!user.verified) {
                return res.status(400).json({ error: 'Account not verified. Please verify your email.' });
            }

            // Compare passwords
            const passwordMatch = await bcrypt.compare(password, user.password);
            if (!passwordMatch) {
                return res.status(400).json({ error: 'Incorrect password' });
            }

            // Send token
            createSendToken(user, 200, res);
        } catch (error) {
            console.log('Error in login:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async verifyOTP(req, res) {
        try {
            const { email, otp } = req.body;

            // Find the user by email and OTP
            const user = await User.findOne({ email, otp });

            // Check if user exists and if the OTP is valid
            if (!user) {
                return res.status(400).json({ error: 'Invalid OTP or email' });
            }

            // Check if OTP is expired (assuming otpExpiration is stored in the database)
            if (user.otpExpiration && Date.now() > user.otpExpiration) {
                return res.status(400).json({ error: 'OTP has expired' });
            }

            // Update user as verified
            user.verified = true;
            await user.save();

            res.status(200).json({ message: 'OTP verified successfully' });
        } catch (error) {
            console.error('Error in verifying OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async resendOTP(req, res) {
        try {
            const { email } = req.body;

            // Find the user by email
            const user = await User.findOne({ email });

            // Check if user exists
            if (!user) {
                return res.status(400).json({ error: 'User not found' });
            }

            // Generate and send new OTP through email
            const otp = Math.floor(100000 + Math.random() * 900000).toString();
            user.otp = otp;
            user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
            await user.save();

            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'sangaychoden2800@gmail.com',
                    pass: 'brme fzad bcil mxsj'
                }
            });
            const mailOptions = {
                from: 'sangaychoden2800@gmail.com',
                to: email,
                subject: 'Verification OTP',
                text: `Your OTP for signup is ${otp}`
            };


            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log('Error sending email:', error);
                    return res.status(500).json({ error: 'Error sending OTP email' });
                } else {
                    console.log('Email sent:', info.response);
                    res.status(200).json({ message: 'New OTP sent successfully. Check your email.' });
                }
            });
        } catch (error) {
            console.error('Error in resending OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async verifyResentOTP(req, res) {
        try {
            const { email, otp } = req.body;

            // Find the user by email and OTP
            const user = await User.findOne({ email, otp });

            // Check if user exists and if the OTP is valid
            if (!user) {
                return res.status(400).json({ error: 'Invalid OTP or email' });
            }

            // Check if OTP is expired (assuming otpExpiration is stored in the database)
            if (user.otpExpiration && Date.now() > user.otpExpiration) {
                return res.status(400).json({ error: 'OTP has expired' });
            }

            // Update user as verified
            user.verified = true;
            await user.save();

            res.status(200).json({ message: 'OTP verified successfully' });
        } catch (error) {
            console.error('Error in verifying resent OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async getAllUsers(req, res) {
        try {
            // Find all users
            const users = await User.find();

            res.status(200).json({ status: 'success', data: users });
        } catch (error) {
            console.error('Error getting all users:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },
    async deleteUser(req, res) {
        try {
            const isAdmin = req.user && req.user.role === 'admin';
            if (!isAdmin) {
                return res.status(403).json({ error: 'Unauthorized - Only admins can delete users' });
            }

            const userId = req.params.id;

            // Find user by ID and delete
            const deletedUser = await User.findByIdAndDelete(userId);

            if (!deletedUser) {
                return res.status(404).json({ error: 'User not found' });
            }

            res.status(200).json({ message: 'User deleted successfully', deletedUser });
        } catch (error) {
            console.error('Error deleting user:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },
    // 
    async searchUsers(req, res) {
        try {
            // Check if the user is an admin
            if (!(req.user && req.user.role === 'admin')) {
                return res.status(403).json({ error: 'Unauthorized - Only admins can perform this action' });
            }

            const { name, email } = req.query;

            // Construct the query based on the provided parameters
            const query = {};
            if (name) {
                query.name = { $regex: new RegExp(name, 'i') }; // Case-insensitive search for name
            }
            if (email) {
                query.email = { $regex: new RegExp(email, 'i') }; // Case-insensitive search for email
            }

            // Execute the query
            const users = await User.find(query);

            // Check if there are no matching users
            if (users.length === 0) {
                return res.status(404).json({ message: 'No users found matching the search criteria' });
            }

            res.status(200).json({ status: 'success', data: users });
        } catch (error) {
            console.error('Error searching users:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },
    async logout(req, res) {
        try {
            const token = req.cookies.jwt; // Assuming token is stored in cookies

            // Check if token exists
            if (!token) {
                return res.status(401).json({ error: 'You are not logged in' });
            }

            // Add token to revoked tokens list
            revokedTokens.push(token);

            // Clear cookie
            res.clearCookie('jwt');

            res.status(200).json({ message: 'Logout successful' });
        } catch (error) {
            console.error('Error logging out:', error);
            res.status(500).json({
                error: 'Internal server error'
            });
        }
    },

    async login(req, res) {
        try {
            const { email, password } = req.body;

            // Find the user by email
            const user = await User.findOne({ email });

            // Check if user exists
            if (!user) {
                return res.status(400).json({ error: 'User not found' });
            }

            // Check if the user's account is verified
            if (!user.verified) {
                return res.status(400).json({ error: 'Account not verified. Please verify your email.' });
            }

            // Compare passwords
            const passwordMatch = await bcrypt.compare(password, user.password);
            if (!passwordMatch) {
                return res.status(400).json({ error: 'Incorrect password' });
            }

            // Send token
            createSendToken(user, 200, res);
        } catch (error) {
            console.log('Error in login:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async verifyOTP(req, res) {
        try {
            const { email, otp } = req.body;

            // Find the user by email and OTP
            const user = await User.findOne({ email, otp });

            // Check if user exists and if the OTP is valid
            if (!user) {
                return res.status(400).json({ error: 'Invalid OTP or email' });
            }

            // Check if OTP is expired (assuming otpExpiration is stored in the database)
            if (user.otpExpiration && Date.now() > user.otpExpiration) {
                return res.status(400).json({ error: 'OTP has expired' });
            }

            // Update user as verified
            user.verified = true;
            await user.save();

            res.status(200).json({ message: 'OTP verified successfully' });
        } catch (error) {
            console.error('Error in verifying OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async resendOTP(req, res) {
        try {
            const { email } = req.body;

            // Find the user by email
            const user = await User.findOne({ email });

            // Check if user exists
            if (!user) {
                return res.status(400).json({ error: 'User not found' });
            }

            // Generate and send new OTP through email
            const otp = Math.floor(100000 + Math.random() * 900000).toString();
            user.otp = otp;
            user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
            await user.save();

            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'sangaychoden2800@gmail.com',
                    pass: 'brme fzad bcil mxsj'
                }
            });

            const mailOptions = {
                from: 'sangaychoden2800@gmail.com',
                to: email,
                subject: 'New OTP',
                text: `Your new OTP is ${otp}`
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log('Error sending email:', error);
                    return res.status(500).json({ error: 'Error sending OTP email' });
                } else {
                    console.log('Email sent:', info.response);
                    res.status(200).json({ message: 'New OTP sent successfully. Check your email.' });
                }
            });
        } catch (error) {
            console.error('Error in resending OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async verifyResentOTP(req, res) {
        try {
            const { email, otp } = req.body;

            // Find the user by email and OTP
            const user = await User.findOne({ email, otp });

            // Check if user exists and if the OTP is valid
            if (!user) {
                return res.status(400).json({ error: 'Invalid OTP or email' });
            }

            // Check if OTP is expired (assuming otpExpiration is stored in the database)
            if (user.otpExpiration && Date.now() > user.otpExpiration) {
                return res.status(400).json({ error: 'OTP has expired' });
            }

            // Update user as verified
            user.verified = true;
            await user.save();

            res.status(200).json({ message: 'OTP verified successfully' });
        } catch (error) {
            console.error('Error in verifying resent OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async getAllUsers(req, res) {
        try {
            // Find all users
            const users = await User.find();

            res.status(200).json({ status: 'success', data: users });
        } catch (error) {
            console.error('Error getting all users:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },
    async deleteUser(req, res) {
        try {
            const isAdmin = req.user && req.user.role === 'admin';
            if (!isAdmin) {
                return res.status(403).json({ error: 'Unauthorized - Only admins can delete users' });
            }

            const userId = req.params.id;

            // Find user by ID and delete
            const deletedUser = await User.findByIdAndDelete(userId);

            if (!deletedUser) {
                return res.status(404).json({ error: 'User not found' });
            }

            res.status(200).json({ message: 'User deleted successfully', deletedUser });
        } catch (error) {
            console.error('Error deleting user:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },
    // 
    async searchUsers(req, res) {
        try {
            // Check if the user is an admin
            if (!(req.user && req.user.role === 'admin')) {
                return res.status(403).json({ error: 'Unauthorized - Only admins can perform this action' });
            }

            const { name, email } = req.query;

            // Construct the query based on the provided parameters
            const query = {};
            if (name) {
                query.name = { $regex: new RegExp(name, 'i') }; // Case-insensitive search for name
            }
            if (email) {
                query.email = { $regex: new RegExp(email, 'i') }; // Case-insensitive search for email
            }

            // Execute the query
            const users = await User.find(query);

            // Check if there are no matching users
            if (users.length === 0) {
                return res.status(404).json({ message: 'No users found matching the search criteria' });
            }

            res.status(200).json({ status: 'success', data: users });
        } catch (error) {
            console.error('Error searching users:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },
    async logout(req, res) {
        try {
            const token = req.cookies.jwt; // Assuming token is stored in cookies

            // Check if token exists
            if (!token) {
                return res.status(401).json({ error: 'You are not logged in' });
            }

            // Add token to revoked tokens list
            revokedTokens.push(token);

            // Clear cookie
            res.clearCookie('jwt');

            res.status(200).json({ message: 'Logout successful' });
        } catch (error) {
            console.error('Error logging out:', error);
            res.status(500).json({
                error: 'Internal server error'
            });
        }
    },
};

module.exports = userController;


