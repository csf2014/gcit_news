// // userController.js

// const jwt = require('jsonwebtoken');
// // const { createSendToken } = require('./auth');


// const secretKey = 'your-secret-key'; // Define your secret key here

// const signToken = (id) => {
//     return jwt.sign({ id }, secretKey, {
//         expiresIn: '1h', // Token expires in 1 hour
//     });
// };
// const createSendToken = (user, statusCode, res) => {
//     const token = signToken(user._id);

//     // Set cookie options
//     const cookieOptions = {
//         expires: new Date(Date.now() + 1 * 60 * 60 * 1000), // Expires in 1 hour
//         httpOnly: true,
//     };

//     // Set cookie with the token
//     res.cookie('jwt', token, cookieOptions);

//     // Remove password from output
//     user.password = undefined;

//     res.status(statusCode).json({
//         status: 'success',
//         token,
//         data: {
//             user,
//         },
//     });
// };

// async function adminLogin(req, res) {
//     try {
//         const { email, password } = req.body;

//         // Check admin credentials (replace with actual admin authentication logic)
//         if (email !== 'admin@example.com' || password !== 'adminpassword') {
//             return res.status(401).json({ error: 'Invalid admin credentials' });
//         }

//         // Mock admin user
//         const adminUser = {
//             _id: 'admin_id',
//             name: 'Admin',
//             email: 'admin@example.com',
//             isAdmin: true, // Set isAdmin to true for admin
//             verified: true // Assume admin is always verified
//         };

//         // Send token for admin
//         createSendToken(adminUser, 200, res);
//     } catch (error) {
//         console.error('Error in admin login:', error);
//         res.status(500).json({ error: 'Internal server error' });
//     }
// }

// module.exports = { adminLogin };
const isAdmin = (req, res, next) => {
    if (req.user.role !== 'admin') {
        return res.status(403).json({ message: 'Unauthorized - Admin access required' });
    }
    next();
};
const jwt = require('jsonwebtoken');

const secretKey = 'your-secret-key'; // Define your secret key here

const signToken = (id, role) => {
    return jwt.sign({ id, role }, secretKey, {
        expiresIn: '1h', // Token expires in 1 hour
    });
};

const createSendToken = (user, statusCode, res) => {
    const token = signToken(user._id, user.role); // Include role in the token

    // Set cookie options
    const cookieOptions = {
        expires: new Date(Date.now() + 1 * 60 * 60 * 1000), // Expires in 1 hour
        httpOnly: true,
    };

    // Set cookie with the token
    res.cookie('jwt', token, cookieOptions);

    // Remove sensitive data from output
    user.password = undefined;

    res.status(statusCode).json({
        status: 'success',
        token,
        data: {
            user,
        },
    });
};

async function adminLogin(req, res) {
    try {
        const { email, password } = req.body;

        // Check admin credentials (replace with actual admin authentication logic)
        if (email !== 'admin@gmail.com' || password !== 'adminpassword') {
            return res.status(401).json({ error: 'Invalid admin credentials' });
        }

        // Mock admin user
        const adminUser = {
            _id: 'admin_id',
            name: 'Admin',
            email: 'admin@gmail.com',
            role: 'admin', // Set role to 'admin' for admin user
            verified: true, // Assume admin is always verified
        };

        // Send token for admin
        createSendToken(adminUser, 200, res);
    } catch (error) {
        console.error('Error in admin login:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

module.exports = { adminLogin };
