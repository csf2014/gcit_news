const jwt = require('jsonwebtoken');

const authenticateUser = (req, res, next) => {
    const token = req.headers.authorization;

    if (!token || !token.startsWith('Bearer ')) {
        return res.status(401).json({ error: 'Unauthorized - Missing or invalid token format' });
    }

    const tokenValue = token.split(' ')[1]; // Extract the token value without 'Bearer '

    try {
        const decoded = jwt.verify(tokenValue, 'your-secret-key'); // Replace 'your-secret-key' with your actual secret key
        req.user = decoded; // Populate req.user with decoded user information
        next(); // Proceed to the next middleware
    } catch (err) {
        console.error(err); // Log the error for debugging
        return res.status(401).json({ error: 'Unauthorized - Invalid token' });
    }
};

module.exports = { authenticateUser };
