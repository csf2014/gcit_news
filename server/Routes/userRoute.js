
const express = require('express');
const router = express.Router();
const userController = require('../Controller/userController');
const admin = require('../Controller/admin')
const { authenticateUser } = require('../Controller/auth');
const multer = require('multer'); // Import multer
const revokedTokens = []; // Array to store revoked tokens
// Multer storage configuration
// Configure multer for file uploads
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/') // Set your upload directory
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname) // Set filename
    }
});
const upload = multer({ storage: storage });
router.post('/admin/login', admin.adminLogin)
// Signup route
router.post('/signup', userController.signup);
router.put('/user/upload-profile-image', upload.single('image'), authenticateUser, userController.uploadProfileImage);

router.post('/verify-otp', userController.verifyOTP);
// router.post('/verify-otp/', userController.verifyOTP);

// Login route
router.post('/login', userController.login);
// router.get('/getEmail', userController.getEmail);
// Route for resending OTP
router.post('/resendOTP', userController.resendOTP);

// Route for verifying resent OTP
router.post('/verifyResentOTP', userController.verifyResentOTP);
router.get('/getusers', authenticateUser, userController.getAllUsers);
// router.delete('/user/:id', authenticateUser, userController.deleteUser);
router.delete('/user/:id', authenticateUser, userController.deleteUser)

// Route for user logout
router.post('/logout', userController.logout);

// Route for searching users (only accessible to admins)
router.get('/search', authenticateUser, userController.searchUsers);

module.exports = router;
