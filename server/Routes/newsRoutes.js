

// module.exports = router;
const express = require('express');
// const router = express.Router();
const multer = require('multer'); // Import multer
const newsController = require('../Controller/newsController');
// const {isAdmin} = require('../Controller/auth')
const { authenticateUser } = require('../Controller/auth'); // Assuming you have a separate auth file with authentication middleware
const router = express.Router();

// Configure multer for file uploads
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/') // Set your upload directory
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname) // Set filename
    }
});
const upload = multer({ storage: storage });
// Apply middleware for JWT authentication
// router.post('/create', authenticateUser, newsController.createNews);
router.post('/create', upload.single('image'), authenticateUser, newsController.createNews);
// Route for approving or disapproving news articles
router.patch('/:id/approve', authenticateUser, newsController.approveNewsById);
// Route to get all approved news articles
router.get('/approved-news', newsController.getApprovedNews);
// Route to get unapproved news
router.get('/unapproved', authenticateUser, newsController.getUnapprovedNews);

// Search for news articles by keyword
router.get('/search', newsController.searchNewsByKeyword);


// Create a new news article
router.post('/createnews', newsController.createNews);

// Get all news articles
router.get('/', newsController.getAllNews);

// Get one news article by ID
router.get('/:id', newsController.getNewsById);

// Update a news article by ID
router.put('/:id', newsController.updateNewsById);

// Delete a news article by ID
router.delete('/:id', authenticateUser, newsController.deleteNewsById);
router.post('/:id/bookmark', authenticateUser, newsController.createBookmark);
// Route to unbookmark a news article
router.delete('/:id/unbookmark', authenticateUser, newsController.removeBookmark);
router.post('/:id/comment', authenticateUser, newsController.createComment);
// Route to get all comments for a particular news article
router.get('/:id/comments', newsController.getAllCommentsForNews);
router.post('/:id/comment/:commentId/report', newsController.reportComment);
// Route to get reported comments for a specific news article
// router.get('/news/:id/reported-comments', authenticateUser, newsController.getReportedComments);
router.get('/comments/reported-comments', newsController.getAllReportedComments);
// Delete reported comment
router.delete('/comments/reported-comments/:commentId', authenticateUser, newsController.deleteReportedComment);
// Route to get all news articles with category "announcements"
router.get('/news/announcements', newsController.getAnnouncements);
router.get('/news/events', newsController.getevents);
router.get('/news/clubs', newsController.getclubs);


module.exports = router;
