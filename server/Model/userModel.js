

// const mongoose = require('mongoose');

// const userSchema = new mongoose.Schema({
//     name: {
//         type: String,
//         required: true
//     },
//     email: {
//         type: String,
//         required: true,
//         unique: true
//     },
//     password: {
//         type: String,
//         required: true
//     },
//     confirmPassword: {
//         type: String,
//         required: true
//     },
//     verified: {
//         type: Boolean,
//         default: false // Set default value to false
//     },
//     otp: {
//         type: String
//     },
//     otpExpiration: {
//         type: Date
//     },
//     registrationDate: {
//         type: Date,
//         default: Date.now // Set default value to the current date and time
//     }
// });

// const User = mongoose.model('User', userSchema);

// module.exports = User;
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    confirmPassword: {
        type: String,
        required: true
    },
    verified: {
        type: Boolean,
        default: false // Set default value to false
    },
    otp: {
        type: String
    },
    otpExpiration: {
        type: Date
    },
    registrationDate: {
        type: Date,
        default: Date.now // Set default value to the current date and time
    },
    profileImage: {
        type: String,
        default: '/default-profile-image.png' // Default profile image path
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
