// // // // // const mongoose = require('mongoose');

// // // // // const newsSchema = new mongoose.Schema({
// // // // //     title: {
// // // // //         type: String,
// // // // //         required: true,
// // // // //         index: true // Enable text indexing on the title field
// // // // //     },
// // // // //     reporter: {
// // // // //         type: String,
// // // // //         required: true
// // // // //     },
// // // // //     content: {
// // // // //         type: String,
// // // // //         required: true
// // // // //     },
// // // // //     image: {
// // // // //         type: String, // You can store image URL here
// // // // //         required: true
// // // // //     },
// // // // //     category: {
// // // // //         type: String,
// // // // //         enum: ['announcements', 'events', 'clubs'],
// // // // //         required: true
// // // // //     },
// // // // //     uploadDate: {
// // // // //         type: Date,
// // // // //         default: Date.now
// // // // //     }
// // // // // });
// // // // // newsSchema.index({ title: 'text', content: 'text' }); // Index multiple fields for text search
// // // // // const News = mongoose.model('News', newsSchema);

// // // // // module.exports = News;
// // // // const mongoose = require('mongoose');

// // // // const commentSchema = new mongoose.Schema({
// // // //     text: {
// // // //         type: String,
// // // //         required: true
// // // //     },
// // // //     user: {
// // // //         type: mongoose.Schema.Types.ObjectId,
// // // //         ref: 'User', // Reference to the user who posted the comment, replace 'User' with your actual user model name
// // // //         required: true
// // // //     },
// // // //     createdAt: {
// // // //         type: Date,
// // // //         default: Date.now
// // // //     }
// // // // });

// // // // const newsSchema = new mongoose.Schema({
// // // //     title: {
// // // //         type: String,
// // // //         required: true,
// // // //         index: true // Enable text indexing on the title field
// // // //     },
// // // //     reporter: {
// // // //         type: String,
// // // //         required: true
// // // //     },
// // // //     content: {
// // // //         type: String,
// // // //         required: true
// // // //     },
// // // //     image: {
// // // //         type: String, // You can store image URL here
// // // //         required: true
// // // //     },
// // // //     category: {
// // // //         type: String,
// // // //         enum: ['announcements', 'events', 'clubs'],
// // // //         required: true
// // // //     },
// // // //     uploadDate: {
// // // //         type: Date,
// // // //         default: Date.now
// // // //     },
// // // //     comments: [commentSchema] // Embed comment schema as an array of comments
// // // approved: {
// // //     type: Boolean,
// // //     default: false,
// // // },
// // // // New field for comments
// // // comments: [{
// // //     text: {
// // //         type: String,
// // //         required: true
// // //     },
// // //     user: {
// // //         type: mongoose.Schema.Types.ObjectId,
// // //         ref: 'User' // Reference to the User model
// // //     }
// // // }]
// // // // });

// // // // newsSchema.index({ title: 'text', content: 'text' }); // Index multiple fields for text search
// // // // const News = mongoose.model('News', newsSchema);

// // // // module.exports = News;
// // // // Update News Model (newsModel.js)
// const mongoose = require('mongoose');

// // // const commentSchema = new mongoose.Schema({
// // //     text: {
// // //         type: String,
// // //         required: true
// // //     },
// // //     user: {
// // //         type: mongoose.Schema.Types.ObjectId,
// // //         ref: 'User', // Reference to the user who posted the comment, replace 'User' with your actual user model name
// // //         required: true
// // //     },
// // //     createdAt: {
// // //         type: Date,
// // //         default: Date.now
// // //     }
// // // });
// // const commentSchema = new mongoose.Schema({
// //     text: {
// //         type: String,
// //         required: true
// //     },
// //     user: {
// //         type: mongoose.Schema.Types.ObjectId,
// //         ref: 'User', // Reference to the user who posted the comment, replace 'User' with your actual user model name
// //         required: true
// //     },
// //     reporter: {
// //         type: mongoose.Schema.Types.ObjectId,
// //         ref: 'User', // Reference to the user who reported the comment
// //     },
// //     reported: {
// //         type: Boolean,
// //         default: false
// //     },
// //     createdAt: {
// //         type: Date,
// //         default: Date.now
// //     }
// // });

// // const newsSchema = new mongoose.Schema({
// //     title: {
// //         type: String,
// //         required: true,
// //         index: true // Enable text indexing on the title field
// //     },
// //     reporter: {
// //         type: String,
// //         required: true
// //     },
// //     content: {
// //         type: String,
// //         required: true
// //     },
// //     image: {
// //         type: String, // You can store image URL here
// //         required: true
// //     },
// //     category: {
// //         type: String,
// //         enum: ['announcements', 'events', 'clubs'],
// //         required: true
// //     },
// //     uploadDate: {
// //         type: Date,
// //         default: Date.now
// //     },
// //     approved: {
// //         type: Boolean,
// //         default: false,
// //     },
// //     comments: [{

// //         text: {
// //             type: String,
// //             required: true
// //         },
// //         user: {
// //             type: mongoose.Schema.Types.ObjectId,
// //             ref: 'User', // Reference to the user who posted the comment, replace 'User' with your actual user model name
// //             required: true
// //         },
// //         reporter: {
// //             type: mongoose.Schema.Types.ObjectId,
// //             ref: 'User', // Reference to the user who reported the comment
// //         },
// //         reported: {
// //             type: Boolean,
// //             default: false
// //         },
// //         createdAt: {
// //             type: Date,
// //             default: Date.now
// //         }
// //     }], bookmarks: [{
// //         user: {
// //             type: mongoose.Schema.Types.ObjectId,
// //             ref: 'User', // Reference to the User model
// //             required: true
// //         }
// //     }],
// // });

// // newsSchema.index({ title: 'text', content: 'text' }); // Index multiple fields for text search
// // const News = mongoose.model('News', newsSchema);

// // module.exports = News;
// const newsSchema = new mongoose.Schema({
//     title: {
//         type: String,
//         required: true,
//         index: true // Enable text indexing on the title field
//     },
//     reporter: {
//         type: String,
//         required: true
//     },
//     content: {
//         type: String,
//         required: true
//     },
//     image: {
//         type: String, // You can store image URL here
//         required: true
//     },
//     category: {
//         type: String,
//         enum: ['announcements', 'events', 'clubs'],
//         required: true
//     },
//     uploadDate: {
//         type: Date,
//         default: Date.now
//     },
//     approved: {
//         type: Boolean,
//         default: false,
//     },
//     comments: [{
//         text: {
//             type: String,
//             required: true
//         },
//         user: {
//             type: mongoose.Schema.Types.ObjectId,
//             ref: 'User', // Reference to the user who posted the comment, replace 'User' with your actual user model name
//             required: true
//         },
//         reporter: {
//             type: mongoose.Schema.Types.ObjectId,
//             ref: 'User', // Reference to the user who reported the comment
//         },
//         reported: {
//             type: Boolean,
//             default: false
//         },
//         reports: [{
//             user: {
//                 type: mongoose.Schema.Types.ObjectId,
//                 ref: 'User', // Reference to the user who reported the comment
//                 required: true
//             }
//         }],
//         reportCount: {
//             type: Number,
//             default: 0
//         },
//         createdAt: {
//             type: Date,
//             default: Date.now
//         }
//     }],
//     bookmarks: [{
//         user: {
//             type: mongoose.Schema.Types.ObjectId,
//             ref: 'User', // Reference to the User model
//             required: true
//         }
//     }],
// });

// newsSchema.index({ title: 'text', content: 'text' }); // Index multiple fields for text search
// const News = mongoose.model('News', newsSchema);

// module.exports = News;
const mongoose = require('mongoose');
const newsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        index: true // Enable text indexing on the title field
    },
    reporter: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    image: {
        type: String, // You can store image URL here
        required: true
    },
    category: {
        type: String,
        enum: ['announcements', 'events', 'clubs'],
        required: true
    },
    uploadDate: {
        type: Date,
        default: Date.now
    },
    approved: {
        type: Boolean,
        default: false,
    },
    comments: [{
        text: {
            type: String,
            required: true
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User', // Reference to the user who posted the comment, replace 'User' with your actual user model name
            required: true
        },
        reported: {
            type: Boolean,
            default: false
        },
        reports: [{
            user: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User', // Reference to the user who reported the comment
                required: true
            }
        }],
        reportCount: {
            type: Number,
            default: 0
        },
        createdAt: {
            type: Date,
            default: Date.now
        }
    }],
    bookmarks: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User', // Reference to the User model
            required: true
        }
    }],
});

newsSchema.index({ title: 'text', content: 'text' }); // Index multiple fields for text search
const News = mongoose.model('News', newsSchema);

module.exports = News;